<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('HTML-404', ['as' => 'notfound', 'uses' => 'HomeController@pagenotfound']);
Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showFormResetPass')->name('password.reset');

//USER
Route::get('/user/change-password','UserController@changePassword')->name('user.change.password');
Route::post('/user/update-password','UserController@updatePassword')->name('user.update.password');

Route::group(['middleware' => ['changePasswordInTheFirstTimeLogin']], function () {
    Route::get('/bank-soal/dashboard','QuestionController@index')->name('bank-soal.dashboard');
    Route::get('/',function(){ return redirect()->route('bank-soal.dashboard'); });
    Route::get('/home',function(){ return redirect()->route('bank-soal.dashboard'); });

    // PACKAGE
    Route::get('/bank-soal/package','PackageController@index')->name('package');
    Route::get('/bank-soal/package/create' , 'PackageController@create')->name('package.create');
    Route::get('/bank-soal/package/{id}' , 'PackageController@show')->name('package.show');
    Route::get('/bank-soal/package/{id}/edit' , 'PackageController@edit')->name('package.edit');
    Route::post('/bank-soal/package' , 'PackageController@store')->name('package.store');
    Route::put('/bank-soal/package/' , 'PackageController@update')->name('package.update');
    Route::put('/bank-soal/package/{id}/activate','PackageController@activate')->name('package.activate');
    Route::put('/bank-soal/package/{id}/unactivated','PackageController@unactivated')->name('package.unactivated');
    // Route::delete('/bank-soal/package/{id}' , 'PackageController@destroy')->name('package.delete');

// QUESTION
    Route::post('/bank-soal/question','QuestionController@store')->name('question.store');
    Route::post('/bank-soal/question-excel','QuestionController@storeWithExcel')->name('question.storeWithExcel');
    Route::get('/bank-soal/input-bank-soal','QuestionController@create')->name('question.create');
    Route::get('/bank-soal/input-bank-soal/create-package','PackageController@createFromInputQuestion')->name('question.create-package');
    Route::get('/bank-soal/input-bank-soal-excel','QuestionController@createWithExcel')->name('question.createWithExcel');
    Route::get('/bank-soal/input-bank-soal-excel/create-package','PackageController@createFromInputQuestionExcel')->name('question-excel.create-package');
    Route::get('/question/{question_id}/edit' , 'QuestionController@edit')->name('question.edit');
    Route::post('/downloadFailedQuestionUpload','QuestionController@downloadFailedQuestionUpload')->name('download-failed-question-upload');
    Route::post('/question/get/ajax' , 'QuestionController@getQuestionAjaxData')->name('question.get.ajax');
    Route::post('/question/progressStoreWithExcel' , 'QuestionController@StoreProcessWithExcel')->name('question.progressStoreWithExcel');
    Route::post('/question/commiteStoreWithExcel' , 'QuestionController@StoreCommiteWithExcel')->name('question.commiteStoreWithExcel');
    Route::put('/question' , 'QuestionController@update')->name('question.update');
    Route::put('/question/{id}/activate','QuestionController@activate')->name('question.activate');
    Route::put('/question/{id}/unactivated','QuestionController@unactivated')->name('question.unactivated');
    Route::put('/question/{id}/reject-loyalty-question','QuestionController@rejectLoyaltyQuestion')->name('question.reject-loyalty-question');
    Route::put('/question/{id}/accept-loyalty-question','QuestionController@acceptLoyaltyQuestion')->name('question.accept-loyalty-question');
    // Route::delete('/question/{id}' , 'QuestionController@destroy')->name('question.delete');

// FUNCTION
    Route::get('/CRUD/function','FunctionController@index')->name('function');
    Route::post('/CRUD/function' , 'FunctionController@store')->name('function.store');
    Route::get('/CRUD/function/create' , 'FunctionController@create')->name('function.create');
    Route::get('/CRUD/function/download' , 'FunctionController@downloadData')->name('function.download');
    Route::get('/CRUD/function/{id}/edit' , 'FunctionController@edit')->name('function.edit');
    Route::put('/CRUD/function/{id}' , 'FunctionController@update')->name('function.update');
    Route::put('/CRUD/function/{id}/activate','FunctionController@activate')->name('function.activate');
    Route::put('/CRUD/function/{id}/unactivated','FunctionController@unactivated')->name('function.unactivated');
    // Route::delete('/CRUD/function/{id}','FunctionController@destroy')->name('function.delete');

// JOB
    Route::get('/CRUD/job','JobController@index')->name('job');
    Route::post('/CRUD/job' , 'JobController@store')->name('job.store');
    Route::get('/CRUD/job/create','JobController@create')->name('job.create');
    Route::get('/CRUD/job/download' , 'JobController@downloadData')->name('job.download');
    Route::get('/CRUD/job/refresh' , 'JobController@refreshData')->name('job.refresh');
    // Route::get('/CRUD/job/create' , 'JobController@create')->name('job.create');
    Route::get('/CRUD/job/{id}/edit' , 'JobController@edit')->name('job.edit');
    Route::put('/CRUD/job/{id}' , 'JobController@update')->name('job.update');
    Route::put('/CRUD/job/{id}/activate','JobController@activate')->name('job.activate');
    Route::put('/CRUD/job/{id}/unactivated','JobController@unactivated')->name('job.unactivated');
    // Route::delete('/CRUD/job/{id}','JobController@destroy')->name('job.delete');

// COMPETENCY
    Route::get('/CRUD/competency' , 'CompetencyController@index')->name('competency');
    Route::get('/CRUD/competency/create' , 'CompetencyController@create')->name('competency.create');
    Route::get('/CRUD/competency/{id}/edit' , 'CompetencyController@edit')->name('competency.edit');
    Route::get('/CRUD/competency/Download' , 'CompetencyController@downloadData')->name('competency.download');
    Route::post('/CRUD/competency' , 'CompetencyController@store')->name('competency.store');
    Route::put('/CRUD/competency/{id}' , 'CompetencyController@update')->name('competency.update');
    Route::put('/CRUD/competency/{id}/activate','CompetencyController@activate')->name('competency.activate');
    Route::put('/CRUD/competency/{id}/unactivated','CompetencyController@unactivated')->name('competency.unactivated');
    // Route::delete('/CRUD/competency/{id}' , 'CompetencyController@destroy')->name('competency.delete');

// MODUL
    Route::get('/CRUD/modul' , 'ModulController@index')->name('modul');
    Route::get('/CRUD/modul/create' , 'ModulController@create')->name('modul.create');
    Route::get('/CRUD/modul/{id}/edit' , 'ModulController@edit')->name('modul.edit');
    Route::get('/CRUD/modul/Download' , 'ModulController@downloadData')->name('modul.download');
    Route::post('/CRUD/modul' , 'ModulController@store')->name('modul.store');
    Route::put('/CRUD/modul' , 'ModulController@update')->name('modul.update');
    Route::put('/CRUD/modul/{id}/activate','ModulController@activate')->name('modul.activate');
    Route::put('/CRUD/modul/{id}/unactivated','ModulController@unactivated')->name('modul.unactivated');
    // Route::delete('/CRUD/modul/{id}' , 'ModulController@destroy')->name('modul.delete');

//SCORING
    Route::get('/CRUD/scoring' , 'ScoringController@index')->name('scoring');
    Route::get('/CRUD/scoring/create' , 'ScoringController@create')->name('scoring.create');
    Route::get('/CRUD/scoring/Download' , 'ScoringController@downloadData')->name('scoring.download');
    Route::get('/CRUD/scoring/{id}/edit' , 'ScoringController@edit')->name('scoring.edit');
    Route::post('/CRUD/scoring' , 'ScoringController@store')->name('scoring.store');
    Route::put('/CRUD/scoring' , 'ScoringController@update')->name('scoring.update');
    Route::put('/CRUD/scoring/{id}/activate','ScoringController@activate')->name('scoring.activate');
    Route::put('/CRUD/scoring/{id}/unactivated','ScoringController@unactivated')->name('scoring.unactivated');
    // Route::delete('/CRUD/scoring/{id}' , 'ScoringController@destroy')->name('scoring.delete');

// LEVEL
    Route::get('/CRUD/level','LevelController@index')->name('level');
    Route::get('/CRUD/level/create','LevelController@create')->name('level.create');
    Route::get('/CRUD/level/{id}/edit','LevelController@edit')->name('level.edit');
    Route::post('/CRUD/level','LevelController@store')->name('level.store');
    // Route::delete('/CRUD/level/d={id}','LevelController@destroy')->name('level.delete');
    // Route::delete('/CRUD/level/r={id}','LevelController@restore')->name('level.restore');
    Route::put('/CRUD/level/{id}','LevelController@update')->name('level.update');
    Route::put('/CRUD/level/{id}/activate','LevelController@activate')->name('level.activate');
    Route::put('/CRUD/level/{id}/unactivated','LevelController@unactivated')->name('level.unactivated');

// COMPETENCY DETAIL
    Route::get('/matrix-competency/dashboard','CompetencyDetailController@index')->name('competency-detail');
    Route::post('/matrix-competency/competency-required','CompetencyDetailController@store')->name('competency-detail.store');
    Route::get('/matrix-competency/competency-required','CompetencyDetailController@create')->name('competency-detail.create');
    Route::get('/matrix-competency/competency-required/{job_id}/{competency_id}/{level_id}/edit','CompetencyDetailController@edit')->name('competency-detail.edit');
    Route::put('/matrix-competency/competency-required/','CompetencyDetailController@update')->name('competency-detail.update');
    Route::put('/matrix-competency/competency-required/{job_id}/{competency_id}/{level_id}/activate','CompetencyDetailController@activate')->name('competency-detail.activate');
    Route::put('/matrix-competency/competency-required/{job_id}/{competency_id}/{level_id}/unactivated','CompetencyDetailController@unactivated')->name('competency-detail.unactivated');
    // Route::delete('/matrix-competency/competency-required/{job_id}/{competency_id}/{level_id}','CompetencyDetailController@destroy')->name('competency-detail.delete');

// USER
    Route::get('/user','UserController@index')->name('user');
    Route::get('/user/create','UserController@create')->name('user.create');
    //Route::get('/user/edit','UserController@edit')->name('user.edit');
    // Route::put('/user/update','UserController@update')->name('user.update');
    Route::put('/user/{id}/unactivated','UserController@unactivated')->name('user.unactivated');
    Route::put('/user/{id}/activate','UserController@activate')->name('user.activate');


//Customize Notification
    Route::get('/customize-notification/cari','NotificationController@cari')->name('customize-notification.cari');
    Route::get('/customize-notification','NotificationController@index')->name('customize-notification.index');
    Route::get('/customize-notification/create','NotificationController@create')->name('customize-notification.create');
    Route::post('/customize-notification/store','NotificationController@storeNotification')->name('storeNotification.store');
    Route::post('/customize-notification/store-custom','NotificationController@storeNotificationCustom')->name('storeNotificationCustom.store');
    Route::post('/customize-notification/update','NotificationController@updateDefaultNotification')->name('updateDefaultNotification.update');

    Route::delete('/customize-notification/delete/{id}','NotificationController@destroyNotification')->name('destroyNotification.delete');
    Route::delete('/customize-notification/restore/{id}','NotificationController@restoreNotification')->name('restoreNotification.restore');
    Route::get('/customize-notification/{id}/edit','NotificationController@editNotification')->name('editNotification.edit');
    Route::put('/customize-notification/{id}','NotificationController@updateNotification')->name('updateNotification.update');

    Route::get('/notification-log','NotificationLogController@index')->name('notification-log.index');
    Route::get('/notification-log/cari','NotificationLogController@cari')->name('notification-log.cari');

    // Route::get('/customize-notification/daily',function(){return view('customize-notifications.daily');})->name('customize-notification.daily');
    Route::get('/customize-notification/feedback',function(){return view('customize-notifications.feedback');})->name('customize-notification.feedback');
    Route::get('/customize-notification/loyalty',function(){return view('customize-notifications.loyalty');})->name('customize-notification.loyalty');
    // Route::get('/customize-notification/broadcast',function(){return view('customize-notifications.broadcast');})->name('customize-notification.broadcast');
    Route::get('/customize-notification/broadcast','BroadcastController@index')->name('customize-notification.broadcast');

    //Report
    Route::get('/reports','QuizAttemptsController@report')->name('reports');

//Competency Analysis Result
    Route::get('/competency-analysis-results','CompetencyAnalysisResults@index')->name('competency-analysis-results.index');

//Quiz
    Route::get('/quiz','QuizController@index')->name('quiz');
    Route::get('/quiz/create','QuizController@create')->name('quiz.create');
    Route::get('/quiz/{id}','QuizController@show')->name('quiz.show');
    Route::get('/quiz/{id}/edit/{from}','QuizController@edit')->name('quiz.edit');
    Route::post('/quiz','QuizController@store')->name('quiz.store');
    Route::put('/quiz','QuizController@update')->name('quiz.update');
    Route::put('/quiz/{id}/activate','QuizController@activate')->name('quiz.activate');
    Route::put('/quiz/{id}/unactivated','QuizController@unactivated')->name('quiz.unactivated');
    // Route::delete('/quiz/{id}','QuizController@destroy')->name('quiz.delete');

//Quiz Que Grade
    Route::get('quiz/{quiz_id}/add-question','QuizQueGradeController@firstStepCreate')->name('quiz-que-grade.create');
    Route::get('quiz/{quiz_id}/add-question/package/{package_id}','QuizQueGradeController@secondStepCreate')->name('quiz-que-grade.create-step-2');
    Route::post('/quiz-que-grade','QuizQueGradeController@multiStore')->name('quiz-que-grade.store');
    Route::post('/quiz-que-grade/multi-destroy','QuizQueGradeController@multiDestroy')->name('quiz-que-grade.multiDestroy');
    Route::delete('/quiz-que-grade','QuizQueGradeController@destroy')->name('quiz-que-grade.delete');

//Setting
    Route::get('/setting/daily', function(){ return view('settings.daily');})->name('setting.daily');
    Route::get('/setting/loyalty', function(){ return view('settings.loyalty');})->name('setting.loyalty');
    Route::get('/setting/q-bank-package', function(){ return view('settings.q-bank-package');})->name('setting.q-bank-package');

});
Route::get('/check',function () { return date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()));});
Route::get('/check2/{name}','QuestionController@check');

Route::get('','HomeController@index')->name('home');

Route::get('/check',function(){return App\Helpers\Employee::getData();});

