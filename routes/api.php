<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
$router->group(['middleware' => 'cors'], function () {
    Route::get('/get-quiz-today','QuizAttemptsController@getQuizToday');//npk,job_name
    Route::get('/get-quiz','QuizAttemptsController@getUnanswerQuiz');//npk
    Route::post('/answer-quiz','QuizAttemptsController@answerQuestion');//npk,attempts_id,question_answer_id

    Route::get('/get-notif-daily','NotificationController@getNotifDaily');
    Route::get('/get-notif-ot-uncorrect-answer','NotificationController@getNotifOTUnCorrectAnswer');
    Route::get('/get-notif-uncorrect-answer','NotificationController@getNotifUnCorrectAnswer');
    Route::get('/get-notif-correct-answer','NotificationController@getNotifCorrectAnswer');
    Route::get('/get-notif-ot-correct-answer','NotificationController@getNotifOTCorrectAnswer');
    Route::get('/get-function-job', 'QuestionController@getFunctionJob'); //get function-job name

    Route::post('/post-notif-log','NotificationLogController@storeLog'); //post log notification 

    Route::get('/get-report','CompetencyAnalysisResults@getData'); //get report seperti CMS
    Route::get('/get-individual-report-array','QuizAttemptsController@individualReportArray'); //get history daily quiz leader
    Route::get('/get-individual-report','QuizAttemptsController@individualReport');//npk
    Route::get('/get-history-poin','QuizAttemptsController@getIndividualHistoryPoin');//get history poin individual by npk
    Route::get('/get-team-report','QuizAttemptsController@teamReport'); //array npk

    Route::post('/loyalty-question','QuestionController@storeLoyaltyQuestion');//npk, question, , answerA b c d, answer
});