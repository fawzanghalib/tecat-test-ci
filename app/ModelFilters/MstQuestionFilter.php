<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class MstQuestionFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function competency($ids)
    {
        return $this->whereIn('mst_question.competency_id', $ids);
    }

    public function job($ids)
    {
        return $this->whereIn('mst_question.job_id', $ids);
    }

    public function level($ids)
    {
        return $this->whereIn('mst_question.level_id', $ids);
    }

    public function questionType($id){
        return $this->where('mst_question.question_type_id',$id);
    }

    public function search($search){
        return $this->where('question', 'LIKE', '%' . $search . '%');
    }

    public function startDate($startDate){
        return $this->where('mst_question.start_date','>=',$startDate);
    }

    public function endDate($endDate){
        return $this->where('mst_question.end_date','<=',$endDate);
    }

    public function flagActive($id){
        return $this->where('mst_question.flag_active', $id);
    }
}
