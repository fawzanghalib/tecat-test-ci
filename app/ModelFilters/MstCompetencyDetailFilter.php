<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class MstCompetencyDetailFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function competency($ids)
    {
        return $this->whereIn('mst_competency_details.competency_id', $ids);
    }

    public function job($ids)
    {
        return $this->whereIn('mst_competency_details.job_id', $ids);
    }

    public function level($ids)
    {
        return $this->whereIn('mst_competency_details.level_id', $ids);
    }

    public function startDate($startDate){
        return $this->where('mst_competency_details.start_date','>=',$startDate);
    }

    public function endDate($endDate){
        return $this->where('mst_competency_details.end_date','>=',$endDate);
    }

    public function flagActive($id){
        return $this->where('mst_competency_details.flag_active', $id);
    }
}
