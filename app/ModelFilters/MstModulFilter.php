<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class MstModulFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function search($search){
        return $this->where('mst_modul.modul_name', 'LIKE', '%' . $search . '%')
            ->orWhere('mst_modul.description','LIKE', '%' . $search . '%');
    }

    public function startDate($startDate){
        return $this->where('mst_modul.start_date','>=',$startDate);
    }

    public function endDate($endDate){
        return $this->where('mst_modul.end_date','<=',$endDate);
    }

    public function flagActive($id){
        return $this->where('mst_modul.flag_active', $id);
    }
}
