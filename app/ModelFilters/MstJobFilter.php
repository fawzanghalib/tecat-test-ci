<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class MstJobFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function search($search){
        return $this->where('mst_job.job_name', 'LIKE', '%' . $search . '%')
            ->orWhere('mst_job.description','LIKE', '%' . $search . '%')
            ->orWhere('mst_job.job_id','LIKE', '%' . $search . '%');
    }

    public function startDate($startDate){
        return $this->where('mst_job.start_date','>=',$startDate);
    }

    public function endDate($endDate){
        return $this->where('mst_job.end_date','<=',$endDate);
    }

    public function flagActive($id){
        return $this->where('mst_job.flag_active', $id);
    }
}
