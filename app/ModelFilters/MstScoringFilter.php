<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class MstScoringFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function search($search){
        return $this->where('mst_scoring.scoring_type', 'LIKE', '%' . $search . '%');
    }

    public function startDate($startDate){
        return $this->where('mst_scoring.start_date','>=',$startDate);
    }

    public function endDate($endDate){
        return $this->where('mst_scoring.end_date','<=',$endDate);
    }

    public function flagActive($id){
        return $this->where('mst_scoring.flag_active', $id);
    }

    public function type($ids)
    {
        return $this->whereIn('mst_scoring.type', $ids);
    }
}
