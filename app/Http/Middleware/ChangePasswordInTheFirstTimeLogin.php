<?php

namespace App\Http\Middleware;

use Auth;
use DB;
use Closure;

class ChangePasswordInTheFirstTimeLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = DB::table('users')->where('id',Auth::user()->id)->select(DB::raw("TIMESTAMPDIFF(SECOND, created_at , updated_at) as timeStampDiff"))->first();
        // dd($user->timeStampDiff);
        if($user->timeStampDiff > 60 ){
            return $next($request);
        }
            return redirect()->route('user.change.password')->with('error','Change Password In The First Time Login');
    }
}
