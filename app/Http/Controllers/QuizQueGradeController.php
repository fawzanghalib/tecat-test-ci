<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\MstQuiz;
use App\Model\MstQuestion;
use App\Model\MstQuizQueGrade;
use App\Model\MstCompetency;
use App\Model\MstLevel;
use App\Model\MstJob;
use App\Model\MstPackage;
use Auth;

class QuizQueGradeController extends Controller
{
    private $MstQuiz;
    private $MstQuizQueGrade;
    private $MstQuestion;
    private $MstCompetency;
    private $MstLevel;
    private $MstJob;
    private $MstPackage;

    public function __construct(MstQuiz $MstQuiz,MstQuizQueGrade $MstQuizQueGrade,MstQuestion $MstQuestion,MstCompetency $MstCompetency,MstLevel $MstLevel,MstJob $MstJob,MstPackage $MstPackage)
    {
        $this->MstQuiz = $MstQuiz;
        $this->MstQuizQueGrade = $MstQuizQueGrade;
        $this->MstQuestion = $MstQuestion;
        $this->MstCompetency = $MstCompetency;
        $this->MstLevel = $MstLevel;
        $this->MstJob = $MstJob;
        $this->MstPackage = $MstPackage;
        $this->middleware('auth');
    }

    /**
     * Show Active Package list for creating a new QuizQueGrade.
     *
     * @param  int  $quiz_id
     * @return \Illuminate\Http\Response
     */
    public function firstStepCreate($quiz_id)
    {
        $quiz = $this->MstQuiz->getSingleData(true,['quiz_id' => $quiz_id]);
        if(!isset($quiz)){return 404;}
        $packages = $this->MstPackage->getData();
        return view('quiz-que-grades.FirstStepCreate',compact('quiz','packages'));
    }

    /**
     * Show Question list from the package have chosen.
     *
     * @param  int  $quiz_id
     * @return \Illuminate\Http\Response
     */
    public function secondStepCreate(Request $request,$quiz_id,$package_id){
        $competencies = $this->MstCompetency->getData();
        $levels = $this->MstLevel->getData();
        $jobs = $this->MstJob->getData();
        $quiz = $this->MstQuiz->getSingleData(true,['quiz_id' => $quiz_id]);   
        if(!isset($quiz)){return 404;}
        $questions = $this->MstQuestion->getDataWithPackageIdInRangeDateQuiz($request,$quiz_id,$package_id,$quiz->start_date,$quiz->end_date);
        return view('quiz-que-grades.SecondStepCreate',compact('request','competencies','levels','jobs','questions','quiz','package_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function multiStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quiz_id' => 'required|numeric',
            'question.*' => 'required',
        ]);
        if ($validator->fails()) {
            $request->flash();
            return redirect()
                ->route('quiz-que-grade.create',['quiz_id' => $request->input('quiz_id')])
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $this->MstQuizQueGrade->createData($request);
            return redirect()->route('quiz.show',['id' => $request->input('quiz_id')])->with('success','question berhasil ditambahkan ke quiz');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quiz = $this->MstQuiz->getSingleData(false,['quiz_id' => $id],['mstJob']);
        return view('quizzes.show',compact('quiz'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->MstQuizQueGrade->destroyData($request);
        return redirect()->route('quiz.show',['id' => $request->quiz_id])->with('error','question berhasil dihapus dari quiz');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function multiDestroy(Request $request)
    {
        $this->MstQuizQueGrade->multiDestroyData($request);
        return redirect()->route('quiz.show',['id' => $request->quiz_id]);
    }
}
