<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Job\JobStoreRequest;
use App\Http\Requests\Job\JobUpdateRequest;
use App\Model\MstJob;
use App\Model\MstFunction;
use App\Helpers\Job;
use Auth;
use Exporter;

class JobController extends Controller
{
    private $MstJob;
    private $JobHelper;
    private $MstFunction;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MstJob $MstJob, MstFunction $MstFunction,Job $Job)
    {
        $this->MstJob = $MstJob;
        $this->MstFunction = $MstFunction;
        $this->JobHelper = $Job;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Function with jobs list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        $jobs = $this->MstJob->getFilterData($request);
        return view('jobs.index',compact('jobs','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $functions = $this->MstFunction->getData();
        if (empty($functions)) {return 404;}
        return view('jobs.create', compact('functions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobStoreRequest $request)
    {
        $this->MstJob->createData($request->all());
        return redirect()->route('job');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = $this->MstJob->getSingleData(true,['job_id' => $id]);
        if(!isset($job)){return 404;}
        return view('jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = $this->MstJob->getSingleData(true,['job_id' => $id]);
        $jobs = $this->MstJob->getData(true);
        $functions = $this->MstFunction->getData();
        if(!isset($job)){return 404;}
        return view('jobs.edit', compact('job','jobs','functions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,JobUpdateRequest $request)
    {
        $this->MstJob->updateData($request->all(), $id);
        return redirect()->route('job')->with('success','data job berhasil diupdate');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->MstJob->unactivatedData($id);
        return redirect()->route('job')->with('error','data job berhasil dinonaktifkan');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->MstJob->activateData($id);
        return redirect()->route('job')->with('success','data job berhasil diaktifkan');
    }

    public function downloadData(){ 
        $excel = Exporter::make('Excel');
        $excel->load($this->MstJob->getData());
        return $excel->stream('Job.xlsx');
    }

    public function refreshData(){
        $Jobs = $this->JobHelper->getData();
        foreach($Jobs as $job){
            // if($this->MstJob->getSingleDataWithJobId($job['JobId'],true) == null){
            if($this->MstJob->getSingleData(true,['job_id' => $job['JobId']]) == null){
                $this->MstJob->createDataFromApi($job);
            }
        }
        return redirect()->route('job');
    }
}
