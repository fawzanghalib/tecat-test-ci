<?php

namespace App\Http\Controllers;

use App\Serialiser\Serialiser_Reports;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\MstQuiz;
use App\Model\MstJob;
use App\Model\MstFunction;
use App\Model\MstQuizQueGrade;
use App\Model\MstQuestion;
use App\Model\MstQuizAttempts;
use App\Model\MstCompetencyDetail;
use App\Model\MstCompetency;
use App\Model\MstQuizRespons;
use App\Model\MstScoring;
use App\Model\MstQuizGrade;
use App\Model\MstLevel;
use App\Helpers\StaticFunction;
use DateTime;
use File;
use Exporter;

class QuizAttemptsController extends Controller
{
    private $MstQuiz;
    private $MstJob;
    private $MstFunction;
    private $MstQuizQueGrade;
    private $MstQuestion;
    private $MstQuizAttempts;
    private $MstCompetencyDetail;
    private $MstCompetency;
    private $MstQuizRespons;
    private $MstScoring;
    private $MstQuizGrade;
    private $MstLevel;
    private $StaticFunction;
    public function __construct(MstQuiz $MstQuiz,MstJob $MstJob,MstFunction $MstFunction,MstQuizQueGrade $MstQuizQueGrade,MstQuestion $MstQuestion,MstCompetencyDetail $MstCompetencyDetail,MstCompetency $MstCompetency,MstQuizAttempts $MstQuizAttempts,MstQuizRespons $MstQuizRespons,MstScoring $MstScoring,MstQuizGrade $MstQuizGrade,MstLevel $MstLevel,StaticFunction $StaticFunction)
    {
        $this->MstQuiz = $MstQuiz;
        $this->MstJob = $MstJob;
        $this->MstFunction = $MstFunction;
        $this->MstQuizQueGrade = $MstQuizQueGrade;
        $this->MstQuestion = $MstQuestion;
        $this->MstQuizAttempts = $MstQuizAttempts;
        $this->MstCompetencyDetail = $MstCompetencyDetail;
        $this->MstCompetency = $MstCompetency;
        $this->MstQuizRespons = $MstQuizRespons;
        $this->MstScoring = $MstScoring;
        $this->MstQuizGrade = $MstQuizGrade;    
        $this->MstLevel = $MstLevel;    
        $this->StaticFunction = $StaticFunction;    
        $this->middleware('auth')->only(['report']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getQuizToday(Request $request)
    {
        if($this->MstQuizAttempts->haveGotQuizToday($request->npk)){
            return 'Anda sudah mendapatkan quiz hari ini';
        } 
        elseif($this->MstQuizAttempts->getQuizStillNotAnswered($request->npk)->count()!=0){
            return 'Anda masih memiliki quiz yang belum di kerjakan';
        }
        elseif($this->StaticFunction->todayIs('Sunday')){
            return 'Tidak ada soal untuk hari minggu';
        }
        else{
            $quiz = $this->MstQuiz->getActiveQuizToday();
            if(!isset($quiz)){return 404;}
            
            $job = $this->MstJob->getSingleData(false, ['job_name' => $request->job_name]);
            if(!isset($job)){return 404;}

            $arrayAttemptsQuestionId =  $this->MstQuizAttempts->getQuestionsAreAnsweredCorrectly(['user_id' => $request->npk,'quiz_id' => $quiz->quiz_id])->pluck('question_id')->toArray();
            
            if($this->StaticFunction->todayIs('Saturday')){
                $question = $this->MstQuestion->getSingleRandomQuestion(null,$quiz->quiz_id,$arrayAttemptsQuestionId);
            }

            else{   
                $arrayCompetencyId = $this->MstCompetencyDetail->getLeastCompetencyForAttemptsQuiz($job->job_id,$arrayAttemptsQuestionId);
                $question = $this->MstQuestion->getSingleRandomQuestion($job->job_id,$quiz->quiz_id,$arrayAttemptsQuestionId,$arrayCompetencyId);
            }
            
            if($question){
                $attempts_id = $this->MstQuizAttempts->createData($quiz->quiz_id, $request->npk, $question->question_id)->attempts_id;
                $question = $this->MstQuizAttempts->where('attempts_id',$attempts_id)->with('mstQuestion:question_id,question','mstQuestion.mstQuestionAnswers:question_id,question_answer_id,answer')->get(['question_id','attempts_id','time_start']);
                return json_encode($question);
            }
            else
                return 'tidak ada soal';
        }
        
    }

    public function getUnanswerQuiz(Request $request){
        $quiz_attempts = $this->MstQuizAttempts->getQuizStillNotAnswered($request->input('npk'))->first();
        if($quiz_attempts){
            $question = $this->MstQuizAttempts->where('attempts_id',$quiz_attempts->attempts_id)
                ->with('mstQuestion:question_id,question','mstQuestion.mstQuestionAnswers:question_id,question_answer_id,answer')
                ->get(['question_id','attempts_id','time_start']);
                return json_encode($question);
        }
        else{
            return 0;
        }
    }

    public function answerQuestion(Request $request){ 
        $attempt = $this->MstQuizAttempts->getSingleData(['attempts_id' => $request->attempts_id]);
        
        if (!isset($attempt->time_finish)) {
            $question_id =  $this->MstQuizAttempts->where('attempts_id',$request->attempts_id)->first()->question_id;
            $question = $this->MstQuestion->getSingleData(false,['question_id' => $question_id],['mstQuestionAnswers']);
            $answerResult = $this->MstQuestion->getResultAnswer($question_id,$request->question_answer_id);
            $this->MstQuizRespons->createData($request->npk,$request->attempts_id,$attempt->quiz_id,$request->question_answer_id,$answerResult);
            //check apakah proses menjawab tepat waktu, jika jawaban benar
            if($answerResult){
                $date = new DateTime($attempt->time_start);
                $date2 = new DateTime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                if($date->format("Y-m-d")<$date2->format("Y-m-d")){
                    //jawaban benar dan tidak tepat waktu
                    $type = 1;
                    $message = 1; // nilai untuk if-else mobile
                }
                else{
                    //jika jawaban benar dan tepat waktu
                    $type = 2;
                    $message = 2; // nilai untuk if-else mobile
                }
            }
            else{

                $date = new DateTime($attempt->time_start);
                $date2 = new DateTime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                if($date->format("Y-m-d")<$date2->format("Y-m-d")){
                    //jika jawaban salah dan tidak tepat waktu
                    $type = 3;
                    $message = 3; // nilai untuk if-else mobile
                }
                else{
                    //jika jawaban salah dan tepat waktu
                    $type = 4;
                    $message = 4; // nilai untuk if-else mobile
                }

                //jika jawaban salah
                // $type = 3;
                // $message = 3; // nilai untuk if-else mobile
            }
            $scoring = $this->MstScoring->getScoringToday($type,$attempt->time_start);
            $this->MstQuizAttempts->updateScore($request,$scoring,$request->npk);
            // $this->MstQuizGrade->updateScore($question,$request->npk,$scoring->score);
            return $message;
        }
        else{
            return 'the question has been answered';
        }
    }

    public function individualReport(Request $request){
        return $this->MstQuizAttempts->getIndividualReport($request->npk);
    }

    public function getIndividualHistoryPoin(Request $request){
        return $this->MstQuizAttempts->getIndividualHistoryPoin($request->npk);
    }

    public function teamReport(Request $request){
        return $this->MstQuizAttempts->getTeamReport($request->arrayNPK);
    }

    public function individualReportArray(Request $request){
        return $this->MstQuizAttempts->getIndividualReportArray($request->arrayNPK);
    }

    public function report(Request $request){
        $functions = $this->MstFunction->getData();
        $jobs = $this->MstJob->getData();
        $competencies = $this->MstCompetency->getData();
        $levels = $this->MstLevel->getData();
        $reports = $this->MstQuizAttempts->getReportData($request);
        if($request->action == 'download'){
            $serialiser = new Serialiser_Reports();
            $excel = Exporter::make('Excel');
            $excel->load($reports)->setSerialiser($serialiser);
            return $excel->stream('Report.xlsx');
        }
        return view('reports.index',compact('functions','jobs','competencies','levels','request','reports'));
    }
}
