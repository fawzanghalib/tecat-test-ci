<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Package\PackageStoreRequest;
use App\Http\Requests\Package\PackageUpdateRequest;
use App\Model\MstPackage;
use App\Model\MstCompetency;
use App\Model\MstLevel;
use App\Model\MstJob;
use App\Model\MstQuestion;
use Auth;

class PackageController extends Controller
{
    private $MstPackage;
    private $MstCompetency;
    private $MstLevel;
    private $MstJob;
    private $MstQuestion;

    public function __construct(MstPackage $MstPackage,MstCompetency $MstCompetency,MstLevel $MstLevel,MstJob $MstJob,MstQuestion $MstQuestion)
    {
        $this->MstPackage = $MstPackage;
        $this->MstCompetency = $MstCompetency;
        $this->MstLevel = $MstLevel;
        $this->MstJob = $MstJob;
        $this->MstQuestion = $MstQuestion;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the package.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = $this->MstPackage->getData(true);
        return view('packages.index',compact('packages'));
    }

    /**
     * Show the form for creating a new package.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $from = 1;
        return view('packages.create',compact('from'));
    }
    public function createFromInputQuestion(){
        $from = 2;
        return view('packages.create',compact('from'));
    }
    public function createFromInputQuestionExcel(){
        $from = 3;
        return view('packages.create',compact('from'));
    }

    /**
     * Store a newly created package in storage.
     *
     * @param  \Illuminate\Http\PackageStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageStoreRequest $request)
    {
        $this->MstPackage->createData($request->all());
        if($request->from == 2)
            return redirect()->route('question.create')->with('success','data package berhasil ditambahkan');
        elseif($request->from == 3)
            return redirect()->route('question.createWithExcel')->with('success','data package berhasil ditambahkan');
        else
            return redirect()->route('package')->with('success','data package berhasil ditambahkan');
    }

    /**
     * Display the specified package and all questions in it.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {   
        if($id == 0){
            $package = collect();
            $package->package_id = 0;
            $package->package_name = 'Soal loyalty';
            $package->description = 'daftar soal loyalty yang belum di approved';
            $package->flag_active = 1;
        }
        else
            $package = $this->MstPackage->getSingleData(true,['package_id' => $id]);
        $competencies = $this->MstCompetency->getData();
        $levels = $this->MstLevel->getData();
        $jobs = $this->MstJob->getData();
        if($id == 0)
            $questions = $this->MstQuestion->getLoyaltyQuestions();
        else
            $questions = $this->MstQuestion->getFilterData($request,$id);
        return view('packages.show',compact('package','levels','questions','competencies','request','jobs'));
    }

    /**
     * Show the form for editing the specified package.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = $this->MstPackage->getSingleData(true,['package_id' => $id]);
        if (empty($package)) {return 404;}
        return view('packages.edit',compact('package'));
    }

    /**
     * Update the specified package in storage.
     *
     * @param  \Illuminate\Http\PackageUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageUpdateRequest $request)
    {
        $this->MstPackage->updateData($request->all());
        return redirect()->route('package')->with('success','data package berhasil diupdate');
    }

    /**
     * Unactivated the specified package from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->MstPackage->unactivatedData($id);
        return redirect()->route('package.show',['id'=>$id])->with('error','data package berhasil dinonaktifkan');
    }

        /**
     * Unactivated the specified package from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->MstPackage->activateData($id);
        return redirect()->route('package.show',['id'=>$id])->with('success','data package berhasil diaktifkan');
    }
}
