<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\MstNotification;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    private $MstNotification;

    public function __construct(MstNotification $MstNotification)
    {
        $this->MstNotification = $MstNotification;
        // $this->middleware('auth');
    }
    public function index(Request $request)
    {   
        // $data = $this->MstNotification->paginate(10);
        // return view('customize-notifications.daily.index', compact('data'));
       
        $data = $this->MstNotification->getSearchAndFilterData($request);
        return view('customize-notifications.index',compact('data','request'));
    }

    public function create()
    {
        $data = $this->MstNotification->getDataDefault();
        return view('customize-notifications.create', compact('data'));
    }
    public function destroyNotification($id)
    {
        $this->MstNotification->destroyData($id);
        return redirect()->back()->with('error', 'Status dinonaktifkan.');
    }
    public function restoreNotification($id)
    {
        $data = $this->MstNotification->restoreDaily($id);
        if($data==1)
        {
            return redirect()->back()->with('error', 'Gagal. Terdapat Custom Feedback / Daily yang masih aktif.');
        }
        else
        {
            return redirect()->back()->with('success', 'Status diaktifkan.');
        }
        
    }
    public function storeNotification(Request $request)
    {   
        
        $notif = $this->MstNotification->createData($request);

        if($notif==1)
        {
            return redirect()->route('customize-notification.index')->withError(__('Failed. Please change existing data.'));   
        }
        else if($notif==2)
        {
            return redirect()->route('customize-notification.index')->withError(__('Failed. There is some data is currently active.'));
        }
        else
        {
            return redirect()->route('customize-notification.index')->withStatus(__('Data added successfully!'));   
        }
         
    }

    public function storeNotificationCustom(Request $request)
    {
        $notif = $this->MstNotification->createDataCustom($request);

        if($notif==1)
        {
            return redirect()->route('customize-notification.index')->withError(__('Gagal. Silahkan ubah data yang ada..'));   
        }
        else if($notif==2)
        {
            return redirect()->route('customize-notification.index')->withError(__('Gagal. Terdapat Custom Feedback / Daily yang masih aktif.'));
        }
        else
        {
            return redirect()->route('customize-notification.index')->withStatus(__('Data berhasil ditambahkan!'));   
        }
    }

    public function editNotification($id)
    {
        $cek = $this->MstNotification->where('notification_id',$id)->first();
        if($cek == null)
        {
            return redirect()->route('customize-notification.index')->with('IDNotFound', 'ID tidak ditemukan!');
        }
        else 
        {
            $data = $this->MstNotification->where('notification_id',$id)->first();
            return view('customize-notifications.edit', compact('data'));
        }
    }
    public function updateNotification(Request $request, $id)
    {
        $this->MstNotification->updateData($request, $id);
        return redirect()->route('customize-notification.index')->withStatus(__('Sukses. Data berhasil diubah!'));  
    }

    public function updateDefaultNotification(Request $request){
        $this->MstNotification->updateDataDefault($request);
        
        return redirect()->route('customize-notification.index')->withStatus(__('Sukses. Data berhasil diubah!'));  
    }

    
    // API

    public function getNotifDaily() // REMINDER LOGIN BUTTON
    {
        $date_now = Carbon::now('Asia/Jakarta')->toDateTimeString();
        $now = date('Y-m-d', strtotime($date_now));

        $data = $this->MstNotification->where('flag_active','1')->where('type','Daily')->where('notification_name','Master Daily Notification Format')->where('end_date','>=',$now);

        if($data->first()!=null)
        {
           $get = $this->MstNotification->where('flag_active','1')->where('type','Daily')->where('start_date','<=',$now)->where('end_date','>=',$now)->where('notification_name','Custom Seasonal Format');
           if($get->first()!=null)
           {
               return json_decode($get->get());
           }
           else
           {   
               return json_decode($data->get());
           }
        }
    }

    public function getNotifOTCorrectAnswer()
    {
        $date_now = Carbon::now('Asia/Jakarta')->toDateTimeString();
        $now = date('Y-m-d', strtotime($date_now));

        $seasonal = $this->MstNotification->where('flag_active','1')->where('type','Feedback')->where('start_date','<=',$now)->where('end_date','>=',$now)->where('notification_name','Custom Seasonal Format Correct');
        $data = $this->MstNotification->where('flag_active','1')->where('type','Feedback')->where('notification_name','Master Correct Answer Notification Format');
        $get = $this->MstNotification->where('flag_active','1')->where('type','Daily')->where('start_date','<=',$now)->where('end_date','>=',$now)->where('notification_name','Master On Time Answered Question Notification Format');

        if($seasonal->first()!=null)
        {
            return array([
                'default' => $seasonal->get(),
                'on_time' => $get->get()
            ]);
        }
        else
        {
            return array([
                'default' => $data->get(),
                'on_time' => $get->get()
            ]);
        }
    }

    public function getNotifCorrectAnswer()
    {
        $date_now = Carbon::now('Asia/Jakarta')->toDateTimeString();
        $now = date('Y-m-d', strtotime($date_now));

        $seasonal = $this->MstNotification->where('flag_active','1')->where('type','Feedback')->where('start_date','<=',$now)->where('end_date','>=',$now)->where('notification_name','Custom Seasonal Format Correct');
        $data = $this->MstNotification->where('flag_active','1')->where('type','Feedback')->where('notification_name', 'Master Correct Answer Notification Format');

        if($seasonal->first()!=null)
        {
            return json_decode($seasonal->get());
        }
        else
        {
            return json_decode($data->get());
        }
        
    }

    public function getNotifOTUnCorrectAnswer()
    {
        $date_now = Carbon::now('Asia/Jakarta')->toDateTimeString();
        $now = date('Y-m-d', strtotime($date_now));

        $data = $this->MstNotification->where('flag_active','1')->where('type','Feedback')->where('notification_name','Master Uncorrect Answer Notification Format');
        $get = $this->MstNotification->where('flag_active','1')->where('type','Daily')->where('start_date','<=',$now)->where('notification_name','Master On Time Answered Question Notification Format');

        return array([
            'default' => $data->get(),
            'on_time' => $get->get()
        ]);
    }

    public function getNotifUnCorrectAnswer()
    {
        $data = $this->MstNotification->where('flag_active','1')->where('type','Feedback')->where('notification_name', 'Master Uncorrect Answer Notification Format');

        return json_decode($data->get());
    }

}
