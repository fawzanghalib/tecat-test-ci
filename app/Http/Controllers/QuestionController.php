<?php

namespace App\Http\Controllers;

use App\Serialiser\Serialiser_FailedUploadQuestion;
use App\Serialiser\Serialiser_FailedUploadQuestion2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Question\QuestionStoreRequest;
use App\Http\Requests\Question\QuestionUpdateRequest;
use App\Http\Requests\Question\QuestionExcelStoreRequest;
use App\Model\MstPackage;
use App\Model\MstQuestion;
use App\Model\MstQuestionType;
use App\Model\MstCompetencyDetail;
use App\Model\MstCompetency;
use App\Model\MstQuestionAnswer;
use App\Model\MstModul;
use App\Model\MstJob;
use App\Model\MstLevel;
use App\Model\MstQuiz;
use App\Model\MstQuizQueGrade;
use App\Model\MstQuizAttempts;
use App\Model\MstFunction;
use App\Model\MstScoring;
use App\Model\MstEmployeeScoring;
use App\Helpers\Job;
use Auth;
use DB;
use Importer;
use Exporter;
use File;

class QuestionController extends Controller
{
    private $MstPackage;
    private $MstCompetency;
    private $MstQuestion;
    private $MstQuestionType;
    private $MstCompetencyDetail;
    private $MstQuestionAnswer;
    private $MstModul;
    private $MstLevel;
    private $MstJob;
    private $MstQuizQueGrade;
    private $MstQuizAttempts;
    private $MstFunction;
    private $MstScoring;
    private $MstEmployeeScoring;
    private $Job;
    private $MstQuiz;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MstPackage $MstPackage,MstCompetency $MstCompetency,Job $Job,MstQuestion $MstQuestion,MstQuestionType $MstQuestionType,MstCompetencyDetail $MstCompetencyDetail,MstModul $MstModul,MstLevel $MstLevel,MstQuestionAnswer $MstQuestionAnswer,MstJob $MstJob,MstQuiz $MstQuiz,MstQuizQueGrade $MstQuizQueGrade,MstQuizAttempts $MstQuizAttempts,MstFunction $MstFunction,MstScoring $MstScoring,MstEmployeeScoring $MstEmployeeScoring)
    {
        $this->MstPackage = $MstPackage;
        $this->MstCompetency = $MstCompetency;
        $this->MstQuestion = $MstQuestion;
        $this->MstQuestionType = $MstQuestionType;
        $this->MstCompetencyDetail = $MstCompetencyDetail;
        $this->MstQuestionAnswer = $MstQuestionAnswer;
        $this->MstModul = $MstModul;
        $this->MstLevel = $MstLevel;
        $this->MstJob = $MstJob;
        $this->MstQuiz = $MstQuiz;
        $this->MstQuizQueGrade = $MstQuizQueGrade;
        $this->MstQuizAttempts = $MstQuizAttempts;
        $this->MstFunction = $MstFunction;
        $this->MstScoring = $MstScoring;
        $this->MstEmployeeScoring = $MstEmployeeScoring;
        $this->Job = $Job;
        $this->middleware('auth',['except' => ['getFunctionJob','storeLoyaltyQuestion']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quiz = $this->MstQuiz->getActiveQuizToday();
        if($quiz){
            $functionQuestionTotal =  $this->MstQuizQueGrade
                ->where('quiz_id',$quiz->quiz_id)
                ->join('mst_question','mst_quiz_que_grade.question_id','=','mst_question.question_id')
                ->join('mst_job','mst_question.job_id','=','mst_job.job_id')
                ->join('mst_function','mst_job.function_id','=','mst_function.function_id')
                ->groupBy('mst_function.function_id')
                ->select('mst_function.function_id','mst_function.function_name',DB::raw('count(*) as total_soal'))
                ->get();
                
            foreach($functionQuestionTotal as $item){
                $temp = $this->MstQuizQueGrade
                    ->where('quiz_id',$quiz->quiz_id)
                    ->where('mst_job.function_id',$item->function_id)
                    ->join('mst_question','mst_quiz_que_grade.question_id','=','mst_question.question_id')
                    ->join('mst_job','mst_question.job_id','=','mst_job.job_id')
                    ->groupBy('mst_job.job_id')
                    ->select('mst_job.job_id','mst_job.job_name',DB::raw('count(*) as total_soal'))
                    ->get();
                foreach ($temp as $item2) {
                    $temp2 = $this->MstQuizQueGrade
                        ->where('quiz_id',$quiz->quiz_id)
                        ->where('mst_question.job_id',$item2->job_id)
                        ->join('mst_question','mst_quiz_que_grade.question_id','=','mst_question.question_id')
                        ->join('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
                        ->groupBy('mst_competency.competency_id')
                        ->select('mst_competency.competency_id','mst_competency.competency_name',DB::raw('count(*) as total_soal'))
                        ->get();
                    $array_answered_question_id = $this->MstQuizAttempts
                        ->where('quiz_id',$quiz->quiz_id)
                        ->whereNotNull('time_finish')
                        ->get()->pluck('question_id')->toArray();
                    foreach ($temp2 as $item3) {
                        $item3->total_soal_dikerjakan = $this->MstQuizQueGrade
                            ->where('quiz_id',$quiz->quiz_id)
                            ->where('mst_question.job_id',$item2->job_id)
                            ->where('mst_question.competency_id',$item3->competency_id)
                            ->whereIn('mst_question.question_id',$array_answered_question_id)
                            ->join('mst_question','mst_quiz_que_grade.question_id','=','mst_question.question_id')
                            ->select(DB::raw('count(*) as total'))
                            ->first()->total;
                    }
                    
                    $item2->competencyQuizTotal = $temp2;
                }
                $item->jobQuestionTotal = $temp;
            }
            return view('bank-soal.dashboard',compact('functionQuestionTotal'));
        }
        return view('bank-soal.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $function_id = $request->old('function_id');
        $job_id = $request->old('job_id');
        $competency_id = $request->old('competency_id');
        $packages = $this->MstPackage->getData();
        $questionTypes = $this->MstQuestionType->getData();
        $moduls = $this->MstModul->getData();
        $competencies = $this->MstCompetencyDetail->getData(false,['job_id' => $job_id],['mstCompetency'])->pluck('mstCompetency')->unique('competency_id');
        $levels = $this->MstCompetencyDetail->getData(false,['job_id' => $job_id,'competency_id' => $competency_id],['mstLevel'])->pluck('mstLevel')->unique('level_id');
        $functions = $this->MstFunction->getData(false,[]);  
        $jobs = $this->MstJob->getData(false,['function_id' => $function_id]);  
        return view('questions.create', compact('packages','questionTypes','competencies','levels','moduls','functions','jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\QuestionStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $questionId = $this->MstQuestion->createData($request->all());
            $this->MstQuestionAnswer->createData($request, $questionId);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            $message = 'gagal membuat soal';
            return view('layouts.error-messages',compact('message'));
        }
        return redirect()->route('question.create')->with('success','Question berhasil ditambahkan');
    }

    /**
     * Store a newly created Loyalty Question in storage.
     *
     * @param  \Illuminate\Http\QuestionStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLoyaltyQuestion(Request $request){
        try {
            DB::beginTransaction();
            $questionId = $this->MstQuestion->createQuestionLoyalty($request->all());
            $this->MstQuestionAnswer->createQuestionLoyaltyAnswer($request->all(), $questionId);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            return response()->json(['message' => 'Process of creating a question failed']);
        }
        return response()->json(['message' => 'Process of creating a question success']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($question_id)
    {
        $packages = $this->MstPackage->getData();
        $questionTypes = $this->MstQuestionType->getData();
        $moduls = $this->MstModul->getData();
        $question = $this->MstQuestion->getSingleData(true,['question_id' => $question_id],['mstQuestionAnswers']);
        $competencies = $this->MstCompetencyDetail->getData(false,['job_id' => $question->job_id],['mstCompetency'])->pluck('mstCompetency')->unique('competency_id');
        $levels = $this->MstCompetencyDetail->getData(false,['job_id' => $question->job_id, 'competency_id' => $question->competency_id],['mstLevel'])->pluck('mstLevel')->unique('level_id');
        $jobs = $this->MstJob->getData(false,['function_id' => $question->function_id]);
        $functions = $this->MstFunction->getData();
        return view('questions.edit',compact('levels','moduls','competencies','question','packages','questionTypes','jobs','functions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\QuestionUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionUpdateRequest $request)
    { 
        if($this->MstQuestion->isRejectedLoyaltyQuestion($request->question_id)){
            return redirect()->back()->with('error','Rejected Loyalty Question can\'t be edit');
        }
        try {
            DB::beginTransaction();
            $this->MstQuestion->updateData($request->all());
            $this->MstQuestionAnswer->updateMultipleData($request);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            $message = 'gagal memperbarui soal';
            return view('layouts.error-messages',compact('message'));
        }
        return redirect()->route('package.show',['id' => $request->package_id])->with('success','data question berhasil diupdate');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id){
        $this->MstQuestion->unactivatedData($id);
        return redirect()->back()->with('error','data question berhasil dinonaktifkan');
        // return redirect()->route('package.show',['id' => $request->package_id]);
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->MstQuestion->activateData($id);
        return redirect()->back()->with('success','data question berhasil diaktifkan');
    }

    /**
     * Reject the loyalty question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function rejectLoyaltyQuestion($id){
        if($this->MstQuestion->isNotLoyaltyQuestion($id))
            return redirect()->back()->with('error','the Loyalty Question Rejection Process failed');
        if($this->MstQuestion->wasLoyaltyQuestionChecked($id))
            return redirect()->back()->with('error','the Loyalty Question Rejection Process failed');

        
        $this->MstQuestion->rejectLoyaltyQuestion($id);
        return redirect()->back()->with('success','Loyalty Question Rejected');
    }

    
    /**
     * Accept the loyalty question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function acceptLoyaltyQuestion($id){
        try {
            DB::beginTransaction();
            if($this->MstQuestion->isNotLoyaltyQuestion($id))
                return redirect()->back()->with('error','the Loyalty Question Accepting Process failed');
            if($this->MstQuestion->wasLoyaltyQuestionChecked($id))
                return redirect()->back()->with('error','the Loyalty Question Accepting Process failed');
            $this->MstQuestion->acceptLoyaltyQuestion($id);
            $question = $this->MstQuestion->getSingleData(true,['question_id' => $id]);
            $scoring = $this->MstScoring->getScoringToday(5,\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
            $this->MstEmployeeScoring->createEmployeeLoyaltyScore($id, $question->npk, $scoring->scoring_id,$scoring->score);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            return redirect()->back()->with('error','the Loyalty Question Accepting Process failed');
        }        
        return redirect()->back()->with('success','Loyalty Question Accepted');
    }

    public function getQuestionAjaxData(Request $request){
        if($request->type == 'job'){
            $jobs = $this->MstJob->getData(false,$request->only(['function_id']));
            $options = $jobs->map(function ($job) {
                $data['id'] = $job->job_id;
                $data['name'] = $job->job_name;
                return $data;
            });
        }
        else if($request->type == 'competency'){
            $competencies = $this->MstCompetencyDetail->getData(false,$request->only(['job_id']),['mstCompetency'])->pluck('mstCompetency')->unique('competency_id');
            $options = $competencies->map(function ($competency) {
                $data['id'] = $competency->competency_id;
                $data['name'] = $competency->competency_name;
                return $data;
            });
        }
        else if($request->type == 'level') {
            $levels = $this->MstCompetencyDetail->getData(false,$request->only(['job_id','competency_id']),['mstLevel'])->pluck('mstLevel')->unique('level_id');
            $options = $levels->map(function ($level) {
                $data['id'] = $level->level_id;
                $data['name'] = $level->level_name;
                return $data;
            });
        }
        $data = view('layouts.part.ajax-select',compact('options'))->render();
        return response()->json($data);
    }

    public function createWithExcel(Request $request){        
        $packages = $this->MstPackage->getData();
        $questionTypes = $this->MstQuestionType->getData();
        return view("questions.upload-excel", compact('packages','questionTypes'));
    }   

    public function storeWithExcel(QuestionExcelStoreRequest $request){
        $filepath = $request->file->move(public_path('\excel'),$request->file->getClientOriginalName());
        $excel = Importer::make('Excel');
        $excel->load($filepath);
        $excel->setSheet(1);
        $questions = $excel->getCollection();
        $request->session()->forget('questionsUploadExcel');
        $request->session()->put('questionsUploadExcel', $questions);
        
        $questionsLength = count($questions);

        $data['package_id'] = $request->input('package_id');
        $data['question_type_id'] = $request->input('question_type_id');
        $question_type_id = $request->input('question_type_id');
        $data = json_encode($data);
        File::delete(public_path('/excel').'/'.$request->file->getClientOriginalName());
        return view('questions.upload-excel-progress',compact('questionsLength','questions','data', 'question_type_id'));
    }

    public function StoreProcessWithExcel(Request $request){
        $data = $request->input('data');
        $data = json_decode($data, true);
        $requestQuestion = $request->session()->get('questionsUploadExcel')[$request->i];
        
        if($data['question_type_id'] == 1){

            $question['Function'] = $requestQuestion[0];
            $question['Modul'] = ($requestQuestion[1]);
            $question['Competency'] = strtoupper($requestQuestion[2]);
            $question['Level'] = $requestQuestion[3];
            $question['Job'] = strtoupper($requestQuestion[4]);
            $question['Question'] = $requestQuestion[5];
            $question['AnswerA'] = $requestQuestion[6];
            $question['AnswerB'] = $requestQuestion[7];
            $question['AnswerC'] = $requestQuestion[8];
            $question['AnswerD'] = $requestQuestion[9];
            $question['Answer'] = strtoupper($requestQuestion[10]);

            $item['question'] = 
            '<div class="row mb-5 mt-2">
                <div class="col-md-9">
                    <h5 class="mt-3">
                        '.$question['Question'].' ( Jawaban : '.$question['Answer'].')
                    </h5>
                    <ol style="list-style-type: upper-alpha;">
                        <li>'.$question['AnswerA'].'</li>
                        <li>'.$question['AnswerB'].'</li>
                        <li>'.$question['AnswerC'].'</li>
                        <li>'.$question['AnswerD'].'</li>
                    </ol>
                </div>
                <div class="col-md-3" style="border-left: 1px solid lightgray;">
                    <span class="mt-1" style="display:inline-block">'.$question['Job'].'</span><br>
                    <span class="mt-1" style="display:inline-block">'.$question['Competency'].'</span><br>
                    <span class="mt-1" style="display:inline-block">'.$question['Level'].'</span><br>
                    <span class="mt-1" style="display:inline-block">'.$question['Modul'].'</span><br>
                    <span class="mt-1" style="display:inline-block;">
                        start date - end date 
                    </span><br>
                </div>
            </div>';

            $level = $this->MstLevel->getSingleData(true,['level_name' => $question['Level']]);
            $competency = $this->MstCompetency->getSingleData(true, ['competency_name' => $question['Competency']]);
            $job = $this->MstJob->getSingleData(true, ['job_name' => $question['Job']]);
            $function = $this->MstFunction->getSingleData(true, ['function_name' => $question['Function']]);
            $modul = $this->MstModul->getSingleData(true, ['modul_name' => $question['Modul']]);
            
            if($job != null && $competency != null && $level != null && !$this->MstCompetencyDetail->CompetencyDetailIsExist($job->job_id,$competency->competency_id,$level->level_id)){
                // if($job->flag_active == 1 && $competency->flag_active == 1 && $level->flag_active == 1)
                    $this->MstCompetencyDetail->createData(['job_id' => $job->job_id,'competency_id' =>$competency->competency_id,'level_id' => $level->level_id]);          
            }

            $questionFalse = false;

            if (($job == null || $competency == null || $level == null || $function==null ||  $question['Question'] == "" || $question['AnswerA'] == "" || $question['AnswerB'] == "" || $question['AnswerD'] == "" || $question['AnswerD'] == "" || $question['Answer'] == "") || !($question['Answer'] == "A" || $question['Answer'] == "B" || $question['Answer'] == "C" || $question['Answer'] == "D" )) {
                $questionFalse = true;
            }
            // else if($modul == null && $question['Modul'] !== ""){
            //     $questionFalse = true;
            // }
            else if(!$this->MstCompetencyDetail->CompetencyDetailIsActive($job->job_id,$competency->competency_id,$level->level_id)){
                $questionFalse = true;
            }
            else if($job->function_id != $function->function_id){
                $questionFalse = true;
            }

            if($questionFalse){
                $item['result'] = false;
                $item['index'] = $request->i;
                $item = json_encode($item);
                $FailedQuestionUpload = collect();
                $FailedQuestionUpload->Function = $question['Function'];
                $FailedQuestionUpload->Modul = $question['Modul'];
                $FailedQuestionUpload->Competency = $question['Competency'];
                $FailedQuestionUpload->Level = $question['Level'];
                $FailedQuestionUpload->Job = $question['Job'];
                $FailedQuestionUpload->Question = $question['Question'];
                $FailedQuestionUpload->AnswerA = $question['AnswerA'];
                $FailedQuestionUpload->AnswerB = $question['AnswerB'];
                $FailedQuestionUpload->AnswerC = $question['AnswerC'];
                $FailedQuestionUpload->AnswerD = $question['AnswerD'];
                $FailedQuestionUpload->Answer = $question['Answer'];
                // $request->session()->push('arrayFailedQuestionUpload', $FailedQuestionUpload);
                return $item;
            }

        }

        elseif($data['question_type_id'] == 2){
            $question['Modul'] = ($requestQuestion[0]);
            $question['Question'] = $requestQuestion[1];
            $question['AnswerA'] = $requestQuestion[2];
            $question['AnswerB'] = $requestQuestion[3];
            $question['AnswerC'] = $requestQuestion[4];
            $question['AnswerD'] = $requestQuestion[5];
            $question['Answer'] = strtoupper($requestQuestion[6]);
            // $modul = $this->MstModul->getSingleData(true, ['modul_name' => $question['Modul']]);
            $item['question'] = 
            '<div class="row mb-5 mt-2">
                <div class="col-md-9">
                    <h5 class="mt-3">
                        '.$question['Question'].' ( Jawaban : '.$question['Answer'].')
                    </h5>
                    <ol style="list-style-type: upper-alpha;">
                        <li>'.$question['AnswerA'].'</li>
                        <li>'.$question['AnswerB'].'</li>
                        <li>'.$question['AnswerC'].'</li>
                        <li>'.$question['AnswerD'].'</li>
                    </ol>
                </div>
                <div class="col-md-3" style="border-left: 1px solid lightgray;">
                    <span class="mt-1" style="display:inline-block">'.$question['Modul'].'</span><br>

                    <span class="mt-1" style="display:inline-block;">
                        start date - end date 
                    </span><br>
                </div>
            </div>';

            // if (($question['Question'] == "" || $question['AnswerA'] == "" || $question['AnswerB'] == "" || $question['AnswerD'] == "" || $question['AnswerD'] == "" || $question['Answer'] == "") || !($question['Answer'] == "A" || $question['Answer'] == "B" || $question['Answer'] == "C" || $question['Answer'] == "D" ) || ($modul == null && $question['Modul'] !== "")) {
            if (($question['Question'] == "" || $question['AnswerA'] == "" || $question['AnswerB'] == "" || $question['AnswerD'] == "" || $question['AnswerD'] == "" || $question['Answer'] == "") || !($question['Answer'] == "A" || $question['Answer'] == "B" || $question['Answer'] == "C" || $question['Answer'] == "D" )) {
                $item['result'] = false;
                $item = json_encode($item);
                $FailedQuestionUpload = collect();
                $FailedQuestionUpload->Modul = $question['Modul'];
                $FailedQuestionUpload->Question = $question['Question'];
                $FailedQuestionUpload->AnswerA = $question['AnswerA'];
                $FailedQuestionUpload->AnswerB = $question['AnswerB'];
                $FailedQuestionUpload->AnswerC = $question['AnswerC'];
                $FailedQuestionUpload->AnswerD = $question['AnswerD'];
                $FailedQuestionUpload->Answer = $question['Answer'];;
                // $request->session()->push('arrayFailedQuestionUpload', $FailedQuestionUpload);
                return $item;
            }
        }
        $item['result'] = true;    
        $item['index'] = $request->i;    
        // $request->session()->push('arraySuccessQuestionUpload', $question);
        $item = json_encode($item);
        return $item;
    }

    public function StoreCommiteWithExcel(Request $request){
        $data = $request->input('data');
        $data = json_decode($data, true);
        // $arraySuccessQuestionUpload = $request->session()->get('arraySuccessQuestionUpload');
        // $request->session()->forget('arraySuccessQuestionUpload');
        // $request->session()->get('questionsUploadExcel')[$request->i];
        $arrayIndexSuccessUpload = $request->arrayIndexSuccessUpload;
        try {
            DB::beginTransaction();
            foreach($arrayIndexSuccessUpload as $index){
                if($data['question_type_id'] == 1){
                    $requestQuestion = $request->session()->get('questionsUploadExcel')[$index];

                    $question['Function'] = $requestQuestion[0];
                    $question['Modul'] = ($requestQuestion[1]);
                    $question['Competency'] = strtoupper($requestQuestion[2]);
                    $question['Level'] = $requestQuestion[3];
                    $question['Job'] = strtoupper($requestQuestion[4]);
                    $question['Question'] = $requestQuestion[5];
                    $question['AnswerA'] = $requestQuestion[6];
                    $question['AnswerB'] = $requestQuestion[7];
                    $question['AnswerC'] = $requestQuestion[8];
                    $question['AnswerD'] = $requestQuestion[9];
                    $question['Answer'] = strtoupper($requestQuestion[10]);

                    $level = $this->MstLevel->getSingleData(true,['level_name' => $question['Level']]);
                    $competency = $this->MstCompetency->getSingleData(true, ['competency_name' => $question['Competency']]);
                    $job = $this->MstJob->getSingleData(true, ['job_name' => $question['Job']]);
                    $function = $this->MstFunction->getSingleData(true, ['function_name' => $question['Function']]);
                    // if($question['Modul'] == ""){
                    //     $modul_id =  0;
                    // }
                    // else{
                    //     $modul = $this->MstModul->getSingleData(true, ['modul_name' => $question['Modul']]);
                    //     $modul_id = $modul->modul_id;
                    // }
                    $modul_id =  0;
                    $questionId = $this->MstQuestion->createData(['package_id' => $data['package_id'],'question_type_id' => $data['question_type_id'], 'job_id' => $job->job_id, 'competency_id' => $competency->competency_id,'level_id' => $level->level_id,'modul_id' => $modul_id, 'question' => $question['Question']]);
                    $this->MstQuestionAnswer->createQuestionAnswerWithExcelUpload($question, $questionId);
                }
                elseif($data['question_type_id'] == 2){

                    $requestQuestion = $request->session()->get('questionsUploadExcel')[$index];

                    $question['Modul'] = ($requestQuestion[0]);
                    $question['Question'] = $requestQuestion[1];
                    $question['AnswerA'] = $requestQuestion[2];
                    $question['AnswerB'] = $requestQuestion[3];
                    $question['AnswerC'] = $requestQuestion[4];
                    $question['AnswerD'] = $requestQuestion[5];
                    $question['Answer'] = strtoupper($requestQuestion[6]);
                    // if($question['Modul'] == ""){
                    //     $modul_id =  0;
                    // }
                    // else{
                    //     $modul = $this->MstModul->getSingleData(true, ['modul_name' => $question['Modul']]);
                    //     $modul_id = $modul->modul_id;
                    // }
                    $modul_id =  0;
                    $questionId = $this->MstQuestion->createData(['package_id' => $data['package_id'],'question_type_id' => $data['question_type_id'],'modul_id' => $modul_id, 'question' => $question['Question']]);
                    $this->MstQuestionAnswer->createQuestionAnswerWithExcelUpload($question, $questionId);
                }
            }
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            return 0;
        }
        return 1;
    }

    public function downloadFailedQuestionUpload(Request $request){
        $question_type_id = $request->input('question_type_id');

        $serialiserCompetencies = new Serialiser_FailedUploadQuestion();
        $serialieserGeneral = New Serialiser_FailedUploadQuestion2();
        $arrayIndexFailedUpload = $request->arrayIndexFailedUpload;
        $arrayIndexFailedUpload = explode(",",$arrayIndexFailedUpload);
        $arrayFailedQuestionUpload = [];
        foreach($arrayIndexFailedUpload as $index){
            if($question_type_id == 1){
                $requestQuestion = $request->session()->get('questionsUploadExcel')[$index];

                $question['Function'] = $requestQuestion[0];
                $question['Modul'] = ($requestQuestion[1]);
                $question['Competency'] = strtoupper($requestQuestion[2]);
                $question['Level'] = $requestQuestion[3];
                $question['Job'] = strtoupper($requestQuestion[4]);
                $question['Question'] = $requestQuestion[5];
                $question['AnswerA'] = $requestQuestion[6];
                $question['AnswerB'] = $requestQuestion[7];
                $question['AnswerC'] = $requestQuestion[8];
                $question['AnswerD'] = $requestQuestion[9];
                $question['Answer'] = strtoupper($requestQuestion[10]);

                $FailedQuestionUpload = collect();
                $FailedQuestionUpload->Function = $question['Function'];
                $FailedQuestionUpload->Modul = $question['Modul'];
                $FailedQuestionUpload->Competency = $question['Competency'];
                $FailedQuestionUpload->Level = $question['Level'];
                $FailedQuestionUpload->Job = $question['Job'];
                $FailedQuestionUpload->Question = $question['Question'];
                $FailedQuestionUpload->AnswerA = $question['AnswerA'];
                $FailedQuestionUpload->AnswerB = $question['AnswerB'];
                $FailedQuestionUpload->AnswerC = $question['AnswerC'];
                $FailedQuestionUpload->AnswerD = $question['AnswerD'];
                $FailedQuestionUpload->Answer = $question['Answer'];

                array_push($arrayFailedQuestionUpload, $FailedQuestionUpload);
            }
            elseif($question_type_id == 2){
                $requestQuestion = $request->session()->get('questionsUploadExcel')[$index];

                $question['Modul'] = ($requestQuestion[0]);
                $question['Question'] = $requestQuestion[1];
                $question['AnswerA'] = $requestQuestion[2];
                $question['AnswerB'] = $requestQuestion[3];
                $question['AnswerC'] = $requestQuestion[4];
                $question['AnswerD'] = $requestQuestion[5];
                $question['Answer'] = strtoupper($requestQuestion[6]);

                $FailedQuestionUpload = collect();
                $FailedQuestionUpload->Modul = $question['Modul'];
                $FailedQuestionUpload->Question = $question['Question'];
                $FailedQuestionUpload->AnswerA = $question['AnswerA'];
                $FailedQuestionUpload->AnswerB = $question['AnswerB'];
                $FailedQuestionUpload->AnswerC = $question['AnswerC'];
                $FailedQuestionUpload->AnswerD = $question['AnswerD'];
                $FailedQuestionUpload->Answer = $question['Answer'];;
                
                array_push($arrayFailedQuestionUpload, $FailedQuestionUpload);
            }
        }


        $questions = collect($arrayFailedQuestionUpload);
        
        if($question_type_id == 1)
        {
            $excel = Exporter::make('Excel');
            $excel->load($questions)->setSerialiser($serialiserCompetencies);
            
            return $excel->stream('FailedQuestionUploadCompetencies.xlsx');
        }
        elseif($question_type_id == 2)
        {  
            $excel = Exporter::make('Excel');
            $excel->load($questions)->setSerialiser($serialieserGeneral);

            return $excel->stream('FailedQuestionUploadGeneral.xlsx');
        }
    }

    public function getFunctionJob()
    {
        $functions = $this->MstFunction->getData(false,[],['mstJobs']);
        return json_decode($functions);
    }
}
