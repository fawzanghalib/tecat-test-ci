<?php

namespace App\Http\Controllers;

use App\Serialiser\Serialiser_CAR;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\MstQuiz;
use App\Model\MstJob;
use App\Model\MstFunction;
use App\Model\MstQuizQueGrade;
use App\Model\MstQuestion;
use App\Model\MstQuizAttempts;
use App\Model\MstCompetencyDetail;
use App\Model\MstCompetency;
use App\Model\MstQuizRespons;
use App\Model\MstScoring;
use App\Model\MstQuizGrade;
use App\Model\MstLevel;
use DateTime;
use File;
use Exporter;

class CompetencyAnalysisResults extends Controller
{
    private $MstQuiz;
    private $MstJob;
    private $MstFunction;
    private $MstQuizQueGrade;
    private $MstQuestion;
    private $MstQuizAttempts;
    private $MstCompetencyDetail;
    private $MstCompetency;
    private $MstQuizRespons;
    private $MstScoring;
    private $MstQuizGrade;
    private $MstLevel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MstQuiz $MstQuiz,MstJob $MstJob,MstFunction $MstFunction,MstQuizQueGrade $MstQuizQueGrade,MstQuestion $MstQuestion,MstCompetencyDetail $MstCompetencyDetail,MstCompetency $MstCompetency,MstQuizAttempts $MstQuizAttempts,MstQuizRespons $MstQuizRespons,MstScoring $MstScoring,MstQuizGrade $MstQuizGrade,MstLevel $MstLevel)
    {
        $this->MstQuiz = $MstQuiz;
        $this->MstJob = $MstJob;
        $this->MstFunction = $MstFunction;
        $this->MstQuizQueGrade = $MstQuizQueGrade;
        $this->MstQuestion = $MstQuestion;
        $this->MstQuizAttempts = $MstQuizAttempts;
        $this->MstCompetencyDetail = $MstCompetencyDetail;
        $this->MstCompetency = $MstCompetency;
        $this->MstQuizRespons = $MstQuizRespons;
        $this->MstScoring = $MstScoring;
        $this->MstQuizGrade = $MstQuizGrade;    
        $this->MstLevel = $MstLevel;    
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $functions = $this->MstFunction->getData();
        $jobs = $this->MstJob->getData();
        $competencies = $this->MstCompetency->getData();
        $levels = $this->MstLevel->getData();
        $data = $this->MstQuizAttempts->getCompetencyAnalysisResults($request);
        //dd($request);
        if($request->action == 'download'){
            $serialiser = new Serialiser_CAR();
            $excel = Exporter::make('Excel');
            $excel->load($data)->setSerialiser($serialiser);
            return $excel->stream('CompetencyAnalysisResults.xlsx');
        }
        return view('competency-analysis-results.index',compact('functions','jobs','competencies','levels','request','data'));
    }

    // TEST API
    public function getData(Request $request){
        return $data = $this->MstQuizAttempts->getCompetencyAnalysisResults($request);
    }
}
