<?php

namespace App\Http\Controllers;

use App\Model\MstLevel;
use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exporter;

use Auth;


class LevelController extends Controller
{
    private $MstLevel;

    public function __construct(MstLevel $MstLevel)
    {
        $this->MstLevel = $MstLevel;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $levels = $this->MstLevel->getFilterData($request);
        return view('level.index',compact('levels','request'));
    }

    public function create()
    {
        return view('level.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'level_name' => 'required|string|max:100',
            'description' => 'required|string|max:150',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return redirect()
                ->back('level.create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $level_id = $this->MstLevel->createData($request);
            return redirect()->route('level')->with('success', 'Data berhasil ditambahkan!');
        }
    }
    public function edit($id)
    {
        $cek = $this->MstLevel->where('level_id',$id)->first();
        if($cek == null)
        {
            return redirect()->route('level')->with('IDNotFound', 'ID Tidak Ditemukan!');
        }
        else
        {
            $level = $this->MstLevel->getSingleData(true,['level_id' => $id]);  
            return view('level.edit', compact('level'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'level_name' => 'required|string|max:100',
            'description' => 'required|string|max:150',
            'flag_active' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        else{
            
            $this->MstLevel->updateData($request, $id);
            return redirect()->route('level')->with('success','data level berhasil diupdate');

        }
    }
    public function unactivated($id)
    {
        $this->MstLevel->unactivatedData($id);
        return redirect()->route('level')->with('error','data level berhasil dinonaktifkan');
        // return redirect()->route('level')->with('success', 'Level ID : '.$id.' berhasil dinonaktifkan');
    }
    public function activate($id)
    {
        $this->MstLevel->activateData($id);
        return redirect()->route('level')->with('success','data level berhasil diaktifkan');
        // return redirect()->route('level')->with('success', 'Level ID : '.$id.' berhasil diaktifkan');
    }

}