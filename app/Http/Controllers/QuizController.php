<?php

namespace App\Http\Controllers;
use App\Serialiser\Serialiser_Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Model\MstQuiz;
use App\Model\MstQuizQueGrade;
use App\Model\MstQuestion;
use App\Model\MstCompetency;
use App\Model\MstLevel;
use App\Model\MstJob;
use Auth;
use File;
use Exporter;

class QuizController extends Controller
{
    private $MstQuiz;
    private $MstQuizQueGrade;
    private $MstQuestion;
    private $MstCompetency;
    private $MstLevel;
    private $MstJob;
    private $MessageBag;

    public function __construct(MstQuiz $MstQuiz,MstQuizQueGrade $MstQuizQueGrade,MstQuestion $MstQuestion,MstCompetency $MstCompetency,MstLevel $MstLevel,MstJob $MstJob,MessageBag $MessageBag)
    {
        $this->MstQuiz = $MstQuiz;
        $this->MstQuizQueGrade = $MstQuizQueGrade;
        $this->MstQuestion = $MstQuestion;
        $this->MessageBag = $MessageBag;
        $this->MstCompetency = $MstCompetency;
        $this->MstLevel = $MstLevel;
        $this->MstJob = $MstJob;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $quizzes = $this->MstQuiz->getSearchAndFilterData($request);
        return view('quizzes.index', compact('quizzes','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('quizzes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->MessageBag;
        $validator = Validator::make($request->all(), [
            'quiz_name' => 'required|string|max:50',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $validator->getMessageBag()->add('quiz_name', 'Error message');
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()
                ->route('quiz.create')
                ->withErrors($validator->messages())
                ->withInput();
        }

        if ($this->MstQuiz->quizDateIsUnique($request)) {
            $error =  $this->MessageBag->add('date','tidak bisa menambahkan quiz karena range tanggal sudah digunakan');
            $request->flash();
            return redirect()
                ->route('quiz.create')
                ->withErrors($error)
                ->withInput();
        }
        elseif ($validator->fails()) {
            $request->flash();
            return redirect()
                ->route('quiz.create')
                ->withErrors($error)
                ->withInput();
        }
        
        $this->MstQuiz->createData($request);
        return redirect()->route('quiz')->with('success','data quiz berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $competencies = $this->MstCompetency->getData();
        $levels = $this->MstLevel->getData();
        $jobs = $this->MstJob->getData();
        $quiz = $this->MstQuiz->getSingleData(true,['quiz_id' => $id]);
        if(!isset($quiz)){
            return redirect()->route('quiz');
        }
        
        $questions = $this->MstQuestion->getDataAndQuestionAnswerWithQuizId($id,$request);
        // dd($levels);
        $data = $this->MstQuestion->downloadData($id,$request);
        
        if($request->action == 'download'){
            $serialiser = new Serialiser_Quiz();
            $excel = Exporter::make('Excel');
            // $excel->load($data)->setSerialiser($serialiser);
            $excel->load($data);
            return $excel->stream('ListQuiz.xlsx');
        }
        
        return view('quizzes.show',compact('quiz','questions','levels','jobs','competencies','request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$from)
    {
        $quiz = $this->MstQuiz->getSingleData(true,['quiz_id' => $id]);
        return view('quizzes.edit',compact('quiz','from'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'quiz_name' => 'required|string|max:50',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return redirect()
                ->route('quiz.edit', ['id' => $request->quiz_id,'from' => $reques->from])
                ->withErrors($validator)
                ->withInput();
        }
        
        if ($this->MstQuiz->quizDateIsUnique($request)) {
            $error =  $this->MessageBag->add('date','tidak bisa menambahkan quiz karena range tanggal sudah digunakan');
            $request->flash();
            return redirect()
                ->route('quiz.edit', ['id' => $request->quiz_id,'from' => $request->from])
                ->withErrors($error)
                ->withInput();
        }
            
        $this->MstQuiz->updateData($request);
        // return redirect()->route('quiz.show', ['id' => $request->quiz_id]);
        if($request->from == 'quiz')
            return redirect()->route('quiz')->with('success','data quiz berhasil diupdate');
        else
            return redirect()->route('quiz.show',['id' => $request->quiz_id])->with('success','data quiz berhasil diupdate');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->MstQuiz->unactivatedData($id);
        return redirect()->route('quiz')->with('error','data quiz berhasil dinonaktifkan');
    }

    /**
     * Activate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        if ($this->MstQuiz->quizDateIsUnique($request)) {
            return redirect()->route('quiz')->with('error','Quiz aktif dengan tanggal tersebut sudah tersedia');
        }
        $this->MstQuiz->activateData($request->quiz_id);
        return redirect()->route('quiz')->with('success','data quiz berhasil diaktifkan');
    }
}
