<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Competency\CompetencyStoreRequest;
use App\Http\Requests\Competency\CompetencyUpdateRequest;
use App\Model\MstCompetency;
use Auth;
// use Exporter;

class CompetencyController extends Controller
{
    private $MstCompetency;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MstCompetency $MstCompetency)
    {
        $this->MstCompetency = $MstCompetency;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $competencies = $this->MstCompetency->getFilterData($request);
        return view('competencies.index',compact('competencies','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('competencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CompetencyStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompetencyStoreRequest $request)
    {
        $competency_id = $this->MstCompetency->createData($request);
        return redirect()->route('competency');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competency = $this->MstCompetency->getSingleData(true,['competency_id' => $id]);
        if(!isset($competency)){ return 404; }
        return view('competencies.edit', compact('competency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CompetencyUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompetencyUpdateRequest $request, $id)
    {
        $this->MstCompetency->updateData($request, $id);
        return redirect()->route('competency')->with('success','data competency berhasil diupdate');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->MstCompetency->unactivatedData($id);
        return redirect()->route('competency')->with('error','data competency berhasil dinonaktifkan');
    }

    /**
     * Activate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->MstCompetency->activateData($id);
        return redirect()->route('competency')->with('success','data competency berhasil diaktifkan');
    }

    public function downloadData(){
        // $excel = Exporter::make('Excel');
        // $excel->load($this->MstCompetency->getData());
        // return $excel->stream('Competency.xlsx');
    }
}
