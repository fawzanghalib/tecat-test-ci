<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Functions\FunctionStoreRequest;
use App\Http\Requests\Functions\FunctionUpdateRequest;
use App\Model\MstFunction;
use App\Model\MstJob;
use Auth;
use Exporter;

class FunctionController extends Controller
{
    private $MstFunctoin;
    private $MstJob;

    public function __construct(MstFunction $MstFunction, MstJob $MstJob){
        $this->MstFunction = $MstFunction;
        $this->MstJob = $MstJob;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Function with jobs list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $functions = $this->MstFunction->getFilterData();
        return view('functions.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('functions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\FunctionStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FunctionStoreRequest $request)
    {
        $this->MstFunction->createData($request);
        return redirect()->route('function')->with('success','data function berhasil ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $function = $this->MstFunction->getSingleData(true,['function_id' => $id]);
        if(!isset($function)){return 404;}
        return view('functions.edit', compact('function'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\FunctionUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FunctionUpdateRequest $request, $id)
    {
        $this->MstFunction->updateData($request,$id);
        return redirect()->route('function')->with('success','data function berhasil diupdate');
    }

    /**
     * Activate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->MstFunction->activateData($id);
        return redirect()->route('function')->with('success','data function berhasil diaktifkan');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->MstFunction->unactivatedData($id);
        return redirect()->route('function')->with('error','data function berhasil dinonaktifkan');
    }

    public function downloadData(){
        $excel = Exporter::make('Excel');
        $excel->load($this->MstFunction->getData(true));
        return $excel->stream('Function.xlsx');
    }
}
