<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Scoring\ScoringStoreRequest;
use App\Http\Requests\Scoring\ScoringUpdateRequest;
use App\Model\MstScoring;
use Auth;
use Exporter;


class ScoringController extends Controller
{
    private $MstScoring;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MstScoring $MstScoring)
    {
        $this->MstScoring = $MstScoring;
        $this->middleware('auth');
    }

    public function index(Request $request){
        $scorings = $this->MstScoring->getFilterData($request);
        return view('scoring.index', compact('scorings','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('scoring.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ScoringStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScoringStoreRequest $request)
    {
        $this->MstScoring->createData($request);
        return redirect()->route('scoring')->with('success','data scoring berhasil ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $scoring = $this->MstScoring->getSingleData(true,['scoring_id' => $id]);
        return view('scoring.edit', compact('scoring'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ScoringUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ScoringUpdateRequest $request)
    {
        $this->MstScoring->updateData($request);
        return redirect()->route('scoring')->with('success','data scoring berhasil diupdate');
    }

    /**
     * Activate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->MstScoring->activateData($id);
        return redirect()->route('scoring')->with('success','data scoring berhasil diaktifkan');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->MstScoring->unactivatedData($id);
        return redirect()->route('scoring')->with('error','data scoring berhasil dinonaktifkan');
    }

    public function downloadData(){
        $excel = Exporter::make('Excel');
        $excel->load($this->MstScoring->getData());
        return $excel->stream('Scoring.xlsx');
    }
}
