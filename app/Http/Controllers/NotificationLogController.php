<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\MstNotificationLog;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class NotificationLogController extends Controller
{
    private $MstNotificationLog;

    public function __construct(MstNotificationLog $MstNotificationLog)
    {
        $this->MstNotificationLog = $MstNotificationLog;
    }
    
    public function index(Request $request)
    {   
        $data = $this->MstNotificationLog->getSearchAndFilterData($request);
   
        return view('customize-notifications.log',compact('data','request'));
    }

    public function storeLog(Request $request){
        $data = $this->MstNotificationLog->createData($request);
        return "success";
    }
}
