<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Modul\ModulStoreRequest;
use App\Http\Requests\Modul\ModulUpdateRequest;
use App\Model\MstModul;
use Auth;
use Session;
use Exporter;

class ModulController extends Controller
{
    private $MstModul;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MstModul $MstModul)
    {
        $this->MstModul = $MstModul;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $moduls = $this->MstModul->getFilterData($request);
        return view('moduls.index',compact('moduls','request'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('moduls.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ModulStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModulStoreRequest $request)
    {
        $modul_id = $this->MstModul->createData($request);
        return redirect()->route('modul')->with('success','data modul berhasil ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modul = $this->MstModul->getSingleData(true,['modul_id' => $id]);
        if(!isset($modul)){ return 404; }
        return view('moduls.edit', compact('modul'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ModulUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->MstModul->updateData($request);
        return redirect()->route('modul')->with('success','data modul berhasil diupdate');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->MstModul->unactivatedData($id);
        return redirect()->route('modul')->with('error','data modul berhasil dinonaktifkan');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->MstModul->activateData($id);
        return redirect()->route('modul')->with('success','data modul berhasil diaktifkan');
    }

    public function downloadData(){
        $excel = Exporter::make('Excel');
        $excel->load($this->MstModul->getData());
        return $excel->stream('Modul.xlsx');
    }
}
