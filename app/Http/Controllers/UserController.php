<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\User;
use Auth;
use Hash;

class UserController extends Controller
{
    private $User;

    public function __construct(User $User)
    {
        $this->User = $User;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin',['except' => ['changePassword','updatePassword']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->User->getData();
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for change user password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword()
    {
        return view('users.change-password');
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|string|min:8|max:14confirmed|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
            'new_confirm_password' => 'required|string|min:8|max:14confirmed|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors(["Password harus mengandung huruf, angka, spesial karakter, dan capital."]);
        }
        else
        {
            if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->withErrors(["Password lama yang anda masukan salah. Mohon ulang kembali."]);
            }
                
            if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
                //Current password and new password are same
                return redirect()->back()->withErrors(["Password baru tidak boleh sama dengan password lama. Tolong ganti password baru anda."]);
            }
            
            if(!(strcmp($request->get('new_password'), $request->get('new_confirm_password'))) == 0){
                //New password and confirm password are not same
                return redirect()->back()->withErrors(["Password baru dan Ulang password harus sama. Tolong priksa kembali"]);
            }
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new_password'));
            $user->save();
                
            return  redirect()->route('user.change.password')->with('success','Password berhasil di perbarui');
        }

    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($id)
    {
        $this->User->unactivatedData($id);
        return redirect()->route('user')->with('error','user berhasil dinonaktifkan');
    }

    /**
     * Activate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->User->activateData($id);
        return redirect()->route('user')->with('success','user berhasil diaktifkan');
    }

    /**
     * data user dikirmkan untuk menampilkan siapa yang sedang login
     */
    public static function getUserData(){
        $userData = array(
            "name" => Auth::user()->name,
            "email" => Auth::user()->email,
            "id" => Auth::user()->id,
            'role' => Auth::user()->role
        );
        return $userData;
    }

}
