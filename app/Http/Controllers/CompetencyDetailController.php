<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CompetencyDetail\CompetencyDetailStoreRequest;
use App\Http\Requests\CompetencyDetail\CompetencyDetailUpdateRequest;
use App\Model\MstCompetencyDetail;
use App\Model\MstLevel;
use App\Model\MstCompetency;
use App\Model\MstQuizGrade;
use App\Model\User;
use App\Model\MstModul;
use App\Model\MstJob;
use App\Model\MstFunction;
use App\Helpers\Job;
use Auth;

class CompetencyDetailController extends Controller
{
    private $MstCompetencyDetail;
    private $MstLevel;
    private $MstCompetency;
    private $MstQuizGrade;
    private $User;
    private $MstJob;
    private $MstFunction;
    private $Job;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MstCompetencyDetail $MstCompetencyDetail,MstLevel $MstLevel,MstCompetency $MstCompetency,MstQuizGrade $MstQuizGrade,User $User,MstModul $MstModul,MstFunction $MstFunction,Job $Job,MstJob $MstJob)
    {
        $this->MstCompetencyDetail = $MstCompetencyDetail;
        $this->MstLevel = $MstLevel;
        $this->MstFunction = $MstFunction;
        $this->MstCompetency = $MstCompetency;
        $this->MstQuizGrade = $MstQuizGrade;
        $this->User = $User;
        $this->MstModul = $MstModul;
        $this->Job = $Job;
        $this->MstJob = $MstJob;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $competencyDetails = $this->MstCompetencyDetail->getMatrixCompetency($request);
        $competencies = $this->MstCompetency->getData();
        $levels = $this->MstLevel->getData();
        $jobs = $this->MstJob->getData();
        return view('competency-details.index',compact('competencyDetails','competencies','levels','jobs','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $functions = $this->MstFunction->getData(false,[],['mstJobs']);
        $competencies = $this->MstCompetency->getData();
        $levels = $this->MstLevel->getData();
        return view('competency-details.create',compact('levels','functions','competencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CompetencyDetailStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompetencyDetailStoreRequest $request)
    {
        $competency_id = $this->MstCompetencyDetail->createData($request->all());
        return redirect()->route('competency-detail.create')->with('success','data competency detail berhasil ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $job_id,$competency_id,$level_id
     * @return \Illuminate\Http\Response
     */
    public function edit($job_id,$competency_id,$level_id)
    {
        $competency = $this->MstCompetency->getSingleData(true,['competency_id' => $competency_id]);
        $job = $this->MstJob->getSingleData(true,['job_id' => $job_id]);
        $level = $this->MstLevel->getSingleData(true,['level_id' => $level_id]);
        $competency_detail = $this->MstCompetencyDetail->getSingleData(true,['job_id' => $job_id, 'competency_id' => $competency_id,'level_id' => $level_id]);
        if(!isset($competency_detail)){return 404;}
        return view('competency-details.edit',compact('competency','job','level','competency_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CompetencyDetailUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompetencyDetailUpdateRequest $request)
    {
        $competency_id = $this->MstCompetencyDetail->updateData($request);
        return redirect()->route('competency-detail')->with('success','data competency detail berhasil diupdate');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unactivated($job_id,$competency_id,$level_id)
    {
        $this->MstCompetencyDetail->unactivatedData($job_id,$competency_id,$level_id);
        return redirect()->route('competency-detail')->with('error','data matrix competency berhasil dinonaktifkan');
    }

    /**
     * Unactivated the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($job_id,$competency_id,$level_id)
    {
        $this->MstCompetencyDetail->activateData($job_id,$competency_id,$level_id);
        return redirect()->route('competency-detail')->with('success','data matrix competency berhasil diaktifkan');
    }
}
