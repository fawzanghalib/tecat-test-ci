<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;

class JobUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_name' => 'required|max:100',
            'description' => 'required|max:150',
            'function_id' => 'nullable|numeric',
            'flag_active' => 'required|in:1,0',
            'start_date' => 'required|date',
            'end_date' => 'date|after:start_date|nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'function_id.numeric' => 'Pilih function dengan benar',
            'job_name.required' => 'Isi deskripsi job',
            'job_name.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 100 karakter',
            'description.required' => 'Isi deskripsi job',
            'description.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 150 karakter',
            'flag_active.in' => 'Isi flag Active diantara active atau non active',
            'flag_active.required' => 'Isi flag Active diantara active atau non active',
            'start_date.required' => 'isi start date package',
            'start_date.date' => 'format tanggal pada start date salah',
            'end_date.date' => 'format tanggal pada end date salah',
            'end_date.after' => 'isilah end date lebih besar dari start date'
        ];
    }
}
