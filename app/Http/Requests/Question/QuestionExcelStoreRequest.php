<?php

namespace App\Http\Requests\Question;

use Illuminate\Foundation\Http\FormRequest;

class QuestionExcelStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_id' => 'required|numeric',
            'question_type_id' => 'required|numeric',
            'file'=>'required|mimes:xls,xlsx',
            // 'file'=>'required|mimes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'package_id.required' => 'Pilih salah satu package yang tersedia',
            'package_id.numeric' => 'Pilih salah satu package yang tersedia',
            'question_type_id.required' => 'Pilih salah satu question type yang tersedia',
            'question_type_id.numeric' => 'Pilih salah satu question type yang tersedia',
            'file.required' => 'Pilih file dalam format xls atau xlsx',
            'file.mimes' => 'Pilih file dalam format xls atau xlsx',
        ];
    }
}
