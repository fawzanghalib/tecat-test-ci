<?php

namespace App\Http\Requests\Question;

use Illuminate\Foundation\Http\FormRequest;

class QuestionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_id' => 'required|numeric',
            'package_id' => 'required|numeric',
            // 'function_id' => 'required_if:question_type_id,==,1|nullable|numeric',
            'job_id' => 'required_if:question_type_id,==,1|nullable|numeric',
            'question_type_id' => 'required|numeric',
            'competency_id' => 'required_if:question_type_id,==,1|nullable|numeric',
            'modul_id' => 'nullable|numeric',
            'level_id' => 'required_if:question_type_id,==,1|nullable|numeric',
            'question' => 'required|string',
            'answer_1' => 'required|string',
            'answer_2' => 'required|string',
            'answer_3' => 'required|string',
            'answer_4' => 'required|string',
            'answer_1_id' => 'required|numeric',
            'answer_2_id' => 'required|numeric',
            'answer_3_id' => 'required|numeric',
            'answer_4_id' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'date|after:start_date|nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'package_id.required' => 'Pilih salah satu package yang tersedia',
            // 'function_id.required' => 'Pilih salah satu job yang tersedia',
            'job_id.required' => 'Pilih salah satu job yang tersedia',
            'competency_id.required_if' => 'Pilih salah satu competency yang tersedia',
            'level_id.required_if' => 'Pilih salah satu level yang tersedia',
            'package_id.numeric' => 'Pilih salah satu package yang tersedia',
            // 'function_id.numeric' => 'Pilih salah satu function yang tersedia',
            'job_id.numeric' => 'Pilih salah satu job yang tersedia',
            'level_id.numeric' => 'Pilih salah satu level yang tersedia',
            'competency_id.numeric' => 'Pilih salah satu competency yang tersedia',
        ];
    }
}
