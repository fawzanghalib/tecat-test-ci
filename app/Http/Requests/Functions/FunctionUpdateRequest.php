<?php

namespace App\Http\Requests\Functions;

use Illuminate\Foundation\Http\FormRequest;

class FunctionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'function_name' => 'required|max:50',
            'description' => 'required|max:250',
            // 'function_id' => 'required|numeric',
            'flag_active' => 'required|in:1,0',
            'start_date' => 'required|date',
            'end_date' => 'date|after:start_date|nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'function_name.required' => 'Isi nama function',
            'function_name.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 50 karakter',
            'description.required' => 'Isi deskripsi job',
            'description.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 150 karakter',
            'flag_active.in' => 'Isi flag Active diantara active atau non active',
            // 'function_id.required' => 'Isi function',
            // 'function_id.number' => 'Isi function',
            'flag_active.required' => 'Isi flag Active diantara active atau non active',
            'start_date.required' => 'isi start date package',
            'start_date.date' => 'format tanggal pada start date salah',
            'end_date.date' => 'format tanggal pada end date salah',
            'end_date.after' => 'isilah end date lebih besar dari start date'
        ];
    }
}
