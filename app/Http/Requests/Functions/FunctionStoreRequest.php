<?php

namespace App\Http\Requests\Functions;

use Illuminate\Foundation\Http\FormRequest;

class FunctionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'function_name' => 'required|max:50',
            'description' => 'required|max:250',
            'start_date' => 'required|date',
            'end_date' => 'date|after:start_date|nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'function_name.required' => 'Isi nama function',
            'function_name.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 50 karakter',
            'description.required' => 'Isi deskripsi job',
            'description.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 150 karakter',
            'start_date.required' => 'isi start date package',
            'start_date.date' => 'format tanggal pada start date salah',
            'end_date.date' => 'format tanggal pada end date salah',
            'end_date.after' => 'isilah end date lebih besar dari start date'
        ];
    }
}
