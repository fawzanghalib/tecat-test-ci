<?php

namespace App\Http\Requests\Scoring;

use Illuminate\Foundation\Http\FormRequest;

class ScoringStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'scoring_type' => 'required',
            'type' => 'required|numeric|in:1,2,3,4,5',
            'score' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'date|after:start_date|required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'scoring_type.required' => 'Isi nama scoring type',
            'type.numeric' => 'Pilihlah type scoring dengan benar',
            'type.in' => 'Pilihlah type scoring dengan benar',
            'type.required' => 'Pilihlah type scoring dengan benar',
            'score.required' => 'Isilah jumlah score',
            'score.numeric' => 'Isilah jumlah score',
            'start_date.required' => 'isi start date package',
            'start_date.date' => 'format tanggal pada start date salah',
            'end_date.required' => 'isi end date package',
            'end_date.date' => 'format tanggal pada end date salah',
            'end_date.after' => 'isilah end date lebih besar dari start date'
        ];
    }
}
