<?php

namespace App\Http\Requests\CompetencyDetail;

use Illuminate\Foundation\Http\FormRequest;

class CompetencyDetailStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_id' => 'required|numeric|unique_with:mst_competency_details,competency_id,level_id',
            'competency_id' => 'required|numeric',
            'level_id' => 'required|numeric',
            'description' => 'required|max:250',
            'start_date' => 'required|date',
            'end_date' => 'date|after:start_date|nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'job_id.required' => 'Pilih salah satu job yang tersedia',
            'job_id.numeric' => 'Pilih salah satu job yang tersedia',
            'job_id.unique_with' => 'Matrix competency sudah ada',
            'competency_id.required' => 'Pilih salah satu competency yang tersedia',
            'competency_id.numeric' => 'Pilih salah satu competency yang tersedia',
            'competency_id.required' => 'Pilih salah satu competency yang tersedia',
            'description.required' => 'Isi deskripsi',
            'description.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 250 karakter',
            'start_date.required' => 'isi start date',
            'start_date.date' => 'format tanggal pada start date salah',
            'end_date.date' => 'format tanggal pada end date salah',
            'end_date.after' => 'isilah end date lebih besar dari start date'
        ];
    }
}
