<?php

namespace App\Http\Requests\Package;

use Illuminate\Foundation\Http\FormRequest;

class PackageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_name' => 'required|max:50',
            'description' => 'required|min:6|max:250',
            'start_date' => 'required|date',
            'end_date' => 'date|after:start_date|nullable',
        ];
    }

        /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'package_name.required' => 'Isi nama package',
            'package_name.max' => 'panjang maksimal nama yang dapat diberikan memiliki 50 karakter',
            'description.required' => 'Isi deskripsi package',
            'description.max' => 'panjang maksimal deskripsi yang dapat diberikan adalah 250 karakter',
            'description.min' => 'panjang minimal deskripsi yang dapat diberikan adalah 6 karakter',
            'start_date.required' => 'isi start date package',
            'start_date.date' => 'format tanggal pada start date salah',
            'end_date.date' => 'format tanggal pada end date salah',
            'end_date.after' => 'isilah end date lebih besar dari start date'
        ];
    }
}
