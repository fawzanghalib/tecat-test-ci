<?php

namespace App\Helpers;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Employee{

    private $MstCompetencyDetail;

    /**
     * Return Employee list.
     *
     * @return array
     */
    public static function getData(){
        ini_set('max_execution_time', 180);
        $client = new Client();
        $res = $client->request('GET', 'https://hdff.fa.ap1.oraclecloud.com/hcmRestApi/resources/11.13.18.05/', [
            'auth' => ['15854', 'Welcome123@'], 'allow_redirects' => false
        ]);

        $requestForTotalResult = $client->getAsync('https://hdff.fa.ap1.oraclecloud.com/hcmRestApi/resources/11.13.18.05/emps?offset=0&limit=1&totalResults=true' , [
            'auth' => ['15854', 'Welcome123@']])->then(
                function ($response) {
                    $emps = $response->getBody();
                    $emps = json_decode($emps, true);
                    return $emps['totalResults'];
                }, function ($exception) {
                    return $exception->getMessage();
                }
            );
        // $totalIterasi = $requestForTotalResult->wait();
        // $totalIterasi =  ceil($totalIterasi/500);
        $totalIterasi =  50;

        $offset = 0;

        for($i = 0; $i < $totalIterasi; $i++){
            $promise[$i] = $client->getAsync('https://hdff.fa.ap1.oraclecloud.com/hcmRestApi/resources/11.13.18.05/emps?expand=assignments&offset='.$offset.'&limit=10&totalResults=true' , [
                'auth' => ['15854', 'Welcome123@']])->then(
                    function ($response) {
                        $emps = $response->getBody();
                        $emps = json_decode($emps, true);
                        $empsList = [];
                        foreach ($emps['items'] as $emp) {
                            $item['npk'] = $emp['PersonNumber'];
                            $item['Name'] = $emp['DisplayName'];
                            $item['job_id'] = $emp['assignments'][0]['JobId'];
                            array_push($empsList,$item);
                        }
                        return $empsList;
                    }, function ($exception) {
                        return $exception->getMessage();
                    }
                );
            // $offset = $offset + 500;
            $offset = $offset + 10;
        }

        for($i = 0; $i < $totalIterasi; $i++){
            $array[$i] = $promise[$i]->wait();
        }

        $arr = [];

        for($i = 0; $i < $totalIterasi; $i++){
            $arr = array_merge($arr, $array[$i]);
        }
        return $arr;
    }
}