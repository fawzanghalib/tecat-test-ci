<?php

namespace App\Helpers;

class StaticFunction{
    /**
     * Return jobs list.
     *
     * @return array
     */
    public static function todayIs($day){
        if(date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())) == 1 && $day == 'Monday')
            return true;
        elseif(date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())) == 2 && $day == 'Tuesday')
            return true;
        elseif(date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())) == 3 && $day == 'Wednesday')
            return true;
        elseif(date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())) == 4 && $day == 'Thursdat')
            return true;
        elseif(date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())) == 5 && $day == 'Friday')
            return true;
        elseif(date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())) == 6 && $day == 'Saturday')
            return true;
        elseif(date('N', strtotime(\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())) == 7 && $day == 'Sunday')
            return true;
        
            return false;
    }
}