<?php

namespace App\Helpers;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Job{

    private $MstCompetencyDetail;

    /**
     * Return jobs list.
     *
     * @return array
     */
    public static function getData(){
        $client = new Client();
        $res = $client->request('GET', 'https://hdff.fa.ap1.oraclecloud.com/hcmRestApi/resources/11.13.18.05/', [
            'curl'  => [
                CURLOPT_PROXY => '10.7.21.247',
                CURLOPT_PROXYTYPE => 'HTTP',
                CURLOPT_PROXYPORT => 8080,
                CURLOPT_PROXYUSERPWD => '15854:Welcome123@',
            ],
        ]);

        $requestForTotalResult = $client->getAsync('https://hdff.fa.ap1.oraclecloud.com/hcmRestApi/resources/11.13.18.05/jobs?offset=0&limit=1&totalResults=true' , [
                'curl'  => [
                    CURLOPT_PROXY => '10.7.21.247',
                    CURLOPT_PROXYTYPE => 'HTTP',
                    CURLOPT_PROXYPORT => 8080,
                    CURLOPT_PROXYUSERPWD => '15854:Welcome123@',
                ],
            ])->then(
                function ($response) {
                    $jobs = $response->getBody();
                    $jobs = json_decode($jobs, true);
                    return $jobs['totalResults'];
                }, function ($exception) {
                    return $exception->getMessage();
                }
            );
        $totalIterasi = $requestForTotalResult->wait();
        $totalIterasi =  ceil($totalIterasi/500);

        $offset = 0;

        for($i = 0; $i < $totalIterasi; $i++){
            $promise[$i] = $client->getAsync('https://hdff.fa.ap1.oraclecloud.com/hcmRestApi/resources/11.13.18.05/jobs?offset='.$offset.'&limit=500&totalResults=true' , [
                    'curl'  => [
                        CURLOPT_PROXY => '10.7.21.247',
                        CURLOPT_PROXYTYPE => 'HTTP',
                        CURLOPT_PROXYPORT => 8080,
                        CURLOPT_PROXYUSERPWD => '15854:Welcome123@',
                    ],
                ])->then(
                    function ($response) {
                        $jobs = $response->getBody();
                        $jobs = json_decode($jobs, true);
                        $jobsList = [];
                        foreach ($jobs['items'] as $job) {
                            $item['JobId'] = $job['JobId'];
                            $item['Name'] = $job['Name'];
                            array_push($jobsList,$item);
                        }
                        return $jobsList;
                    }, function ($exception) {
                        return $exception->getMessage();
                    }
                );
            $offset = $offset + 500;
        }

        for($i = 0; $i < $totalIterasi; $i++){
            $array[$i] = $promise[$i]->wait();
        }

        $arr = [];

        for($i = 0; $i < $totalIterasi; $i++){
            $arr = array_merge($arr, $array[$i]);
        }
        return $arr;
    }
}