<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;


class MstQuizRespons extends Model
{
    protected $table = 'mst_quiz_respons';
    protected $primaryKey= 'respons_id';
    public $timestamps = true;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public function createData($user_id,$attempts_id,$quiz_id,$question_answer_id,$answerResult){
        $this->create(
            ['attempts_id' => $attempts_id,
            'answer' => $question_answer_id,
            'sum_score' => $answerResult,
            'created_by' => $user_id,
            'last_update_by' => $user_id]);
    }
    
}
