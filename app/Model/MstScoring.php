<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use EloquentFilter\Filterable;
use App\ModelFilters\MstScoringFilter;
use Kyslik\ColumnSortable\Sortable;
use DB;
use Auth;

/*
 *  column type
 *  1 = jawab benar
 *  2 = jawab benar tepat waktu
 *  3 = jawab salah
 *  4 = jawab salah tepat waktu
 *  5 = loyalti
 */

class MstScoring extends Model
{
    use Filterable;
    protected $table = 'mst_scoring';
    protected $primaryKey='scoring_id';
    public $timestamps = true;
    use Sortable;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public $sortable = [
        'scoring_type','score','score_type','type'
    ];

    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $scoring = $this;
        if(!$getNonActiveData){
            $scoring = $scoring->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('end_date')
                        ->orWhere('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                })
                ->where('flag_active',1);
        }
        
        if(array_key_exists('scoring_id',$conditions)){
            $scoring = $scoring->where('mst_scoring.scoring_id',$conditions['scoring_id']);
        }
        if(array_key_exists('scoring_name',$conditions)){
            $scoring = $scoring->where('mst_scoring.scoring_name',$conditions['scoring_name']);
        }

        if(!empty($relationships)){
            $scoring = $scoring->with($relationships);
        }

        return $scoring->get(['scoring_id','scoring_type','score','type','start_date','end_date','flag_active']);
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $scoring = $this;
        if(!$getNonActiveData){
            $scoring = $scoring->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('end_date')
                        ->orWhere('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                })
                ->where('flag_active',1);
        }
        
        if(array_key_exists('scoring_id',$conditions)){
            $scoring = $scoring->where('mst_scoring.scoring_id',$conditions['scoring_id']);
        }
        if(array_key_exists('scoring_name',$conditions)){
            $scoring = $scoring->where('mst_scoring.scoring_name',$conditions['scoring_name']);
        }

        if(!empty($relationships)){
            $scoring = $scoring->with($relationships);
        }

        return $scoring->first(['scoring_id','scoring_type','score','type','start_date','end_date','flag_active']);
    }

    public function getFilterData(Request $request){
        $MstScoringFilter = MstScoringFilter::class;
        return $this->filter($request->all(), $MstScoringFilter)
        ->sortable(['scoring_id','scoring_type','score','type','start_date','end_date','flag_active'])
            ->get();
    }

    public function getScoringToday($type,$time_start){
        return $this->where('type',$type)
            ->where('flag_active',1)
            ->where('start_date','<=',$time_start )
            ->where('end_date','>=',$time_start)
            ->select(array(
                'scoring_id', 'score',
                DB::raw("(abs(DATEDIFF(start_date,'".$time_start."')) + (DATEDIFF(end_date,'".$time_start."')))AS date_diff")))
            ->orderBy('date_diff','asc')
            ->first();
    }

    public function createData(Request $request){
        $this->create(
            ['scoring_type' => $request->input('scoring_type'),
            'type' => $request->input('type'),
            'score' => $request->input('score'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }

    public function updateData(Request $request){
        //mencegah scoring default untuk di edit
        if($request->input('scoring_id') == 1 || $request->input('scoring_id') == 2 || $request->input('scoring_id') == 3){
            $this->where('scoring_id',$request->input('scoring_id'))
                ->update([
                    'score' => $request->input('score'),
                    'last_update_by' => Auth::user()->id
                ]);
        }
        else{
            $this->where('scoring_id',$request->input('scoring_id'))
                ->update([
                    'scoring_type' => $request->input('scoring_type'),
                    'type' => $request->input('type'),
                    'score' => $request->input('score'),
                    'flag_active' => $request->input('flag_active'),
                    'start_date' => $request->input('start_date'),
                    'end_date' => $request->input('end_date'),
                    'last_update_by' => Auth::user()->id
            ]);
        }
    }

    public function unactivatedData($scoring_id){
        //mencegah scoring default untuk di non active kan
        if($scoring_id != 1 && $scoring_id != 2 && $scoring_id != 3){
            $this->where('scoring_id',$scoring_id)
                ->update([
                    'flag_active' => 0,
                    'last_update_by' => Auth::user()->id
                ]);
        }
    }
    public function activateData($scoring_id){
        //mencegah scoring default untuk di non active kan
        if($scoring_id != 1 && $scoring_id != 2 && $scoring_id != 3){
            $this->where('scoring_id',$scoring_id)
                ->update([
                    'flag_active' => 1,
                    'last_update_by' => Auth::user()->id
                ]);
        }
    }
}
