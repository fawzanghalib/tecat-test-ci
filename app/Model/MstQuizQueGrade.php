<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;


class MstQuizQueGrade extends Model
{
    protected $table = 'mst_quiz_que_grade';
    public $incrementing = false;
    protected $primaryKey= array('quiz_id','question_id');
    public $timestamps = true;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public function mstQuestion(){
        return $this->belongsTo('App\Model\MstQuestion','question_id','question_id');
    }

    public function getData(){
        return $this->get(['quiz_id','question_id']);
    }

    public function getQuestionWithQuizId($quiz_id){
        return $this->where('quiz_id',$quiz_id)->with('mstQuestion')->get()->pluck('mstQuestion');
    }

    public function getRandomQuestion($quiz_id,$arrayQuestionId){
        return $this->where('quiz_id',$quiz_id)
            ->orderByRaw('RAND()')->take(1)
            ->first(['quiz_id','question_id']);
    }

    public function createData(Request $request){
        foreach ($request->input('question') as $key => $question_id) {
            $this->create(
                ['quiz_id' => $request->input('quiz_id'),
                'question_id' => $question_id,
                'created_by' => Auth::user()->id,
                'last_update_by' => Auth::user()->id,]);
        }
    }

    public function destroyData(Request $request){
        $this->where('quiz_id',$request->quiz_id)
            ->where('question_id',$request->question_id)
            ->delete();
    }
}
