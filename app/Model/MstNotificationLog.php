<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
use DateTime;
use DB;
use Auth;

class MstNotificationLog extends Model
{
    use Sortable;
    protected $table = 'mst_notification_log';
    protected $primaryKey='notification_log_id';
    protected $fillable = ['notification_id','broadcast_id','npk','created_at','updated_at','keterangan'];

    protected $guarded = [
        'created_at','updated_at'
    ];

    public $sortable = [
        'notification_name','start_date','end_date','flag_active'
    ];

    public function createData(Request $request){
        $data = $this->create(
            ['notification_id' => $request->input('notification_id'),
            'npk' => $request->input('npk'),
            'keterangan' => $request->input('keterangan'),
            'created_at' =>  Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => Carbon::now('Asia/Jakarta')->toDateTimeString()
        ]);
        
        return $data;
    }

    public function getSortAbleData($getNonActiveData = false){
        if(!$getNonActiveData){
            return $this->join('mst_notification','mst_notification_log.notification_id','=','mst_notification.notification_id')
            ->select('mst_notification.notification_name','mst_notification.template','mst_notification_log.created_at','mst_notification_log.npk','mst_notification_log.keterangan')
            ->sortable(['notification_log_id','broadcast_id','npk']);
        }
        else if($getNonActiveData){
            return $this->join('mst_notification','mst_notification_log.notification_id','=','mst_notification.notification_id')
            ->select('mst_notification.notification_name','mst_notification.template','mst_notification_log.created_at','mst_notification_log.npk','mst_notification_log.keterangan')
            ->sortable(['notification_log_id','broadcast_id','npk']);
        }
    }

    public function getSearchAndFilterData(Request $request){
        $notifications = $this->getSortAbleData(true); 
        
        if($request->get('search')){
            $notifications =  $notifications->Where(function ($query) use ($request) {
                $query->where('notification_name','like',"%".$request->get('search')."%")
                    ->orWhere('npk','like',"%".$request->get('search')."%");
            });
        }

        return $notifications =  $notifications->orderby('notification_log_id','desc')->paginate(15);

    }
}
