<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use Auth;
use DB;

class MstQuizGrade extends Model
{
    protected $table = 'mst_quiz_grade';
    public $timestamps = true;
    protected $primaryKey = 'quiz_grade_id';

    protected $guarded = [
        'created_at','updated_at'
    ];

    public function updateScore($question,$npk,$newScore){
        if(!$this->where('user_id',$npk)->first()){
            $this->create(
                ['user_id' => $npk,
                'created_by' => $npk,
                'last_update_by' => $npk]);
        }

        $score = $this->where('user_id',$npk)
            ->first()->score;

        (int)$score = (int)$score + (int)$newScore;
        
        $this->where('user_id',$npk)
            ->update([
                'score' => $score,
                'last_update_by' => $npk
            ]);
    }
}