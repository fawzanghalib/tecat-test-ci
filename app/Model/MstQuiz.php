<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use Auth;


class MstQuiz extends Model
{
    protected $table = 'mst_quiz';
    protected $primaryKey='quiz_id';
    public $timestamps = true;
    use Sortable;

    protected $guarded = [
        'created_at'
    ];

    public $sortable = [
        'quiz_name','start_date','end_date','updated_at'
    ];

    public function mstJob(){
        return $this->belongsTo('App\MstJob','job_id','job_id')->orderBy('job_name');
    }

    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $quiz = $this;

        if(!$getNonActiveData){
            $quiz =  $quiz->where('mst_quiz.flag_active',1)
                ->where('mst_quiz.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_quiz.end_date')
                        ->orWhere('mst_quiz.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }
        if(array_key_exists('quiz_id',$conditions)){
            $quiz = $quiz->where('mst_quiz.quiz_id',$conditions['quiz_id']);
        }

        if(!empty($relationships)){
            $quiz = $quiz->with($relationships);
        }
        
        return $quiz->get();
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $quiz = $this;

        if(!$getNonActiveData){
            $quiz =  $quiz->where('mst_quiz.flag_active',1)
                ->where('mst_quiz.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_quiz.end_date')
                        ->orWhere('mst_quiz.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }
        if(array_key_exists('quiz_id',$conditions)){
            $quiz = $quiz->where('mst_quiz.quiz_id',$conditions['quiz_id']);
        }

        if(!empty($relationships)){
            $quiz = $quiz->with($relationships);
        }
        
        return $quiz->first();
    }

    public function getSortAbleData($getNonActiveData = false){
        if(!$getNonActiveData){
            return $this->where('flag_active',1)
                ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
                ->sortable(['quiz_name','start_date','end_date','flag_active']);
        }
        elseif ($getNonActiveData) {
            return $this->sortable(['quiz_name','start_date','end_date','flag_active']);
        }
    }

    public function getSearchAndFilterData(Request $request){
        $quizzes =  $this->getSortAbleData(true);
        if($request->get('search')){
            $quizzes =  $quizzes->Where(function ($query) use ($request) {
                $query->where('quiz_name','like',"%".$request->get('search')."%");
            });
        }
        if ($request->get('start_date') && $request->get('start_date') != '') {
            $quizzes =  $quizzes->where('start_date','>=',$request->get('start_date'));
        }
        if ($request->get('end_date') && $request->get('end_date') != '') {
            $quizzes =  $quizzes->where('end_date','<=',$request->get('end_date'));
        }
        if ($request->get('flag_active') != null) {
            $quizzes =  $quizzes->where('flag_active',$request->get('flag_active'));  
        }
        return $quizzes =  $quizzes->orderby('mst_quiz.quiz_id','desc')->paginate(50);
    }

    public function getActiveQuizToday(){
        return $this
            ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateString() )
            ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateString())
            ->where('flag_active',1)
            ->first();
    }

    public function createData(Request $request){
        $this->create(
            ['quiz_name' => $request->input('quiz_name'),
            // 'job_id' => $request->input('job_id'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }

    public function updateData(Request $request){
        $this->where('quiz_id',$request->input('quiz_id'))
            ->update([
                'quiz_name' => $request->input('quiz_name'),
                'flag_active' => $request->input('flag_active'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'last_update_by' => Auth::user()->id
            ]);
    }

    public function unactivatedData($quiz_id){
        $this->where('quiz_id',$quiz_id)
            ->update([
                'flag_active' => 0,
                'last_update_by' => Auth::user()->id
            ]);
    }

    public function activateData($quiz_id){
        $this->where('quiz_id',$quiz_id)
            ->update([
                'flag_active' => 1,
                'last_update_by' => Auth::user()->id
            ]);
    }

    public function quizDateIsUnique(Request $request){
        //cari end date yang ada di dalam date baru
        if ($request->quiz_id) {
            // $check1 = $this->where('job_id',$request->job_id)
            $check1 = $this
                ->where('flag_active',1)
                ->where('quiz_id','!=',$request->quiz_id)
                ->where('end_date', '>=' , $request->start_date)
                ->where('end_date', '<=' , $request->end_date)
                ->get();
        }
        else{
            // $check1 = $this->where('job_id',$request->job_id)
            $check1 = $this
                ->where('flag_active',1)
                ->where('end_date', '>=' , $request->start_date)
                ->where('end_date', '<=' , $request->end_date)
                ->get();
        }

        //cari start date yang ada di dalam date baru
        if ($request->quiz_id) {
            // $check2 = $this->where('job_id',$request->job_id)
            $check2 = $this
                ->where('flag_active',1)
                ->where('quiz_id','!=',$request->quiz_id)
                ->where('start_date', '>=' , $request->start_date)
                ->where('start_date', '<=' , $request->end_date)
                ->get();
        }
        else {
            // $check2 = $this->where('job_id',$request->job_id)
            $check2 = $this
                ->where('flag_active',1)
                ->where('start_date', '>=' , $request->start_date)
                ->where('start_date', '<=' , $request->end_date)
                ->get();
        }

        //cari apakah date baru ada didalam salah satu date yang ada
        if ($request->quiz_id) {
            // $check3 = $this->where('job_id',$request->job_id)
            $check3 = $this
                ->where('flag_active',1)
                ->where('quiz_id','!=',$request->quiz_id)
                ->where('start_date', '<=' , $request->start_date)
                ->where('end_date', '>=' , $request->end_date)
                ->get();
        }
        else {
            // $check3 = $this->where('job_id',$request->job_id)
            $check3 = $this
                ->where('flag_active',1)
                ->where('start_date', '<=' , $request->start_date)
                ->where('end_date', '>=' , $request->end_date)
                ->get();
        }

        if(count($check1) || count($check2) || count($check3)){        
            return true;
        }
        else {
            return false;
        }
    }
}
