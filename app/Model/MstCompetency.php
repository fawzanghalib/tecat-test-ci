<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use EloquentFilter\Filterable;
use App\ModelFilters\MstCompetencyFilter;
use Auth;

class MstCompetency extends Model
{
    use Filterable;
    protected $table = 'mst_competency';
    protected $primaryKey='competency_id';
    public $timestamps = true;
    use Sortable;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public $sortable = [
        'competency_name','start_date','end_date'
    ];

    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $competency = $this;
        if(!$getNonActiveData){
            $competency = $competency->where('mst_competency.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_competency.end_date')
                        ->orWhere('mst_competency.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                })
                ->where('mst_competency.flag_active',1);
        }
        
        if(array_key_exists('competency_id',$conditions)){
            $competency = $competency->where('mst_competency.competency_id',$conditions['competency_id']);
        }

        if(array_key_exists('competency_name',$conditions)){
            $competency = $competency->where('mst_competency.competency_name',$conditions['competency_name']);
        }


        if(!empty($relationships)){
            $competency = $competency->with($relationships);
        }

        return $competency->get(['competency_id','competency_name','description','start_date','end_date','flag_active']);
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $competency = $this;
        if(!$getNonActiveData){
            $competency = $competency->where('mst_competency.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_competency.end_date')
                        ->orWhere('mst_competency.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                })
                ->where('mst_competency.flag_active',1);
        }
        
        if(array_key_exists('competency_id',$conditions)){
            $competency = $competency->where('mst_competency.competency_id',$conditions['competency_id']);
        }

        if(array_key_exists('competency_name',$conditions)){
            $competency = $competency->where('mst_competency.competency_name',$conditions['competency_name']);
        }


        if(!empty($relationships)){
            $competency = $competency->with($relationships);
        }

        return $competency->first(['competency_id','competency_name','description','start_date','end_date','flag_active']);
        
    }
    
    public function getFilterData(Request $request){
        $MstCompetencyFilter = MstCompetencyFilter::class;
        return $this->filter($request->all(), $MstCompetencyFilter)
            ->sortable(['competency_id','competency_name','description','start_date','end_date'])
            ->get();
    }

    public function createData(Request $request){
        $this->create(
            ['competency_name' => $request->input('competency_name'),
            'description' => $request->input('description'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }

    public function updateData(Request $request,$id){
        $this->where('competency_id',$id)->update(
            ['competency_name' => $request->input('competency_name'),
            'flag_active' => $request->input('flag_active'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'description' => $request->input('description')]
        );
    }

    public function unactivatedData($id){
        $this->where('competency_id',$id)->update(
            ['flag_active' => 0,
            'last_update_by' => Auth::user()->id ]
        );
    }

    public function activateData($id){
        $this->where('competency_id',$id)->update(
            ['flag_active' => 1,
            'last_update_by' => Auth::user()->id ]
        );
    }
}
