<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;

class MstQuestionAnswer extends Model
{
    protected $table = 'mst_question_answer';
    protected $primaryKey='question_answer_id';
    public $timestamps = true;

    protected $guarded = [
        'created_at','updated_at'
    ];
    
    public function createData(Request $request,$question_id){
        $datas = array(
            array('question_id'=> $question_id, 'answer'=>$request->input('answer_1'),'score' => true, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id),
            array('question_id'=> $question_id, 'answer'=>$request->input('answer_2'),'score' => false, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id),
            array('question_id'=> $question_id, 'answer'=>$request->input('answer_3'),'score' => false, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id),
            array('question_id'=> $question_id, 'answer'=>$request->input('answer_4'),'score' => false, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id),
        );

        foreach ($datas as $data) {
            $this->create($data);
        }
    }

    public function createQuestionAnswerWithExcelUpload($question,$question_id){
        // natni di pull aja , check sebelum , array_push
        $datas = [];
        $datas['A'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerA'],'score' => false, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id];
        $datas['B'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerB'],'score' => false, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id];
        $datas['C'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerC'],'score' => false, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id];
        $datas['D'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerD'],'score' => false, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id];
        $datas[$question['Answer']] = ['question_id'=> $question_id, 'answer'=>$question['Answer'.$question['Answer']],'score' => true, 'created_by' => Auth::user()->id,'last_update_by' => Auth::user()->id];

        foreach ($datas as $data) {
            $this->create($data);
        }
    }

    public function createQuestionLoyaltyAnswer($question,$question_id){
        // natni di pull aja , check sebelum , array_push
        $datas = [];
        $datas['A'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerA'],'score' => false, 'created_by' => 0,'last_update_by' => 0];
        $datas['B'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerB'],'score' => false, 'created_by' => 0,'last_update_by' => 0];
        $datas['C'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerC'],'score' => false, 'created_by' => 0,'last_update_by' => 0];
        $datas['D'] = ['question_id'=> $question_id, 'answer'=>$question['AnswerD'],'score' => false, 'created_by' => 0,'last_update_by' => 0];
        $datas[$question['Answer']] = ['question_id'=> $question_id, 'answer'=>$question['Answer'.$question['Answer']],'score' => true, 'created_by' => 0,'last_update_by' => 0];

        foreach ($datas as $data) {
            $this->create($data);
        }
    }

    public function updateMultipleData(Request $request){
        $this->where('question_answer_id',$request->answer_1_id)
            ->where('question_id',$request->question_id)
            ->update(
                ['answer' => $request->answer_1,]
            );
        $this->where('question_answer_id',$request->answer_2_id)
            ->where('question_id',$request->question_id)
            ->update(
                ['answer' => $request->answer_2,]
            );
        $this->where('question_answer_id',$request->answer_3_id)
            ->where('question_id',$request->question_id)
            ->update(
                ['answer' => $request->answer_3,]
            );
        $this->where('question_answer_id',$request->answer_4_id)
            ->where('question_id',$request->question_id)
            ->update(
                ['answer' => $request->answer_4,]
            );
    }
}
