<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MstEmployeeScoring extends Model
{
    protected $table = 'mst_employee_scoring';
    protected $primaryKey='employee_scoring_id';
    public $timestamps = true;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public function createEmployeeLoyaltyScore($question_id, $npk, $scoring_id, $score){
        $this->create(
            ['npk' => $npk,
            'loyalty_question_id' => $question_id,
            'scoring_id' => $scoring_id,
            'score' => $score]);
    }
}
