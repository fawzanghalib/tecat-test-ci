<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;

class MstPackage extends Model
{
    protected $table = 'mst_package';
    protected $primaryKey='package_id';
    public $timestamps = true;

    protected $guarded = [
        'created_at',
        'updated_at'];

    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $package = $this;
        if(!$getNonActiveData){
            $package = $package->where('mst_package.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_package.end_date')
                        ->orWhere('mst_package.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                })
                ->where('mst_package.flag_active',1);
        }
        
        if(array_key_exists('package_id',$conditions)){
            $package = $package->where('mst_package.package_id',$conditions['package_id']);
        }
        if(array_key_exists('package_name',$conditions)){
            $package = $package->where('mst_package.package_name',$conditions['package_name']);
        }

        if(!empty($relationships)){
            $package = $package->with($relationships);
        }

        return $package->get(['package_id','package_name','description','flag_active','start_date','end_date']);
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $package = $this;
        if(!$getNonActiveData){
            $package = $package->where('mst_package.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_package.end_date')
                        ->orWhere('mst_package.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                })
                ->where('mst_package.flag_active',1);
        }
        
        if(array_key_exists('package_id',$conditions)){
            $package = $package->where('mst_package.package_id',$conditions['package_id']);
        }
        if(array_key_exists('package_name',$conditions)){
            $package = $package->where('mst_package.package_name',$conditions['package_name']);
        }

        if(!empty($relationships)){
            $package = $package->with($relationships);
        }

        return $package->first(['package_id','package_name','description','flag_active','start_date','end_date']);
    }

    public function createData(array $package){
        $this->create([
            'package_name' => $package['package_name'],
            'description' => $package['description'],
            'start_date' => $package['start_date'],
            'end_date' => $package['end_date'],
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id,
        ]);
    }

    public function updateData(array $package){
        $this->where('package_id',$package['package_id'])
            ->update([
                'package_name' => $package['package_name'],
                'description' => $package['description'],
                'flag_active' => $package['flag_active'],
                'start_date' => $package['start_date'],
                'end_date' => $package['end_date'],
                'last_update_by' => Auth::user()->id,
            ]);
    }

    public function unactivatedData($id){
        $this->where('package_id',$id)->update(
            ['flag_active' => 0 ,
            'last_update_by' => Auth::user()->id]
        );
    }

    public function activateData($id){
        $this->where('package_id',$id)->update(
            ['flag_active' => 1 ,
            'last_update_by' => Auth::user()->id]
        );
    }
}
