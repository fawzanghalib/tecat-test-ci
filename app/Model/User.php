<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    public $timestamps = true;
    use Notifiable;
    use Sortable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','npk'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $sortable = [
        'name', 'email'
    ];

    public function getData(){
        return $this->get(['id','name','email','flag_active','npk']);    
    }

    public function unactivatedData($id){
        if($id != 1){
            $this->where('id',$id)
                ->update([
                    'flag_active' => 0,
                ]);
        }
    }

    public function activateData($id){
        $this->where('id',$id)
            ->update([
                'flag_active' => 1,
            ]);
    }
}
