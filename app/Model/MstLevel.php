<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use EloquentFilter\Filterable;
use App\ModelFilters\MstLevelFilter;
use Auth;

class MstLevel extends Model
{
    protected $table = 'mst_level';
    use Sortable;
    use Filterable;

    protected $primaryKey='level_id';
    public $timestamps = true;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public $sortable = [
        'level_id','level_name','description','start_date','end_date'
    ];

    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $levels = $this;

        if(!$getNonActiveData){
            $levels =  $levels->where('mst_level.flag_active',1)
                ->where('mst_level.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_level.end_date')
                        ->orWhere('mst_level.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('level_id',$conditions)){
            $levels = $levels->where('mst_level.level_id',$conditions['level_id']);
        }
        if(array_key_exists('level_name',$conditions)){
            $levels = $levels->where('mst_level.level_name',$conditions['level_name']);
        }

        if(!empty($relationships)){
            $levels = $levels->with($relationships);
        }

        return $levels->get(['level_id','level_name','description','start_date','end_date','flag_active']);
    }
    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $level = $this;

        if(!$getNonActiveData){
            $level =  $level->where('mst_level.flag_active',1)
                ->where('mst_level.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_level.end_date')
                        ->orWhere('mst_level.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('level_id',$conditions)){
            $level = $level->where('mst_level.level_id',$conditions['level_id']);
        }
        if(array_key_exists('level_name',$conditions)){
            $level = $level->where('mst_level.level_name',$conditions['level_name']);
        }

        if(!empty($relationships)){
            $level = $level->with($relationships);
        }

        return $level->first(['level_id','level_name','description','start_date','end_date','flag_active']);
        
    }
    public function getSortAbleData($getNonActiveData = false){
        if(!$getNonActiveData){
            return $this->where('flag_active',1)
                ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
                ->sortable(['level_id','level_name','description','start_date','end_date']);
        }
        else if($getNonActiveData){
            return $this->sortable(['level_id','level_name','description','start_date','end_date']);
        }
    }
    public function getFilterData(Request $request){
        $MstLevelFilter = MstLevelFilter::class;
        return $this->filter($request->all(), $MstLevelFilter)
            ->sortable(['id','level_id','level_name','description','start_date','end_date','flag_active'])
            ->get();
    }
    public function createData(Request $request){
        $this->create(
            ['level_name' => $request->input('level_name'),
            'description' => $request->input('description'),
            'flag_active' => $request->input('flag_active'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }
    public function updateData(Request $request,$id){
        $this->where('level_id',$id)->update(
            ['level_name' => $request->input('level_name'),
            'flag_active' => $request->input('flag_active'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'description' => $request->input('description')]
        );
    }
    public function unactivatedData($id){
        $this->where('level_id',$id)->update(
            ['flag_active' => 0,
            'last_update_by' => Auth::user()->id ]
        );
    }
    public function activateData($id){
        $this->where('level_id',$id)->update(
            ['flag_active' => 1,
            'last_update_by' => Auth::user()->id ]
        );
    }
}
