<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use EloquentFilter\Filterable;
use App\ModelFilters\MstJobFilter;
use Auth;

class MstJob extends Model
{
    protected $table = 'mst_job';
    use Sortable;
    use Filterable;

    protected $primaryKey='id';

    protected $guarded = [
        'created_at','updated_at'
    ];
    
    public $sortable = [
        'job_name','start_date','end_date'
    ];

    public function user(){
        return $this->hasMany('App\Modul\User');
    }

    public function mstFunction(){
        return $this->belongsTo('App\Modul\MstFunction','function_id','function_id');
    }
    
    public function mstLevels(){
        return $this->belongsToMany('App\Modul\MstLevel','mst_competency_details','id','level_id');
    }

    public function mstCompetencies(){
        return $this->belongsToMany('App\Modul\MstCompetency','mst_competency_details','id','competency_id');
    }

    public function mstModuls(){
        return $this->belongsToMany('App\Modul\MstModul','mst_competency_details','id','modul_id');
    }

    public function mstCompetencyDetails(){
        return $this->hasMany('App\Modul\MstCompetencyDetail','job_id','job_id');
    }
    
    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $jobs = $this->leftJoin('mst_function','mst_job.function_id','=','mst_function.function_id');

        if(!$getNonActiveData){
            $jobs =  $jobs->where('mst_job.flag_active',1)
                ->where('mst_job.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_job.end_date')
                        ->orWhere('mst_job.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('job_id',$conditions)){
            $jobs = $jobs->where('mst_job.job_id',$conditions['job_id']);
        }
        if(array_key_exists('function_id',$conditions)){
            $jobs = $jobs->where('mst_job.function_id',$conditions['function_id']);
        }
        if(array_key_exists('job_name',$conditions)){
            $jobs = $jobs->where('mst_job.job_name',$conditions['job_name']);
        }

        if(!empty($relationships)){
            $jobs = $jobs->with($relationships);
        }

        return $jobs->get(['id','job_id','mst_job.function_id','function_name','job_name','mst_job.description','mst_job.start_date','mst_job.end_date','mst_job.flag_active']);
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $job = $this->leftJoin('mst_function','mst_job.function_id','=','mst_function.function_id');

        if(!$getNonActiveData){
            $job =  $job->where('mst_job.flag_active',1)
                ->where('mst_job.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_job.end_date')
                        ->orWhere('mst_job.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }
        if(array_key_exists('job_id',$conditions)){
            $job = $job->where('mst_job.job_id',$conditions['job_id']);
        }
        if(array_key_exists('function_id',$conditions)){
            $job = $job->where('mst_job.function_id',$conditions['function_id']);
        }
        if(array_key_exists('job_name',$conditions)){
            $job = $job->where('mst_job.job_name',$conditions['job_name']);
        }

        if(!empty($relationships)){
            $job = $job->with($relationships);
        }

        return $job->first(['id','job_id','mst_job.function_id','function_name','job_name','mst_job.description','mst_job.start_date','mst_job.end_date','mst_job.flag_active']);
    }

    public function getFilterData(Request $request){
        $MstJobFilter = MstJobFilter::class;
        return $this->filter($request->all(), $MstJobFilter)
            ->leftJoin('mst_function','mst_job.function_id','=','mst_function.function_id')
            ->sortable(['id','job_id','function_id','function_name','job_name','description','start_date','end_date','flag_active'])
            ->orderby('job_name','asc')
            ->get(['id','job_id','mst_job.function_id','function_name','job_name','mst_job.description','mst_job.start_date','mst_job.end_date','mst_job.flag_active']);
            
    }

    public function getMatrixCompetency(){
        return $this->with([
            'mstCompetencyDetails' => function ($query) {
                $query->where('mst_competency_details.flag_active',1)
                    ->where('mst_competency_details.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                    ->where(function($query2){
                        $query2->whereNull('end_date')
                            ->orWhere('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                    })

                    ->join('mst_competency','mst_competency_details.competency_id','=','mst_competency.competency_id')
                    ->join('mst_modul','mst_competency_details.modul_id','=','mst_modul.modul_id')
                    ->join('mst_level','mst_competency_details.level_id','=','mst_level.level_id')
                    ->join('mst_job','mst_competency_details.job_id','=','mst_job.job_id')
                    ->join('mst_function','mst_job.function_id','=','mst_function.function_id');},
            'mstCompetencyDetails.mstCompetency',
            'mstCompetencyDetails.mstModul',
            'mstCompetencyDetails.mstLevel',
            'mstFunction',
        ])->get();
    }

    public function createDataFromApi($job){
        $this->create(
            ['job_name' => $job['Name'],
            'job_id' => $job['JobId'],
            'description' => 'Description',
            'start_date' => '2019-01-01 00:00:00',
            'flag_active' => '0',
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }

    public function createData(array $job){
        if($job['end_date'] == null | $job['end_date'] == "")
        {
            $this->create(
                ['job_name' => $job['job_name'],
                'description' => $job['description'],
                'job_id' => $job['job_id'],
                'function_id' => $job['function_id'],
                'start_date' => $job['start_date'],
                'end_date' => "9999-01-01",
                'created_by' => Auth::user()->id,
                'last_update_by' => Auth::user()->id
            ]);
        }else
        {
            $this->create(
                ['job_name' => $job['job_name'],
                'description' => $job['description'],
                'job_id' => $job['job_id'],
                'function_id' => $job['function_id'],
                'start_date' => $job['start_date'],
                'end_date' => $job['end_date'],
                'created_by' => Auth::user()->id,
                'last_update_by' => Auth::user()->id
            ]);
        }
       
    }

    public function updateData(array $job,$id){
        $this->where('id',$id)->update(
            ['job_name' => $job['job_name'],
            'description' => $job['description'],
            'function_id' => $job['function_id'],
            'flag_active' => $job['flag_active'],
            'start_date' => $job['start_date'],
            'end_date' => $job['end_date'],
            'last_update_by' => Auth::user()->id]
        );
    }

    public function unactivatedData($id){
        $this->where('job_id',$id)->update(
            ['flag_active' => 0,
            'last_update_by' => Auth::user()->id ]
        );
    }

    public function activateData($id){
        $this->where('job_id',$id)->update(
            ['flag_active' => 1,
            'last_update_by' => Auth::user()->id ]
        );
    }
}
