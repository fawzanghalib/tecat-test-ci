<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MstQuestionType extends Model
{
    protected $table = 'mst_question_type';

    public $timestamps = true;

    public function getData(){
        return $this->where('flag_active',1)
            ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
            ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
            ->get(['question_type_id','question_type_name','description']);
    }

    public function getSingleData($id){
        return $this->where('flag_active',1)
            ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
            ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
            ->where('question_type_id',$id)
            ->first(['question_type_id','question_type_name','description']);
    }
}
