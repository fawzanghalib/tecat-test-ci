<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;
use DB;
use Kyslik\ColumnSortable\Sortable;

class MstQuizAttempts extends Model
{
    use Sortable;
    protected $table = 'mst_quiz_attempts';
    protected $primaryKey= 'attempts_id';
    public $timestamps = true;
    protected $guarded = [
        'created_at','updated_at'
    ];

    public function mstQuestion(){
        return $this->belongsTo('App\Model\MstQuestion','question_id','question_id');
    }

    public function getSingleData(array $conditions = [],array $relationships = []){
        $quizAttempt = $this;
        
        if(array_key_exists('attempts_id',$conditions)){
            $quizAttempt = $quizAttempt->where('mst_quiz_attempts.attempts_id',$conditions['attempts_id']);
        }

        if(array_key_exists('user_id',$conditions)){
            $quizAttempt = $quizAttempt->where('mst_quiz_attempts.user_id',$conditions['user_id']);
        }

        if(array_key_exists('quiz_id',$conditions)){
            $quizAttempt = $quizAttempt->where('mst_quiz_attempts.quiz_id',$conditions['quiz_id']);
        }

        if(!empty($relationships)){
            $quizAttempt = $quizAttempt->with($relationships);
        }

        return $quizAttempt->first();
    }

    public function getData(array $conditions = [],array $relationships = []){
        $quizAttempt = $this;
        
        if(array_key_exists('attempts_id',$conditions)){
            $quizAttempt = $quizAttempt->where('mst_quiz_attempts.attempts_id',$conditions['attempts_id']);
        }

        if(array_key_exists('user_id',$conditions)){
            $quizAttempt = $quizAttempt->where('mst_quiz_attempts.user_id',$conditions['user_id']);
        }

        if(array_key_exists('quiz_id',$conditions)){
            $quizAttempt = $quizAttempt->where('mst_quiz_attempts.quiz_id',$conditions['quiz_id']);
        }

        if(array_key_exists('time_finish',$conditions)){
            $quizAttempt = $quizAttempt->where('mst_quiz_attempts.time_finish',$conditions['time_finish']);
        }

        if(!empty($relationships)){
            $quizAttempt = $quizAttempt->with($relationships);
        }

        return $quizAttempt->get();
    }

    
    public function createData($quiz_id,$user_id,$question_id){
        return $this->create(
            ['quiz_id' => $quiz_id,
            'question_id' => $question_id,
            'user_id' => $user_id,
            'time_start' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => $user_id,
            'last_update_by' => $user_id]);
    }

    public function updateScore(Request $request,$scoring,$user_id){
        $this->where('attempts_id',$request->attempts_id)
            ->update(
            ['scoring_id' => $scoring->scoring_id,
            'sum_score' => $scoring->score,
            'time_finish' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'last_update_by' => $user_id]);
    }

    public function haveGotQuizToday($user_id){
        $tamp = $this->where('user_id',$user_id)
            ->whereDate('time_start','=',\Carbon\Carbon::now('Asia/Jakarta')->toDateString())
            ->get();
        
        if(count($tamp)==0){
            return false;
        }
        else{
            return true;
        }
    }

    public function getQuizStillNotAnswered($user_id){
        return $this->where('user_id',$user_id)
            ->whereNull('time_finish')
            ->whereDate('time_start','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateString())
            ->get();
    }

    public function getIndividualReport($npk){
        return $this->where('user_id',$npk)
            ->join('mst_scoring','mst_quiz_attempts.scoring_id','=','mst_scoring.scoring_id')
            ->join('mst_quiz_respons','mst_quiz_attempts.attempts_id','=','mst_quiz_respons.attempts_id')
            ->join('mst_question','mst_quiz_attempts.question_id','=','mst_question.question_id')
            ->leftjoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            ->groupBy('mst_question.competency_id')
            ->select('mst_question.question_type_id','mst_competency.competency_name', 'mst_competency.description', DB::raw("SUM((mst_quiz_respons.sum_score)) as total_benar"),DB::raw('count(*) as total_soal'))
            // ->orderby('mst_quiz_attempts.time_finish','desc')
            ->get();
    }

    public function getIndividualHistoryPoin($npk){
        return $this->where('user_id',$npk)
            ->join('mst_scoring','mst_quiz_attempts.scoring_id','=','mst_scoring.scoring_id')
            ->join('mst_quiz_respons','mst_quiz_attempts.attempts_id','=','mst_quiz_respons.attempts_id')
            ->join('mst_question','mst_quiz_attempts.question_id','=','mst_question.question_id')
            ->leftjoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            ->groupBy('mst_quiz_respons.respons_id')
            ->select('mst_quiz_respons.attempts_id','mst_question.question_type_id','mst_competency.competency_name', 'mst_competency.description', DB::raw("SUM((mst_quiz_respons.sum_score)) as total_benar"),DB::raw('count(*) as total_soal'),'mst_quiz_attempts.time_finish','mst_quiz_attempts.sum_score','mst_scoring.type')
            // ->orderby('mst_quiz_attempts.time_finish','desc')
            ->get();
    }

    public function getReportData(Request $request){
        $reports = $this->join('mst_quiz_respons','mst_quiz_attempts.attempts_id','=','mst_quiz_respons.attempts_id')
            ->join('mst_question','mst_quiz_attempts.question_id','=','mst_question.question_id')
            ->join('mst_question_answer','mst_quiz_respons.answer','=','mst_question_answer.question_answer_id')
            ->join('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            ->join('mst_level','mst_question.level_id','=','mst_level.level_id')
            ->join('mst_job','mst_question.job_id','=','mst_job.job_id')
            ->join('mst_function','mst_job.function_id','=','mst_function.function_id');
        if($request->function_id){
            $reports = $reports->whereIn('mst_function.function_id',$request->function_id);
        }
        if($request->job_id){
            $reports = $reports->whereIn('mst_question.job_id',$request->job_id);
        }
        if($request->competency_id){
            $reports = $reports->whereIn('mst_question.competency_id',$request->competency_id);
        }
        if($request->level_id){
            $reports = $reports->whereIn('mst_question.level_id',$request->level_id);
        }
        if($request->start_date){
            $reports = $reports->whereDate('mst_quiz_attempts.time_start','>=',$request->start_date);
        }
        if($request->end_date){
            $reports = $reports->whereDate('mst_quiz_attempts.time_start','<=',$request->end_date);
        }
        return $reports->select('mst_quiz_attempts.user_id','mst_function.function_name','mst_job.job_name','mst_competency.competency_name','mst_level.level_name','mst_question.question','mst_question_answer.answer','mst_question.question_id','mst_quiz_respons.sum_score','mst_quiz_attempts.time_start','mst_quiz_attempts.time_finish')->get();
    }

    public function getTeamReport($arrayNPK){
        return $this->whereIn('user_id',$arrayNPK)
            ->join('mst_quiz_respons','mst_quiz_attempts.attempts_id','=','mst_quiz_respons.attempts_id')
            ->join('mst_question','mst_quiz_attempts.question_id','=','mst_question.question_id')
            ->leftjoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            ->groupBy('mst_question.competency_id')
            ->select('mst_quiz_respons.attempts_id','mst_question.question_type_id','mst_competency.competency_name', 'mst_competency.description',DB::raw("SUM((mst_quiz_respons.sum_score)) as total_benar"),DB::raw('count(*) as total_soal'))
            ->get();
    }

    public function getIndividualReportArray($arrayNPK){
        return $this->whereIn('user_id',$arrayNPK)
        ->join('mst_scoring','mst_quiz_attempts.scoring_id','=','mst_scoring.scoring_id')
        ->join('mst_quiz_respons','mst_quiz_attempts.attempts_id','=','mst_quiz_respons.attempts_id')
        ->join('mst_question','mst_quiz_attempts.question_id','=','mst_question.question_id')
        ->leftjoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
        ->groupBy('mst_question.competency_id')
        ->select('mst_quiz_respons.attempts_id','mst_question.question_type_id','mst_competency.competency_name', 'mst_competency.description', DB::raw("SUM((mst_quiz_respons.sum_score)) as total_benar"),DB::raw('count(*) as total_soal'),'mst_quiz_attempts.time_finish','mst_quiz_attempts.sum_score')
        // ->orderby('mst_quiz_attempts.time_finish','desc')
        ->get();
    }

    // CompetencyAnalysisResults

    public function getSortAbleData($getNonActiveData = false){
        
        if($getNonActiveData){
            return $this->join('mst_quiz_respons','mst_quiz_attempts.attempts_id','=','mst_quiz_respons.attempts_id')
            ->join('mst_question','mst_quiz_attempts.question_id','=','mst_question.question_id')
            ->join('mst_job','mst_question.job_id','=','mst_job.job_id')
            ->join('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            ->sortable(['user_id']);
        }

    }

    public function getCompetencyAnalysisResults(Request $request){

        $data =  $this->getSortAbleData(true)
            ->groupBy('mst_quiz_attempts.user_id','mst_job.job_name','mst_competency.competency_name');

        if($request->get('search')){
            $data =  $data->Where(function ($query) use ($request) {
                $query->where('mst_quiz_attempts.user_id','like',"%".$request->get('search')."%");
            });
        }

        if($request->job_id){
            $data = $data->whereIn('mst_question.job_id',$request->job_id);
        }
        if($request->competency_id){
            $data = $data->whereIn('mst_question.competency_id',$request->competency_id);
        }

        return $data->select('mst_quiz_attempts.user_id','mst_job.job_name','mst_competency.competency_name',
        DB::raw('count(*) as total_soal'),DB::raw("SUM((mst_quiz_respons.sum_score)) as total_benar"), 
        DB::raw('count(*) - SUM((mst_quiz_respons.sum_score)) as total_salah'), DB::raw('100 * SUM((mst_quiz_respons.sum_score)) / count(*) as percentage'))
        // ->orderby('mst_quiz_attempts.time_finish','desc')
        ->get();

    }

    // public function downloadData(Request $request){
    //     $data =  $this->getSortAbleData(true); 

    //     return $data->select('mst_job.job_name','mst_quiz_respons.attempts_id','mst_question.question_type_id',
    //     'mst_competency.competency_name',DB::raw("SUM((mst_quiz_respons.sum_score)) as total_benar"),
    //     DB::raw('count(*) as total_soal'),'mst_quiz_attempts.time_finish','mst_quiz_attempts.sum_score',
    //     'mst_scoring.type','mst_quiz_attempts.user_id')
    //     ->orderby('mst_quiz_attempts.time_finish','desc')
    //     ->get();
    // }

    /**
     * Get questions that have been answered correctly by employees
     *
     * @param  int $item['user_id'], int $item['quiz_id']
     * @return Illuminate\Support\Collection
     */
    public function getQuestionsAreAnsweredCorrectly(array $item = []){
        return $this->join('mst_scoring','mst_quiz_attempts.scoring_id','=','mst_scoring.scoring_id')
            ->where('mst_quiz_attempts.user_id',$item['user_id'])
            ->where('mst_quiz_attempts.quiz_id',$item['quiz_id'])
            ->where(function($query){
                $query->where('mst_scoring.type',1)
                    ->orWhere('mst_scoring.type',2);
            })
            ->get();
    }
    
}
