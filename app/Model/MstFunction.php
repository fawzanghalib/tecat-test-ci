<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use Kyslik\ColumnSortable\Sortable;
use DB;
use Auth;

class MstFunction extends Model
{
    use Sortable;
    protected $table = 'mst_function';
    protected $primaryKey='function_id';
    protected $guarded = [
        'created_at','updated_at'
    ];

    public $sortable = [
        'function_name'
    ];

    public function mstJobs(){
        return $this->hasMany('App\Model\MstJob','function_id','function_id')
            ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
            ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
            ->orderBy('job_name');
    }
    
    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $functions = $this;
        if(!$getNonActiveData){
            $functions =  $functions->where('mst_function.flag_active',1)
                ->where('mst_function.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_function.end_date')
                        ->orWhere('mst_function.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('function_id',$conditions)){
            $functions = $functions->where('mst_function.function_id',$conditions['function_id']);
        }

        if(array_key_exists('function_name',$conditions)){
            $functions = $functions->where('mst_function.function_name',$conditions['function_name']);
        }

        if(!empty($relationships)){
            $functions = $functions->with($relationships);
        }

        return $functions->get(['function_id','function_name','description','start_date','end_date','flag_active']);
    }

    public function getFilterData(){
        return $this->sortable(['function_id','function_name','description','start_date','end_date','flag_active'])
            ->get(['function_id','function_name','description','start_date','end_date','flag_active']);
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $function = $this;
        if(!$getNonActiveData){
            $function =  $function->where('mst_function.flag_active',1)
                ->where('mst_function.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_function.end_date')
                        ->orWhere('mst_function.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('function_id',$conditions)){
            $function = $function->where('mst_function.function_id',$conditions['function_id']);
        }

        if(array_key_exists('function_name',$conditions)){
            $function = $function->where('mst_function.function_name',$conditions['function_name']);
        }

        if(!empty($relationships)){
            $function = $function->with($relationships);
        }

        return $function->first(['function_id','function_name','description','start_date','end_date','flag_active']);
        
    }

    public function createData(Request $request){
        $this->create(
            ['function_name' => $request->input('function_name'),
            'description' => $request->input('description'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }

    public function updateData(Request $request,$id){
        $this->where('function_id',$id)
            ->update(
                ['function_name' => $request->input('function_name'),
                'flag_active' => $request->input('flag_active'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'description' => $request->input('description'),
                'last_update_by' => Auth::user()->id]);
    }
    
    public function activateData($id){
        $this->where('function_id',$id)
            ->update(
                ['flag_active' => 1,
                'last_update_by' => Auth::user()->id ]);
    }

    public function unactivatedData($id){
        $this->where('function_id',$id)
            ->update(
                ['flag_active' => 0,
                'last_update_by' => Auth::user()->id ]);
    }
}
