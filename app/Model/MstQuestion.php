<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use EloquentFilter\Filterable;
use App\ModelFilters\MstQuestionFilter;
use Auth;
use DB;

class MstQuestion extends Model
{
    use Filterable;
    protected $table = 'mst_question';
    protected $primaryKey='question_id';
    public $timestamps = true;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public function mstQuizQueGrades(){
        return $this->hasMany('App\Model\MstQuizQueGrade','question_id','question_id');
    }

    public function mstQuestionAnswers(){
        return $this->hasMany(
            'App\Model\MstQuestionAnswer',
            'question_id',
            'question_id');
    }

    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $questions = $this;
        if(!$getNonActiveData){
            $questions =  $questions->where('mst_function.flag_active',1)
                ->where('mst_function.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_function.end_date')
                        ->orWhere('mst_function.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('questions_id',$conditions)){
            $questions = $questions->where('mst_question.questions_id',$conditions['questions_id']);
        }

        if(!empty($relationships)){
            $questions = $questions->with($relationships);
        }

        return $questions->get();
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $questions = $this;
        if(!$getNonActiveData){
            $questions =  $questions->where('mst_question.flag_active',1)
                ->where('mst_question.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_question.end_date')
                        ->orWhere('mst_question.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('question_id',$conditions)){
            $questions = $questions->where('mst_question.question_id',$conditions['question_id']);
        }

        if(!empty($relationships)){
            $questions = $questions->with($relationships);
        }

        return $questions->first();
    }

    public function getFilterData(Request $request,$package_id){
        $MstQuestionFilter = MstQuestionFilter::class;
        return $this->where('package_id',$package_id)
            ->filter($request->all(), $MstQuestionFilter)
            ->leftJoin('mst_modul','mst_question.modul_id','=','mst_modul.modul_id')
            ->leftjoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            ->leftjoin('mst_level','mst_question.level_id','=','mst_level.level_id')
            ->leftjoin('mst_job','mst_question.job_id','=','mst_job.job_id')
            ->select('mst_question.*','mst_level.level_name','mst_modul.modul_name','mst_competency.competency_name','mst_job.job_name')
            ->with('mstQuestionAnswers')
            ->get();
    }

    public function getResultAnswer($question_id,$question_answer_id){
        $result = $this->join('mst_question_answer','mst_question.question_id','=','mst_question_answer.question_id')
            ->where('mst_question.question_id',$question_id)
            ->where('mst_question_answer.question_answer_id', $question_answer_id)
            ->first();
        if($result){
            return $result->score;}
        else{
            return 'question and answer do not exist';}
    }
    
    public function getLoyaltyQuestions(Request $request){
        $MstQuestionFilter = MstQuestionFilter::class;
        return $this->whereNotNull('npk')
            ->filter($request->all(), $MstQuestionFilter)
            ->get();
    }

    public function isRejectedLoyaltyQuestion($id){
        $question = $this->where('question_id',$id)
            ->whereNotNull('npk')
            ->where('flag_loyalty',0)
            ->get();
        if(count($question) != 0)
            return true;
        else
            return false;
    }

    public function isNotLoyaltyQuestion($id){
        $question = $this->where('question_id',$id)
            ->whereNotNull('npk')
            ->get();
        if(count($question) != 0)
            return false;
        else
            return true;
    }

    public function wasLoyaltyQuestionChecked($id){
        $question = $this->where('question_id',$id)
            ->whereNotNull('npk')
            ->whereNotNull('flag_loyalty')
            ->get();
        if(count($question) != 0)
            return true;
        else
            return false;
    }

    public function rejectLoyaltyQuestion($id){
        $this->where('question_id',$id)->update(
            ['flag_loyalty' => 0,
            'last_update_by' => Auth::user()->id]
        );
    }

    public function acceptLoyaltyQuestion($id){
        $this->where('question_id',$id)->update(
            ['flag_loyalty' => 1,
            'last_update_by' => Auth::user()->id]
        );
    }

    public function getSingleRandomQuestion($job_id =  null,$quiz_id,$arrayAttemptsQuestionId,$arrayCompetencyId = null){
        $arrayQuestionId = $this->getArrayQuestionIdInQuiz($quiz_id);
        if($job_id !== null && $arrayCompetencyId !== null){
            foreach($arrayCompetencyId as $competency_id){
                $temp = $this->whereIn('mst_question.question_id', $arrayQuestionId)
                ->where('question_type_id',1)
                ->where('job_id',$job_id)
                ->whereNotIn('mst_question.question_id',$arrayAttemptsQuestionId)
                ->where('competency_id',$competency_id)
                ->inRandomOrder()
                ->first();
                if($temp){
                    return $temp;
                }
            }
        }

        else {
            return $this->whereIn('mst_question.question_id', $arrayQuestionId)
            ->where('question_type_id',2)
            ->whereNotIn('mst_question.question_id',$arrayAttemptsQuestionId)
            ->inRandomOrder()
            ->first();
        }
        return null;       
    }

    public function getDataWithPackageIdInRangeDateQuiz(Request $request,$quiz_id,$package_id,$start_date,$end_date){
        $arrayQuestionIdListedInQuiz = $this->getArrayQuestionIdInQuiz($quiz_id);
        $MstQuestionFilter = MstQuestionFilter::class;
        return $this->filter($request->all(), $MstQuestionFilter)
            ->where('mst_question.flag_active',1)
            ->where('mst_question.start_date','<=', $start_date )
            ->where(function($query) use ($end_date){
                $query->whereNull('mst_question.end_date')
                    ->orWhere('mst_question.end_date','>=', $end_date);
            })
            ->where('mst_question.package_id',$package_id)
            ->leftJoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            //->leftJoin('mst_package','mst_question.package_id','=','mst_package.package_id')
            ->leftJoin('mst_level','mst_question.level_id','=','mst_level.level_id')
            ->leftJoin('mst_job','mst_question.job_id','=','mst_job.job_id')
            ->leftJoin('mst_modul','mst_question.modul_id','=','mst_modul.modul_id')
            ->select('mst_question.*','mst_level.level_name','mst_competency.competency_name','mst_job.job_name','mst_modul.modul_name')
            ->whereNotIn('mst_question.question_id',$arrayQuestionIdListedInQuiz)
            ->with('mstQuestionAnswers')
            ->orderBy('mst_question.question_id','ASC')
            ->get();
    }

    //harusnya ini dipindah ke MstQuizQueGrade tapi belum bisa menemukan cara untuk menggunakan function di model lewat model
    public function getArrayQuestionIdInQuiz($quiz_id){
        //mencari question_id dalam mst_quiz_que_grade dengan $quiz_id
        $arrayQuestionId = DB::table('mst_quiz_que_grade')
            ->where('quiz_id',$quiz_id)
            ->select(DB::raw("mst_quiz_que_grade.question_id"))
            ->get()->pluck('question_id')->toArray();

        //ubah keyvalue array ke array biasa dengan isi question_id
        return array_values($arrayQuestionId);
    }

    public function getDataAndQuestionAnswerWithQuizId($quiz_id,Request $request){
        $MstQuestionFilter = MstQuestionFilter::class;

        return $this->filter($request->all(), $MstQuestionFilter)
            ->leftJoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            // ->leftJoin('mst_package','mst_question.package_id','=','mst_package.package_id')
            ->leftJoin('mst_modul','mst_question.modul_id','=','mst_modul.modul_id')
            ->leftJoin('mst_level','mst_question.level_id','=','mst_level.level_id')
            ->leftJoin('mst_job','mst_question.job_id','=','mst_job.job_id')
            ->join('mst_quiz_que_grade','mst_question.question_id','=','mst_quiz_que_grade.question_id')
            ->join('mst_package','mst_question.package_id','=','mst_package.package_id')
            ->where('mst_quiz_que_grade.quiz_id',$quiz_id)
            // ->whereIn('mst_question.question_id', $arrayQuestionId)
            ->orderBy('created_at','desc')
            ->select('mst_question.*','mst_package.package_name','mst_level.level_name','mst_modul.modul_name','mst_competency.competency_name','mst_job.job_name')
            ->with('mstQuestionAnswers')
            ->get();
    }

    public function downloadData($quiz_id, Request $request)
    {
        $MstQuestionFilter = MstQuestionFilter::class;

        return $this->filter($request->all(), $MstQuestionFilter)
            ->leftJoin('mst_competency','mst_question.competency_id','=','mst_competency.competency_id')
            ->leftJoin('mst_modul','mst_question.modul_id','=','mst_modul.modul_id')
            ->leftJoin('mst_level','mst_question.level_id','=','mst_level.level_id')
            ->leftJoin('mst_job','mst_question.job_id','=','mst_job.job_id')
            ->join('mst_quiz_que_grade','mst_question.question_id','=','mst_quiz_que_grade.question_id')
            ->join('mst_package','mst_question.package_id','=','mst_package.package_id')
            ->where('mst_quiz_que_grade.quiz_id',$quiz_id)
            ->select('mst_question.question_id','mst_question.question', 'mst_level.level_name','mst_package.package_name','mst_level.level_name','mst_job.job_name')
            ->get();
    }

    public function createData(array $question){
        return $this->create(
            ['package_id' => $question['package_id'],
            'question_type_id' => $question['question_type_id'],
            // 'function_id' => $question['function_id'] ?? 0,
            'job_id' => $question['job_id'] ?? 0,
            'competency_id' => $question['competency_id'] ?? 0,
            'modul_id' => $question['modul_id'] ?? 0,
            'level_id' => $question['level_id'] ?? 0,
            'question' => $question['question'],
            'start_date' => $question['start_date'] ?? \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => $question['end_date'] ?? null,
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id])->question_id;
    }

    public function createQuestionLoyalty(array $question){
        return $this->create(
            ['package_id' => 0,
            'question_type_id' => 0,
            'function_id' => $question['function_id'],
            'job_id' => 0,
            'competency_id' => 0,
            'modul_id' => 0,
            'level_id' => 0,
            'question' => $question['question'],
            'npk' => $question['npk'],
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => null,
            'created_by' => null,
            'last_update_by' => null])->question_id;
    }

    public function updateData(array $question){
        $this->where('question_id',$question['question_id'])->update(
            ['package_id' => $question['package_id'],
            'question_type_id' => $question['question_type_id'],
            'function_id' => $question['function_id'] ?? 0,
            'job_id' => $question['job_id'] ?? 0,
            'competency_id' => $question['competency_id'] ?? 0,
            'level_id' => $question['level_id'] ?? 0,
            'modul_id' => $question['modul_id'],
            'question' => $question['question'],
            'start_date' => $question['start_date'],
            'end_date' => $question['end_date'],
            'last_update_by' => Auth::user()->id]);
    }

    public function unactivatedData($id){
        $this->where('question_id',$id)
        ->update([
            'flag_active' => 0,
            'last_update_by' => Auth::user()->id
        ]);
    }

    public function activateData($id){
        $this->where('question_id',$id)
        ->update([
            'flag_active' => 1,
            'last_update_by' => Auth::user()->id
        ]);
    }
}
