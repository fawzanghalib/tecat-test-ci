<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use EloquentFilter\Filterable;
use App\ModelFilters\MstCompetencyDetailFilter;
use Auth;
use DB;

class MstCompetencyDetail extends Model
{
    use Filterable;
    
    protected $table = 'mst_competency_details';
    public $timestamps = true;
    public $incrementing = false;
    protected $primaryKey = array('competency_id','job_id','level_id','modul_id');

    protected $guarded = [
        'created_at','updated_at'
    ];

    public function mstLevel(){
        return $this->belongsTo('App\Model\MstLevel','level_id','level_id');
    } 

    public function mstCompetency(){
        return $this->belongsTo('App\Model\MstCompetency','competency_id','competency_id');
    }


    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $competencyDetails = $this;
        
        if(!$getNonActiveData){
            $competencyDetails = $competencyDetails->where('flag_active',1)
                ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_competency_details.end_date')
                        ->orWhere('mst_competency_details.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('competency_id',$conditions)){
            $competencyDetails = $competencyDetails->where('competency_id',$conditions['competency_id']);
        }
        if(array_key_exists('job_id',$conditions)){
            $competencyDetails = $competencyDetails->where('job_id',$conditions['job_id']);
        }
        if(array_key_exists('level_id',$conditions)){
            $competencyDetails = $competencyDetails->where('level_id',$conditions['level_id']);
        }

        if(!empty($relationships)){
            $competencyDetails = $competencyDetails->with($relationships);
        }

        return $competencyDetails->get();
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $competencyDetail = $this;
            
        if(!$getNonActiveData){
            $competencyDetail = $competencyDetail->where('flag_active',1)
                ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_competency_details.end_date')
                        ->orWhere('mst_competency_details.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }

        if(array_key_exists('competency_id',$conditions)){
            $competencyDetail = $competencyDetail->where('competency_id',$conditions['competency_id']);
        }
        if(array_key_exists('job_id',$conditions)){
            $competencyDetail = $competencyDetail->where('job_id',$conditions['job_id']);
        }
        if(array_key_exists('level_id',$conditions)){
            $competencyDetail = $competencyDetail->where('level_id',$conditions['level_id']);
        }

        if(!empty($relationships)){
            $competencyDetail = $competencyDetail->with($relationships);
        }

        return $competencyDetail->first();
    }

    // index competency detail
    public function getMatrixCompetency(Request $request){
        $MstCompetencyDetailFilter = MstCompetencyDetailFilter::class;
        return $this->filter($request->all(), $MstCompetencyDetailFilter)
            ->join('mst_competency','mst_competency_details.competency_id','=','mst_competency.competency_id')
            ->join('mst_level','mst_competency_details.level_id','=','mst_level.level_id')
            ->join('mst_job','mst_competency_details.job_id','=','mst_job.job_id')
            ->leftJoin('mst_function','mst_job.function_id','=','mst_function.function_id')
            ->select('mst_competency_details.*','mst_competency.competency_name','mst_level.level_name','mst_job.job_name','mst_function.function_name')
            ->orderBy('job_name', 'ASC')
            ->orderBy('competency_name', 'ASC')
            ->orderBy('level_name', 'ASC')
            ->get();
    }

    public function CompetencyDetailIsExist($job_id = 0,$competency_id = 0,$level_id = 0){
        $tamp = $this->where('job_id',$job_id)
            ->where('competency_id',$competency_id)
            ->where('level_id',$level_id)
            ->first(['competency_id','job_id','level_id','description','flag_active','start_date','end_date']);

        if($tamp){
            return true;
        }
        else{
            return false;
        }
    }

    public function CompetencyDetailIsActive($job_id = 0,$competency_id = 0,$level_id = 0){
        $tamp = $this->where('flag_active',1)
            ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
            ->where(function($query){
                $query->whereNull('end_date')
                    ->orWhere('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
            })
            ->where('job_id',$job_id)
            ->where('competency_id',$competency_id)
            ->where('level_id',$level_id)
            ->first(['competency_id','job_id','level_id','description','flag_active','start_date','end_date']);

        if($tamp){
            return true;
        }
        else{
            return false;
        }
    }

    public function getLeastCompetencyForAttemptsQuiz($job_id,$arrayAttemptsQuestionId){
        $competency_details = $this->where('job_id',$job_id)
            ->select('competency_id')
            ->groupBy('competency_id')
            ->inRandomOrder()
            ->get();
        $item = [];
        foreach ($competency_details as $competency_detail) {
            $jumlahCompetency = count(DB::table('mst_question')->whereIn('question_id',$arrayAttemptsQuestionId)->where('competency_id',$competency_detail->competency_id)->get());
            $item[$competency_detail->competency_id] = $jumlahCompetency;
        }
        asort($item);
        return array_keys($item);
    }

    public function createData(array $competencyDetails){
        $this->create(
            ['competency_id' => $competencyDetails['competency_id'],
            'job_id' => $competencyDetails['job_id'],
            'level_id' => $competencyDetails['level_id'],
            'description' => $competencyDetails['description'] ?? 'description',
            'start_date' => $competencyDetails['start_date'] ?? '2019-01-01 00:00:00',
            'end_date' => $competencyDetails['end_date'] ?? null,
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }

    public function updateData($request){
        $this->where('competency_id',$request->input('competency_id'))
            ->where('job_id',$request->input('job_id'))
            ->where('level_id', $request->input('level_id'))
            ->update([
                'description' => $request->input('description'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'flag_active' => $request->input('flag_active'),
                'last_update_by' => Auth::user()->id
            ]);
    }

    public function unactivatedData($job_id,$competency_id,$level_id){
        $this->where('competency_id',$competency_id)
            ->where('job_id',$job_id)
            ->where('level_id',$level_id)
            ->update(
                ['flag_active' => 0,
                'last_update_by' => Auth::user()->id ]
            );
    }

    public function activateData($job_id,$competency_id,$level_id){
        $this->where('competency_id',$competency_id)
            ->where('job_id',$job_id)
            ->where('level_id',$level_id)
            ->update(
                ['flag_active' => 1,
                'last_update_by' => Auth::user()->id ]
            );
    }

}
