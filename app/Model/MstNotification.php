<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
use DateTime;
use DB;
use Auth;
use App\Model\MstScoring;

class MstNotification extends Model
{
    use Sortable;
    protected $table = 'mst_notification';
    protected $primaryKey='notification_id';
    protected $fillable = ['notification_name','flag_active','template','type','created_by','last_updated_by','start_date','end_date'];

    public function destroyData($id){
        $this->where('notification_id',$id)->update(
            ['flag_active' => 0 ]
        );
    }
    public function restoreDaily($id){
        
        $data = $this->where('notification_id',$id)->first();
        $cek = $this->where('notification_name',$data->notification_name)->where('type', $data->type)->where('flag_active','1')->first();
        
        if($cek==null)
        {
            $this->where('notification_id',$id)->update(
                ['flag_active' => 1 ]
            );
            return 2;
        }
        else
        {
            return 1;
        }
        
    }

    protected $guarded = [
        'created_at','updated_at'
    ];

    public $sortable = [
        'notification_name','start_date','end_date','flag_active'
    ];

    public function createData(Request $request){

        $tempName = $request->input('notification_name');
        $tempType = $request->input('type');

        $data = $this->checkData($tempName,$tempType);
        if($data==3)
        {
            $this->buatData($request);
        }

        return $data;
    }

    public function createDataCustom(Request $request){
        $tempName = $request->input('notification_name_reminder');
        $tempType = $request->input('type_reminder');

        $data = $this->checkData($tempName,$tempType);
        if($data==3)
        {
            $tempName2 = $request->input('notification_name_feedback');
            $tempType2 = $request->input('type_feedback');

            $data2 = $this->checkData($tempName2,$tempType2);
            
            if($data2==3)
            {
                $this->buatDataCustom($request);
            }

            return $data2;
        }

        return $data;

    }

    public function buatData(Request $request){
        if($request->input('end_date')==null)
        { 
            $this->create(
                ['notification_name' => $request->input('notification_name'),
                'template' => $request->input('template'),
                'type' => $request->input('type'),
                'flag_active' => 1,
                'start_date' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'end_date' => '9999-01-01',
                'last_updated_by' => 1,
                'created_at' =>  Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'created_by' => 1,
                'default' => 0
            ]);   
        }
        else
        {
            $this->create(
                ['notification_name' => $request->input('notification_name'),
                'template' => $request->input('template'),
                'type' => $request->input('type'),
                'flag_active' => 1,
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'last_updated_by' => Auth::user()->id,
                'created_at' =>  Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'created_by' => Auth::user()->id,
                'default' => 0
            ]); 
        }
    }

    public function buatDataCustom(Request $request){
        $this->create(
            [
                'notification_name' => $request->input('notification_name_reminder'),
                'template' => $request->input('template_reminder'),
                'type' => $request->input('type_reminder'),
                'flag_active' => 1,
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'last_updated_by' => Auth::user()->id,
                'created_at' =>  Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'created_by' => Auth::user()->id,
                'default' => 0
            ]);
        
        $this->create(
            [
                'notification_name' => $request->input('notification_name_feedback'),
                'template' => $request->input('template_feedback'),
                'type' => $request->input('type_feedback'),
                'flag_active' => 1,
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'last_updated_by' => Auth::user()->id,
                'created_at' =>  Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'created_by' => Auth::user()->id,
                'default' => 0
            ]);

        MstScoring::create(
            ['scoring_type' => $request->input('scoring_type'),
            'type' => $request->input('type'),
            'score' => $request->input('score'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id
            ]);
    }

    public function checkData($tempName,$tempType)
    {
        if($tempName == 'Custom Seasonal Format' && $tempType == 'Daily' || $tempName == 'Custom Seasonal Format Correct' && $tempType == 'Feedback' || $tempName == 'Custom Seasonal Format Uncorrect' && $tempType == 'Feedback' || $tempName == 'Custom Seasonal Format Loyalty' && $tempType == 'Loyalty')
        {
            $cek = $this->where('notification_name',$tempName)->where('type', $tempType)->where('flag_active','1')->first();

            if($cek==null)
            {
                return 3;
            }
            else
            {
                return 2;
            }

        }
        else
        { 
            return 3;
        } 
    }

    public function updateData(Request $request,$id){

        if($id==1 || $id==2 || $id==3 || $id==4 || $id==5 || $id==6 || $id==7)
        {
            $this->where('notification_id',$id)->update(
                ['template' => $request->input('template'),
                'updated_at' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'last_updated_by' => Auth::user()->id
            ]);
        }
        else
        {
            $this->where('notification_id',$id)->update(
                ['template' => $request->input('template'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'updated_at' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'last_updated_by' => Auth::user()->id,
                'default' => 0
            ]);
        }
        
    }
    
    public function updateDataDefault(Request $request){
        $updated = $this->where('notification_name','=',$request->input('notification_name'))
        ->where('notification_id','=',$request->input('notification_id'))
        ->where('type','=',$request->input('type'))
        ->update(
            ['template' => $request->input('template'),
            'updated_at' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'last_updated_by' => Auth::user()->id
        ]);

    }
    public function getSortAbleData($getNonActiveData = false){
        if(!$getNonActiveData){
            return $this->where('flag_active',1)
                ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
                ->sortable(['notification_id','notification_name','template','start_date','end_date','flag_active']);
        }
        else if($getNonActiveData){
            return $this->where('type','<>','Loyalty')
            ->sortable(['notification_id','notification_name','template','start_date','end_date','flag_active']);
        }
    }

    public function getSearchAndFilterData(Request $request){
        $notifications =  $this->getSortAbleData(true); 

        if($request->get('search')){
            $notifications =  $notifications->Where(function ($query) use ($request) {
                $query->where('notification_name','like',"%".$request->get('search')."%")
                    ->orWhere('template','like',"%".$request->get('search')."%");
            });
        }
        if ($request->get('start_date') && $request->get('start_date') != '') {
            $notifications =  $notifications->where('start_date','>=',$request->get('start_date'));
        }
        if ($request->get('end_date') && $request->get('end_date') != '') {
            $notifications =  $notifications->where('end_date','<=',$request->get('end_date'));
        }
        if ($request->get('flag_active') != null) {
            $notifications =  $notifications->where('flag_active',$request->get('flag_active'));  
        }
        if ($request->get('type') != null) {
            $notifications =  $notifications->where('type',$request->get('type'));  
        }
        
        return $notifications =  $notifications->orderby('notification_id','desc')->paginate(15);
    }

    public function getDataDefault(){
        return $this->where('flag_active',1)
            ->where('start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
            ->where('end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString())
            ->get();
    }
}
