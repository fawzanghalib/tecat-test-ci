<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use EloquentFilter\Filterable;
use App\ModelFilters\MstModulFilter;
use Auth;

class MstModul extends Model
{
    use Filterable;
    protected $table = 'mst_modul';
    protected $primaryKey='modul_id';
    public $timestamps = true;
    use Sortable;

    protected $guarded = [
        'created_at','updated_at'
    ];

    public $sortable = [
        'modul_name','start_date','end_date','description'
    ];

    public function getData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $modul = $this;

        if(!$getNonActiveData){
            $modul =  $modul->where('mst_modul.flag_active',1)
                ->where('mst_modul.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_modul.end_date')
                        ->orWhere('mst_modul.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }
        if(array_key_exists('modul_id',$conditions)){
            $modul = $modul->where('mst_modul.modul_id',$conditions['modul_id']);
        }

        if(!empty($relationships)){
            $modul = $modul->with($relationships);
        }

        return $modul->get(['modul_id','modul_name','description','start_date','end_date','flag_active']);
    }

    public function getSingleData($getNonActiveData = false, array $conditions = [],array $relationships = []){
        $modul = $this;

        if(!$getNonActiveData){
            $modul =  $modul->where('mst_modul.flag_active',1)
                ->where('mst_modul.start_date','<=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString() )
                ->where(function($query){
                    $query->whereNull('mst_modul.end_date')
                        ->orWhere('mst_modul.end_date','>=',\Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString());
                });
        }
        if(array_key_exists('modul_id',$conditions)){
            $modul = $modul->where('mst_modul.modul_id',$conditions['modul_id']);
        }
        if(array_key_exists('modul_name',$conditions)){
            $modul = $modul->where('mst_modul.modul_name',$conditions['modul_name']);
        }

        if(!empty($relationships)){
            $modul = $modul->with($relationships);
        }
        
        return $modul->first(['modul_id','modul_name','description','start_date','end_date','flag_active']);
    }

    public function getFilterData(Request $request){
        $data = $this;
        /* OLD

        $MstModulFilter = MstModulFilter::class;

        return $this->filter($request->all(), $MstModulFilter)
            ->sortable(['modul_id','modul_name','description','start_date','end_date','flag_active'])
            ->get();
        */
        
        // search range date

        if($request->get('search')){
            $data =  $this->Where(function ($query) use ($request) {
                $query->where('mst_modul.modul_name','like',"%".$request->get('search')."%")
                    ->orWhere('mst_modul.description','like',"%".$request->get('search')."%");
            });
        }
        

        if ($request->get('start_date') && $request->get('start_date') != '') {
            $data =  $this->where('start_date','>=',$request->get('start_date'));
        }
        if ($request->get('end_date') && $request->get('end_date') != '') {
            $data =  $this->where('end_date','<=',$request->get('end_date'));
        }
        if ($request->get('flag_active') != null) {
            $data =  $this->where('flag_active',$request->get('flag_active'));  
        }

        if ($request->get('start_date') && $request->get('end_date') != '') {
            $data =  $this->where('start_date','>=',$request->get('start_date'))
            ->where('end_date','>',$request->get('end_date'));
        }
        

        return $data->sortable(['modul_id','modul_name','description','start_date','end_date','flag_active'])
        ->get();
    }

    public function createData(Request $request){
        $this->create(
            ['modul_name' => $request->input('modul_name'),
            'description' => $request->input('description'),
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]);
    }

    public function updateData(Request $request){
        $this->where('modul_id',$request->input('modul_id'))
            ->update(
                ['modul_name' => $request->input('modul_name'),
                'description' => $request->input('description'),
                'flag_active' => $request->input('flag_active'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'last_update_by' => Auth::user()->id]);
    }

    public function unactivatedData($id){
        $this->where('modul_id',$id)->update(
            ['flag_active' => 0,
            'last_update_by' => Auth::user()->id ]
        );
    }

    public function activateData($id){
        $this->where('modul_id',$id)->update(
            ['flag_active' => 1,
            'last_update_by' => Auth::user()->id ]
        );
    }
}
