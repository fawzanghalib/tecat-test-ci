<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_FailedUploadQuestion2 implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->Modul;
        $row[] = $data->Question;
        $row[] = $data->AnswerA;
        $row[] = $data->AnswerB;
        $row[] = $data->AnswerC;
        $row[] = $data->AnswerD;
        $row[] = $data->Answer;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Topik / Learning Point',
            'Soal',
            'A',
            'B',
            'C',
            'D',
            'Kunci Jawaban'
        ];
    }
}

?>