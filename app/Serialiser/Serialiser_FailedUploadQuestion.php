<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_FailedUploadQuestion implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->Function;
        $row[] = $data->Modul;
        $row[] = $data->Competency;
        $row[] = $data->Level;
        $row[] = $data->Job;
        $row[] = $data->Question;
        $row[] = $data->AnswerA;
        $row[] = $data->AnswerB;
        $row[] = $data->AnswerC;
        $row[] = $data->AnswerD;
        $row[] = $data->Answer;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Fungsi',
            'Topik / Learning Point',
            'Kompetensi',
            'Level',
            'Job Name',
            'Soal',
            'A',
            'B',
            'C',
            'D',
            'Kunci Jawaban'
        ];
    }
}

?>