<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_CAR implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->user_id;
        $row[] = $data->job_name;
        $row[] = $data->competency_name;
        $row[] = $data->total_soal;
        $row[] = $data->total_benar;
        $row[] = $data->total_salah;
        $row[] = $data->percentage;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'NPK',
            'Job Name',
            'Competency Name',
            'Total Soal',
            'Total Benar',
            'Total Salah',
            'Percentage'
        ];
    }
}

?>