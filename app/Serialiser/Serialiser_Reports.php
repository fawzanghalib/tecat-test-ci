<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_Reports implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->user_id;
        $row[] = $data->function_name;
        $row[] = $data->job_name;
        $row[] = $data->competency_name;
        $row[] = $data->level_name;
        $row[] = $data->question;
        $row[] = $data->answer;
        $row[] = $data->question_id;
        $row[] = $data->sum_score;
        $row[] = $data->time_start;
        $row[] = $data->time_finish;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'NPK',
            'Function Name',
            'Job Name',
            'Competency Name',
            'Level',
            'Question',
            'Answer',
            'Question Code',
            'Score',
            'Question Date',
            'Answer Date'
        ];
    }
}

?>