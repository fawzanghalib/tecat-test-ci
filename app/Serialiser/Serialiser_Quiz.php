<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_Quiz implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->question_id;
        $row[] = $data->question;
        $row[] = $data->level_name;
        $row[] = $data->package_name;
        $row[] = $data->job_name;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Question Code',
            'Question',
            'Level Name',
            'Package Name',
            'Job Name'
        ];
    }
}

?>