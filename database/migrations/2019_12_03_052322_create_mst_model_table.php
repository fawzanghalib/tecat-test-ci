<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_modul', function (Blueprint $table) {
            $table->increments('modul_id');
            $table->string('modul_name', 50)->nullable();
            $table->string('description', 250)->nullable();
            $table->string('flag_active', 1)->nullable()->default(1);
            $table->date('start_date')->nullable(); 
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_modul');
    }
}
