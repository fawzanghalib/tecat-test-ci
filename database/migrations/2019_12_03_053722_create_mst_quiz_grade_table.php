<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstQuizGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mst_quiz_grade');
        Schema::create('mst_quiz_grade', function (Blueprint $table) {
            $table->increments('quiz_grade_id');
            $table->integer('user_id')->notNullable()->unsigned()->nullable();
            $table->integer('score',0)->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_quiz_grade');
    }
}
