<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstQuizQueGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_quiz_que_grade', function (Blueprint $table) {
            $table->integer('quiz_id')->notNullable()->unsigned();
            $table->integer('question_id')->notNullable()->unsigned();
            // $table->integer('level_id')->notNullable()->unsigned();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
            $table->primary(['quiz_id', 'question_id'],'mst_quiz_que_grade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_quiz_que_grade');
    }
}
