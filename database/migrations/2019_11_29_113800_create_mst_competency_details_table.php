<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstCompetencyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mst_competency_details');

        Schema::create('mst_competency_details', function (Blueprint $table) {
            $table->integer('competency_id')->unsigned();
            $table->bigInteger('job_id')->unsigned();
            $table->integer('level_id')->unsigned();
            $table->string('description', 250)->nullable();
            $table->string('flag_active', 1)->nullable()->default(1);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
            $table->primary(['competency_id', 'job_id','level_id'],'mst_competency_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_competency_details');
    }
}
