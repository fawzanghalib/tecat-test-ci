<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::dropIfExists('mst_package');
        Schema::create('mst_package', function (Blueprint $table) {
            $table->increments('package_id');
            $table->string('package_name', 50)->notNullable();
            $table->string('description', 250)->notNullable();
            $table->string('flag_active', 1)->notNullable()->default(1);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_package');
    }
}
