<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstQuizResponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_quiz_respons', function (Blueprint $table){
            $table->increments('respons_id');
            $table->integer('attempts_id')->unsigned()->notNullable();
            //$table->integer('user_id')->unsigned()->notNullable();
            $table->integer('answer');
            $table->integer('sum_score'); // untuk melihat benar atau salah, jika benar maka int 1 jika salah int 2
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_quiz_respons');
    }
}
