<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFunctionToQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_question', function (Blueprint $table) {
            $table->integer('function_id')->unsigned()->notNullable();
        });

        DB::table('mst_question')
            ->leftJoin('mst_job','mst_question.job_id','=','mst_job.job_id')
            ->update([
                'mst_question.function_id' => DB::raw('mst_job.function_id')
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('mst_question', 'function_id')){
            Schema::table('mst_question', function (Blueprint $table){
                $table->dropColumn('function_id');
            });
        }
    }
}
