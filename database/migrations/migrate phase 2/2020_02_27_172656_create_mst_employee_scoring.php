<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstEmployeeScoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_employee_scoring', function (Blueprint $table) {
            $table->increments('employee_scoring_id');
            $table->integer('npk')->unsigned()->notNullable();
            $table->integer('loyalty_question_id')->unsigned()->nullable();
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->integer('scoring_id')->unsigned()->notNullable();
            $table->integer('score')->integer()->notNullable();
            $table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_employee_scoring');
    }
}
