<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstQuizAttemptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_quiz_attempts', function (Blueprint $table){
            $table->increments('attempts_id');
            $table->integer('quiz_id')->notNullable()->unsigned();
            $table->integer('question_id')->notNullable()->unsigned();
            $table->integer('user_id')->notNullable()->unsigned();
            $table->integer('scoring_id')->nullable()->unsigned();
            $table->integer('sum_score')->nullable();
            $table->dateTime('time_start');
            $table->dateTime('time_finish')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_quiz_attempts');
    }
}
