<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstCompetencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_competency', function (Blueprint $table) {
            $table->increments('competency_id');
            $table->string('competency_name',50)->nullable();
            $table->string('description',100)->nullable();
            $table->string('flag_active',1)->nullable()->default(1);
            $table->date('start_date')->nullable();;
            $table->date('end_date')->nullable();;
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_competency');
    }
}
