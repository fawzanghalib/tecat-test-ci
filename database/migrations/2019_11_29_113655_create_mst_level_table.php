<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_level', function (Blueprint $table) {
            $table->increments('level_id');
            $table->string('level_name',20)->notable();
            $table->string('description', 50)->nullable();
            $table->string('flag_active', 1)->nullable()->default(1);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->notNullable();
            $table->integer('created_by')->notNullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_level');
    }
}
