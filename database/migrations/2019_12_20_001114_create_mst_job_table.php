<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('mst_job', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('function_id')->unsigned()->nullable();
            $table->bigInteger('job_id')->unsigned()->nullable()->unique();;
            $table->string('job_name' , 100)->nullable();
            $table->string('description', 150)->nullable();
            $table->string('flag_active', 0)->nullable()->default(1);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_job');
	}

}
