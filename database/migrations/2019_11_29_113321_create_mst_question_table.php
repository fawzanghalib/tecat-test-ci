<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_question', function (Blueprint $table) {
            $table->increments('question_id');
            $table->integer('package_id')->unsigned()->notNullable();
            $table->integer('competency_id')->unsigned()->notNullable()->default(0);
            $table->bigInteger('job_id')->unsigned()->notNullable()->default(0);
            $table->integer('level_id')->unsigned()->notNullable()->default(0);
            $table->integer('modul_id')->unsigned()->nullable();
            $table->integer('question_type_id')->notNullable();
            $table->text('question')->notNullable();
            $table->string('flag_loyalty',1)->nullable();
            $table->string('flag_active',1)->nullable()->default(1);
            $table->string('npk',5)->nullable();
            $table->date('start_date')->nullable(); 
            $table->date('end_date')->nullable(); 
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_question');
    }
}
