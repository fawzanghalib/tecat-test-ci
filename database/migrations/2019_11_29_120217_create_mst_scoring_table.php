<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstScoringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_scoring',function (Blueprint $table){
            $table->increments('scoring_id');
            $table->string('scoring_type')->notNullable();
            $table->integer('score')->notNullable();
            $table->integer('type')->unsigned()->notNullable();
            $table->string('flag_active', 1)->nullable()->default(1);
            $table->date('start_date')->nullable(); 
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_scoring');
    }

}
