<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstQuestionAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_question_answer', function (Blueprint $table) {
            $table->increments('question_answer_id');
            $table->integer('question_id')->unsigned()->notNullable();
            $table->boolean('score')->nullable(); //Mengetahui apakah jawaban ini benar atau salah
            $table->string('answer')->nullable();
            //$table->integer('learning_point',5)->nullable();
            //$table->string('flag_active', 1)->nullable()->default(1);
            $table->date('start_date')->nullable(); 
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('last_update_by')->unsigned()->notNullable();
            $table->integer('created_by')->unsigned()->notNullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_question_answer');
    }
}
