<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_notification',function (Blueprint $table){
            $table->increments('notification_id');
            $table->string('notification_name'); // DAILY | DAILY REMINDER | SEASONAL | OTA
            $table->string('template', 350); // HAI SAHABAT ACC,.....
            $table->string('type'); // DAILY | FEEDBACK | LOYALTY | BROADCAST
            $table->string('flag_active')->nullable();
            $table->date('start_date')->nullable(); 
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->integer('created_by')->unsigned()->notNullable();
            $table->integer('last_updated_by')->unsigned()->notNullable();
            $table->string('default')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_notification');
    }
    
}
