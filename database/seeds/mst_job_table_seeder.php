<?php

use Illuminate\Database\Seeder;

class mst_job_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstJob('CREDIT ANALYST',300000001909944,4);
		$this->insertToMstJob('TELLER PDC',300000001909684,3);
		$this->insertToMstJob('SERVICE RELATION OFFICER',300000001909876,3);
		$this->insertToMstJob('AP PAYMENT ADMINISTRATOR',300000001909264,4);
		$this->insertToMstJob('TELLER',300000001910564,3);
		$this->insertToMstJob('CUSTOMER SERVICE OFFICER',300000001910146,3);
		$this->insertToMstJob('RETAIL COMMERCIAL CREDIT ANALYST',300000001909564,4);
		$this->insertToMstJob('AR HANDLING OFFICER',300000001909715,2);
		$this->insertToMstJob('CREDIT ADMINISTRATOR',300000001910713,4);
		$this->insertToMstJob('FINANCE & BANKING OFFICER',300000001910837,3);
		$this->insertToMstJob('SURVEYOR',300000001910216,4);
		$this->insertToMstJob('SALES - HEAD',300000001910902,1);
		$this->insertToMstJob('SALES - MANAGER',300000011814579,1);
		$this->insertToMstJob('SALES ACTING HEAD',300000001910578,1);
		$this->insertToMstJob('SALES ACTING MANAGER',300000010348749,1);
		$this->insertToMstJob('SALES ACTING MANAGER USED CAR',300000001910433,1);
		$this->insertToMstJob('SALES AGENT FRONTLINER',300000030305904,5);
		$this->insertToMstJob('SALES AGENT LEADER',300000001910664,5);
		$this->insertToMstJob('SALES AGENT LEADER MIXED',300000001910168,5);
		$this->insertToMstJob('SALES AGENT WIN',300000017651127,5);
		$this->insertToMstJob('SALES HEAD',300000001910206,1);
		$this->insertToMstJob('SALES MANAGER',300000008052294,1);
		$this->insertToMstJob('SALES MANAGER USED CAR',300000001909618,1);
		$this->insertToMstJob('SALES UNDERWRITER - ASTRA COMMERCIAL',300000007112607,1);
		$this->insertToMstJob('SALES UNDERWRITER - ASTRA DIRECT',300000001910181,1);
		$this->insertToMstJob('SALES UNDERWRITER - ASTRA INDIRECT',300000001909362,1);
		$this->insertToMstJob('SALES UNDERWRITER - ASTRA LUXURY',300000007382384,1);
		$this->insertToMstJob('SALES UNDERWRITER - ASTRA MIX',300000001910270,1);
		$this->insertToMstJob('SALES UNDERWRITER - LUXURY',300000040957994,1);
		$this->insertToMstJob('SALES UNDERWRITER - MIXED BRAND',300000001910163,1);
		$this->insertToMstJob('SALES UNDERWRITER - NONA',300000001910458,1);
		$this->insertToMstJob('SALES UNDERWRITER - USED',300000001909343,1);
		$this->insertToMstJob('SALES UNDERWRITER - USED & NONA',300000001910198,1);
		$this->insertToMstJob('SALES UNDERWRITER - USED CAR',300000001910018,1);
		$this->insertToMstJob('AR REPOSSESSION OFFICER',300000001910762,2);
		$this->insertToMstJob('RECOVERY MANAGEMENT OFFICER',300000001910596,2);
		$this->insertToMstJob('SALES & SURVEY OFFICER',300000001910096,1);
		$this->insertToMstJob('BASE MASTER & CREDIT ANALYST',300000001910670,4);
		$this->insertToMstJob('BPKB CUSTODIAN',300000001910276,3);
		$this->insertToMstJob('SALES OFFICER USED CAR',300000001909434,1);
		$this->insertToMstJob('SALES OFFICER HEAD - NONA',300000001909380,1);
		$this->insertToMstJob('SALES OFFICER HEAD - ASTRA',300000001909667,1);
		$this->insertToMstJob('SALES OFFICER - C2C SHOWROOM',300000001909403,5);
		$this->insertToMstJob('SALES OFFICER - WALK IN',300000001909811,1);
		$this->insertToMstJob('SALES OFFICER - USED & NONA',300000001909491,1);
		$this->insertToMstJob('SALES OFFICER - USED CAR',300000001909527,1);
		$this->insertToMstJob('SALES OFFICER - MIXED BRAND',300000001909709,1);
		$this->insertToMstJob('SALES OFFICER - ASTRA INDIRECT',300000001909582,1);
		$this->insertToMstJob('SALES OFFICER - ASTRA DIRECT',300000001909689,1);
		$this->insertToMstJob('SALES OFFICER - NONA',300000001909779,1);
		$this->insertToMstJob('SALES OFFICER - ASTRA COMMERCIAL',300000007112653,1);
		$this->insertToMstJob('SALES OFFICER HEAD - MIXED BRAND',300000014491440,1);
		$this->insertToMstJob('SALES OFFICER - HEAD - MIXED BRAND',300000014573899,1);
		$this->insertToMstJob('SALES OFFICER HEAD - USED CAR',300000001910493,1);
		$this->insertToMstJob('SALES OFFICER - HEAD - USED CAR',300000001910277,1);
		$this->insertToMstJob('SALES OFFICER ACTING HEAD - ASTRA',300000001910294,1);
		$this->insertToMstJob('SALES OFFICER - HEAD - ASTRA',300000001910524,1);
		$this->insertToMstJob('SALES OFFICER - ASTRA MIX',300000001910678,1);
		$this->insertToMstJob('SALES OFFICER HEAD - MULTIGUNA',300000038890956,1);
		$this->insertToMstJob('SALES OFFICER - LUXURY',300000042226060,1);
		$this->insertToMstJob('SALES OFFICER - C2C',300000001909726,5);
		$this->insertToMstJob('SALES OFFICER - C2C AGENT',300000001909847,5);
		$this->insertToMstJob('SALES OFFICER - DIRECT',300000001909934,1);
		$this->insertToMstJob('SALES OFFICER - C2C DIRECT',300000001910953,5);
    }

    public function insertToMstJob($job_name,$job_id,$function_id){
		DB::table('mst_job')->insert(
			['function_id' => $function_id,
			'job_id' => $job_id,
			'job_name' => $job_name,
			'description' => 'description',
			'flag_active' => '1',
			'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
			'end_date' => '9999-01-01 00:00:00',
			'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
			'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
			'created_by' => '1',
			'last_update_by' => '1',]
		);
	}
}
