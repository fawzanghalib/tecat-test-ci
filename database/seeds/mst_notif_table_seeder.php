<?php

use Illuminate\Database\Seeder;

class mst_notif_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertDefaultCorrectAnswer();
        $this->insertDefaultUncorrectAnswer();
        $this->insertDefaultOnTimeAnsweredQuestion();
        $this->insertDefaultReminderPagi();
        $this->insertDefaultLoyaltySubmit();
        $this->insertDefaultLoyaltyDeclined();
        $this->insertDefaultLoyaltyApproved();
    }

    
    public function insertDefaultCorrectAnswer()
    {
        DB::table('mst_notification')->insert([ 
            'notification_name' => 'Master Correct Answer Notification Format',
            'template' => 'Selamat! Hari ini kamu menjawab Daily Test dengan benar, point mu bertambah +5.',
            'type' => 'Feedback',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'flag_active' => '1',
            'created_by' => 1,
            'last_updated_by' => 1,
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => '9999-01-01',
            'default' => '1'
        ]);
    }

    public function insertDefaultUncorrectAnswer()
    {
        DB::table('mst_notification')->insert([
            'notification_name' => 'Master Uncorrect Answer Notification Format',
            'template' => 'Sayang sekali, jawabanmu tidak tepat. Point kamu berkurang -3. Kamu bisa belajar lagi yaa, banyak modul-modul ter-update di ACC Self Learning.',
            'type' => 'Feedback',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'flag_active' => '1',
            'created_by' => 1,
            'last_updated_by' => 1,
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => '9999-01-01',
            'default' => '1'
        ]);
    }

    public function insertDefaultOnTimeAnsweredQuestion()
    {
        DB::table('mst_notification')->insert([
            'notification_name' => 'Master On Time Answered Question Notification Format',
            'template' => 'Karena kamu menjawab tepat waktu, kamu mendapatkan tambahan point +5. Keep it up!',
            'type' => 'Daily',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'flag_active' => '1',
            'created_by' => 1,
            'last_updated_by' => 1,
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => '9999-01-01',
            'default' => '1'
        ]);
    }

    public function insertDefaultReminderPagi()
    {
        DB::table('mst_notification')->insert([
            'notification_name' => 'Master Daily Notification Format',
            'template' => 'Hai sahabat ACC. Daily test mu menanti. Ada beberapa soal yang belum terjawab. Yuk, jangan lupa dijawab yaa... ',
            'type' => 'Daily',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'flag_active' => '1',
            'created_by' => 1,
            'last_updated_by' => 1,
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => '9999-01-01',
            'default' => '1'
        ]);
    }

    public function insertDefaultLoyaltySubmit()
    {
        DB::table('mst_notification')->insert([
            'notification_name' => 'Master After Contributor Submit the Question',
            'template' => 'Soal mu berhasil diupload. Terima kasih ya kontribusinya. Saat ini komite bank soal akan mereview soal tersebut. Status progressnya akan dikabarkan lebih lanjut.',
            'type' => 'Loyalty',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'flag_active' => '1',
            'created_by' => 1,
            'last_updated_by' => 1,
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => '9999-01-01',
            'default' => '1'
        ]);
    }

    public function insertDefaultLoyaltyDeclined()
    {
        DB::table('mst_notification')->insert([
            'notification_name' => 'Master Question Submitted Declined',
            'template' => 'Hai, terima kasih atas kontribusinya. Soal yang kamu upload belum dapat disetujui oleh komite bank soal dengan alasan pertanyaan / jawaban tidak valid atau soal tersebut sudah tersedia di bank soal kami. Nanti coba lagi yaa!',
            'type' => 'Loyalty',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'flag_active' => '1',
            'created_by' => 1,
            'last_updated_by' => 1,
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => '9999-01-01',
            'default' => '1'
        ]);
    }

    public function insertDefaultLoyaltyApproved()
    {
        DB::table('mst_notification')->insert([
            'notification_name' => 'Master Question Submitted Approved',
            'template' => 'Selamat! Soal yang kamu upload telah disetujui oleh Kakak Komite Bank Soal. Kamu berhak mendapat point +5.',
            'type' => 'Loyalty',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'flag_active' => '1',
            'created_by' => 1,
            'last_updated_by' => 1,
            'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'end_date' => '9999-01-01',
            'default' => '1'
        ]);
    }
}
