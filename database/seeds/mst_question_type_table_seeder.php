<?php

use Illuminate\Database\Seeder;

class mst_question_type_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstQuestionType('Competencies');
        $this->insertToMstQuestionType('General');
    }

    public function insertToMstQuestionType( $questionTypeName ){
        DB::table('mst_question_type')->insert([
	        'question_type_name' => $questionTypeName ,
	        'description' => 'description for '.$questionTypeName ,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1'
	    ]);
    }
}
