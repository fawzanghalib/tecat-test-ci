<?php

use Illuminate\Database\Seeder;

class users_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertSuperAdmin();
        $this->insertAdmin();
    }

    public function insertSuperAdmin()
    {
        DB::table('users')->insert([
	        'name' => 'Super Admin',
	        'email' => 'hclearningmanagement@gmail.com', // hclearningmanagement@gmail.com
            'password' => '$2y$10$jH/tYT6.Yx.jJhCFZtSA/O4ipELhWW4lsLtMqX4ufkW4lnE2f7lGK', // Le@rning1!
            'flag_active' => '1',
            'remember_token' => '0vzFEJs1n8STJAcxMxOGFG0QRu8W6ngm6rzre1b57cbZYCT2Hkfm5k7sxFor',
            'created_at' => '2020-01-01 00:00:00',
            'updated_at' => '2020-01-01 00:00:00',
            'npk' => '00001',
            'role' => '1' // Super Admin
        ]);
    }
    
    public function insertAdmin()
    {
        DB::table('users')->insert([
	        'name' => 'Hesti Asmiliaty',
	        'email' => 'hesti.asmiliaty@acc.co.id', // hesti.asmiliaty@acc.co.id
            'password' => '$2y$10$jH/tYT6.Yx.jJhCFZtSA/O4ipELhWW4lsLtMqX4ufkW4lnE2f7lGK', // Le@rning1!
            'flag_active' => '1',
            'remember_token' => '0vzFEJs1n8STJAcxMxOGFG0QRu8W6ngm6rzre1b57cbZYCT2Hkfm5k7sxFor',
            'created_at' => '2020-01-01 00:00:00',
            'updated_at' => '2020-01-01 00:00:00',
            'npk' => '13146',
            'role' => '2' // Admin
        ]);
    }
}
