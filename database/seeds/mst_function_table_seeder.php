<?php

use Illuminate\Database\Seeder;

class mst_function_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstFunction('SALES','1');
        $this->insertToMstFunction('AR MANAGEMENT','2');
        $this->insertToMstFunction('SERVICE','3');
        $this->insertToMstFunction('UNDERWRITING','4');
        $this->insertToMstFunction('SALES MGU','5');
    }

    public function insertToMstFunction( $function_name , $function_id){
        DB::table('mst_function')->insert([
            'function_id' => $function_id,
	        'function_name' => $function_name,
	        'description' => 'description for '.$function_name,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1',
        ]);
    }
}
