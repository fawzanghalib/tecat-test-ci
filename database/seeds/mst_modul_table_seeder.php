<?php

use Illuminate\Database\Seeder;

class mst_modul_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstModul('modul 1');
        $this->insertToMstModul('modul 2');
        $this->insertToMstModul('modul 3');
    }

    public function insertToMstModul($modul_name){
        DB::table('mst_modul')->insert([
	        'modul_name' => $modul_name,
	        'description' => 'description for '.$modul_name,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1',
	    ]);
    }
}
