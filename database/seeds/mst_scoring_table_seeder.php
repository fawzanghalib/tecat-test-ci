<?php

use Illuminate\Database\Seeder;

class mst_scoring_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstScoring('default jawaban benar',5,1);
        $this->insertToMstScoring('default jawaban benar & tepat waktu',10,2);
        $this->insertToMstScoring('default jawaban salah',-3,3);
        $this->insertToMstScoring('default jawaban salah & tepat waktu',2,4);
    }

    public function insertToMstScoring( $scoring_type,$score,$type){
        DB::table('mst_scoring')->insert([
	        'scoring_type' => $scoring_type,
	        'score' => $score,
	        'type' => $type,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1',
        ]);
    }
}
