<?php

use Illuminate\Database\Seeder;

class mst_competency_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstCompetency('DOC','Dokumentasi');
        $this->insertToMstCompetency('AKT','Aplikasi Komputer Terapan');
        $this->insertToMstCompetency('TAK','Teknik Appraisal Kendaraan');
        $this->insertToMstCompetency('TPO','Teknik  Pengelolaan Pool');
        $this->insertToMstCompetency('CS1','Customer Knowledge 1');
        $this->insertToMstCompetency('CS2','Customer Knowledge 2');
        $this->insertToMstCompetency('PBU','Pembukuan');
        $this->insertToMstCompetency('TKT','Teknok Komunikasi Telepon');
        $this->insertToMstCompetency('TPA','Teknik Penanganan AR');
        $this->insertToMstCompetency('CRD','Credit');
        $this->insertToMstCompetency('SCS','Segmentasi Customer');
        $this->insertToMstCompetency('SWI','Segmentasi Wilayah');
        $this->insertToMstCompetency('LGD','Legal Documentation');
        $this->insertToMstCompetency('LIT','Litigation');
        $this->insertToMstCompetency('NEG','Negosiasi');
        $this->insertToMstCompetency('MRI','Riset Pasar & Marktet Intelligence');
        $this->insertToMstCompetency('SAP','System & Alat Pembayaran Perbankan');
        $this->insertToMstCompetency('CF','Customer Focused');
        $this->insertToMstCompetency('DPR','Document Processing');
        $this->insertToMstCompetency('ET','Excel Terapan');
        $this->insertToMstCompetency('LDL','Legal Documentation & Litigation');
        $this->insertToMstCompetency('CS','Customer Knowledge');
    }

    public function insertToMstCompetency( $competencyName, $competencyDescription ){
        DB::table('mst_competency')->insert([
	        'competency_name' => $competencyName,
	        'description' => $competencyDescription,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1',
        ]);
    }
}
