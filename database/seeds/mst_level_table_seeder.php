<?php

use Illuminate\Database\Seeder;

class mst_level_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstLevel('1');
        $this->insertToMstLevel('2');
        $this->insertToMstLevel('3');
        $this->insertToMstLevel('4');
        $this->insertToMstLevel('5');
    }

    public function insertToMstLevel( $levelName ){
        DB::table('mst_level')->insert([
	        'level_name' => $levelName,
	        'description' => 'description for level '.$levelName,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1',
        ]);
    }
}
