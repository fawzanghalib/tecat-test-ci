<?php

use Illuminate\Database\Seeder;

class mst_package_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertToMstPackage('Package 1');
        $this->insertToMstPackage('Package 2');
    }

    public function insertToMstPackage( $packageName ){
        DB::table('mst_package')->insert([
	        'package_name' => $packageName,
	        'description' => 'description for '.$packageName,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1',
        ]);
    }
}
