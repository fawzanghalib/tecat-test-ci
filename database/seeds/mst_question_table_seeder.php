<?php

use Illuminate\Database\Seeder;

class mst_question_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // package_id,competency_id,job_id,level_id,modul_id,question_type_id,question
        //$this->insertToMstQuestion(1,3,1,1,2,1,'hasil dari 2 x 2 adalah');
    }

    public function insertToMstQuestion( $package_id,$competency_id,$job_id,$level_id,$modul_id,$question_type_id,$question ){
        DB::table('mst_question')->insert([
	        'package_id' => $package_id,
	        'competency_id' => $competency_id,
	        'job_id' => $job_id,
	        'level_id' => $level_id,
	        'modul_id' => $modul_id,
	        'question_type_id' => $question_type_id,
	        'question' => $question,
	        'flag_active' => '1',
	        'start_date' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'end_date' => '9999-01-01 00:00:00',
	        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
	        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => '1',
            'last_update_by' => '1',
        ]);
    }
}
