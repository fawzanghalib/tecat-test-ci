@extends('layouts.home')
@section('title-tab')
    Competency Analysis Results
@endsection
@section('title-content')
<a href="{{ route('competency-analysis-results.index') }}" style="color:#212529">
    Competency Analysis Results
</a>
@endsection
@section('button')

@endsection

@section('main-content')
<div class="card">
    <div class="card-block">
        <form action="{{route('competency-analysis-results.index')}}" method="GET" autocomplete="off" style="margin-top:-50px">
            <div class="row">
                <div class="col-md-6">
                    <label class="mt-5">Job Name</label>
                    <div class="row">
                        <div class="col-md-9 pr-0 multiple-select-filter"  >
                            <select name="job_id[]" class="form-control custom-select select-job-filter" multiple="multiple">                                
                                @foreach ($jobs as $job)
                                    <option value="{{$job->job_id}}"
                                    @if ($request->get('job_id') !== null)    
                                        @if (in_array($job->job_id,$request->get('job_id'), FALSE))
                                            selected
                                        @endif
                                    @endif
                                    > {{$job->job_name}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="mt-5">Competency</label>
                    <div class="row">
                        <div class="col-md-9 pr-0 multiple-select-filter"  >
                            <select name="competency_id[]" class="form-control custom-select select-competency-filter" multiple="multiple">                                
                                @foreach ($competencies as $competency)
                                    <option value="{{$competency->competency_id}}"
                                        @if ($request->get('competency_id') !== null)
                                            @if (in_array($competency->competency_id,$request->get('competency_id'), FALSE))
                                                selected
                                            @endif  
                                        @endif  
                                    > {{$competency->competency_name}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>

                <div class="row mt-5 col-md-7">
                    <div class="col-md-4">
                        <button class="btn btn-warning btn-lg btn-block" name="action" type="submit" value="filter">Filter</button>
                    </div>
                    <div class="col-md-4">
                        <a href="{{route('competency-analysis-results.index')}}">
                            <button class="btn btn-outline-warning btn-lg btn-block" type="button">Reset</button>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card" >
    <div class="card-block">
        <div class="row">
            <div class="col-md-12">
                {{-- search form --}}
                <form action="{{route('competency-analysis-results.index')}}" method="GET" autocomplete="off">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="input-group mb-3 ">
                                @foreach ($request->query as $key => $value)
                                @if ($key != 'search' && $key != 'competency_id' && $key != 'job_id')
                                <input type="hidden" name="{{$key}}" value="{{$value}}">
                                @endif
                                @endforeach
                                <input name="search" type="text" class="form-control" placeholder="Search by NPK.."
                                    value="{{$request->get('search')}}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i
                                            class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <button class="btn btn-warning btn-md btn-block" name="action" type="submit" value="download">Download</button>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>


        @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <span>{{ session('status') }}</span>
                    </div>
                </div>
            </div>
        @endif

        <div class="table-responsive" style="overflow-x:auto;">
            <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm text-center" width="7%">@sortablelink('user_id','NPK')</th>
                        <th class="th-sm text-center" width="20%">Job Name</th>
                        <th class="th-sm text-center" width="4%">Competency</th>
                        <th class="th-sm text-center" width="9%">Total Question</th>
                        <th class="th-sm text-center" width="8%">Correct</th>
                        <th class="th-sm text-center" width="8%">Incorrect</th>
                        <th class="th-sm text-center" width="8%">%</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data->count())
                    @foreach($data as $datas)
                    <tr>
                        <td>{{ $datas->user_id }}</td>
                        <td>{{ $datas->job_name }}</td>
                        <td>{{ $datas->competency_name }}</td>
                        <td class="text-center">{{ $datas->total_soal }}</td>
                        <td class="text-center">{{ $datas->total_benar }}</td>
                        <td class="text-center">{{ $datas->total_soal - $datas->total_benar }}</td>
                        <td class="text-center"><?php echo number_format((100*$datas->total_benar / $datas->total_soal),1)."%"; ?></td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('script')
    <script>
        $(document).ready(function(){
            $(".select-competency-filter , .select-job-filter").select2();
        })
    </script>
@endsection

@endsection