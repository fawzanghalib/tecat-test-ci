@extends('layouts.home')
@section('title-tab')
    Master Function 
@endsection
@section('title-content')
    Master Function
@endsection
@section('button')
    {{-- <a class="ml-2" href="{{route('modul.download')}}" onclick="window.open( this.href ,'_blank');return false;">
        <button class="btn btn-md btn-outline-success pull-right">
            <i class="fas fa-file-download"></i>
            Download   
        </button>
    </a> --}}
    <a class="ml-2" href="{{route("function.create")}}">
        <button class="btn btn-md btn-warning pull-right">
            <i class="fas fa-plus-circle"></i>
            Add Function
        </button>
    </a>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <div class="table-responsive" style="overflow-x:auto;">
                <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm text-center">@sortablelink('function_name','Function Name')</th>
                            <th class="th-sm text-center">Description</th>
                            <th class="th-sm text-center">@sortablelink('start_date','Start Date')</th>
                            <th class="th-sm text-center">@sortablelink('end_date','End Date')</th>
                            <th class="th-sm text-center">Status</th>
                            <th class="th-sm text-center" width="12%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($functions->count())
                            @foreach($functions as $function)
                                <tr>
                                    <td>@php echo ucfirst($function->function_name) @endphp</td>
                                    <td>@php echo ucfirst($function->description) @endphp</td>
                                    <td class="text-center">{{ $function->start_date }}</td>
                                    <td class="text-center">{{ $function->end_date }}</td>
                                    <td class="text-center">
                                        @if ($function->flag_active == 1)
                                            Active
                                        @else
                                            No
                                        @endif
                                    </td>
                                    <td>
                                        <a class="float-left ml-2" href="{{route('function.edit',['id' => $function->function_id])}}">
                                            <button class="btn btn-sm btn-success">
                                                <i>edit</i>
                                            </button>
                                        </a>
                                        {{-- <form class="float-left ml-2 delete" action="{{route('function.delete',['id' => $function->function_id])}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i>disable</i>        
                                            </button>
                                        </form> --}}
                                        @if ($function->flag_active )
                                            <form class="float-left ml-2 delete" action="{{route('function.unactivated',['id' => $function->function_id])}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('put') }}
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i>disable</i> 
                                                </button>
                                            </form>
                                        @else
                                            <form class="float-left ml-2 delete" action="{{route('function.activate',['id' => $function->function_id])}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('put') }}
                                                <button type="submit" class="btn btn-sm btn-primary">
                                                    <i>enable</i>    
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
    </script>
@endsection