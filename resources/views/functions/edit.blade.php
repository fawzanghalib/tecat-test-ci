@extends('layouts.home')
@section('title-tab')
    Edit Function | {{$function->function_name}}
@endsection
@section('title-content')
    Edit Function
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('function')}}">Master Function</a></li>
    <li class="breadcrumb-item active">Edit Function</li>
</ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <form class="needs-validation" action="{{ route('function.update', ['id' => $function->function_id]) }}" method="POST">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            <div class="ro">
                <div class="card card-shadow col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="mb-3 col-md-6">
                            <label>Name<span style="color:red">*</span> </label>
                            <input name="function_name" type="text" class="form-control" maxlength="50" minlength="4" value="{{  old('function_name') ?? $function->function_name}}" required="" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Description<span style="color:red">*</span></label>
                            <textarea name="description" class="form-control" maxlength="250" minlength="6" rows="3" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>{{  old('description') ?? $function->description}}</textarea>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label>Flag Active :<span style="color:red">*</span></label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f1" type="radio" value="1" @if($function->flag_active == 1) checked @endif><label for="f1">Active</label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f2" type="radio" value="0" @if($function->flag_active == 0) checked @endif><label for="f2">non Active</label>
                        </div>
                        <div class="row ml-1">
                            <div class="col-md-3 mb-3">
                                <label>Start Date<span style="color:red">*</span></label>
                                <input name="start_date" type="date" id="sd1" onchange="setEndDate()" max="9999-12-31" class="form-control"  required value="{{ old('start_date') ?? $function->start_date }}" min="1">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>End Date</label>
                                <input name="end_date" type="date" id="ed1" max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $function->end_date }}" min="1">
                                {{-- <input name="end_date" type="date" id="ed1" disabled max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $function->end_date }}" min="1"> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
            </div>
            
        </form>
    </div>
</div>
@endsection