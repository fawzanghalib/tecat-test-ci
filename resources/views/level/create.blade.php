@extends('layouts.home')
@section('title-tab')
    Create Level
@endsection
@section('title-content')
    Create Level
@endsection
@section('button')

@endsection 
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('level')}}">Master Level</a></li>
    <li class="breadcrumb-item active">Create Level</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <div class="card card-shadow col-md-12">
                <form class="needs-validation" action="{{route('level.store')}}" method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    @if ($errors->any())
                    <ul class="alert alert-danger mt-3">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    <div class="card-body">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Level Name') }}<span style="color:red">*</span></label>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <input type="text" name="level_name" autocomplete="off"
                                    class="form-control" value="{{ old('level_name') }}" maxlength="50" minlength="1" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Description') }}<span style="color:red">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <textarea name="description" class="form-control" rows="3" maxlength="250" minlength="6" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/
                                    >{{ old('description') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Flag') }}<span style="color:red">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <td>
                                    <input type="radio" id="f1" name="flag_active" value="1" required />
                                    <label for="f1">&nbsp;Active</label>
                                    <a>&nbsp;&nbsp;</a>
                                    <input type="radio" id="f2" name="flag_active" value="0" required /> 
                                    <label for="f2">&nbsp;Non Active</label>
                                </td>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Start Date') }}<span style="color:red">*</span></label>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input type="date" max="9999-12-31" id="sd1" onchange="setEndDate()" name="start_date" class="form-control" required
                                    value="{{ old('start_date') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input type="date" max="9999-12-31" id="ed1" disabled name="end_date" class="form-control" required
                                    value="{{ old('end_date') }}">
                            </div>
                        </div>
                    </div></div></div>
                    <button class="btn btn-warning btn-block btn-lg" type="submit">Add Level</button>
                </form>
            
        </div>
    </div>
@endsection