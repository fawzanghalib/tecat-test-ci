@extends('layouts.home')
@section('title-tab')
    Edit Level | {{$level->level_name}}
@endsection
@section('title-content')
    Edit Level
@endsection
@section('button')

@endsection 
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('level')}}">Master Level</a></li>
    <li class="breadcrumb-item active">Edit Level</li>
</ol>
@endsection

@section('main-content')
    <div class="card ">
        <div class="card-block">
            <div class="mb-3 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('level.update', ['id' => $level->level_id]) }}"
                            method="POST">
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            @if ($errors->any())
                            <ul class="alert alert-danger mt-3">
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Level Name') }}<span style="color:red">*</span></label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input type="text" name="level_name" autocomplete="off"
                                            value="{{ $level->level_name }}" class="form-control" maxlength="50" minlength="1"
                                            required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Description') }}<span style="color:red">*</span></label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <textarea name="description" class="form-control" id="" maxlength="250" minlength="6" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"
                                            rows="3" required>{{ $level->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Flag') }}<span style="color:red">*</span></label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <td>
                                            <input type="radio" value="1" id="f1" name="flag_active" required
                                                @if($level->flag_active == 1) checked @endif><label for="f1">Active</label>
                                            <a>&nbsp;&nbsp;</a>
                                            <input type="radio" value="0" id="f2" name="flag_active" required
                                                @if($level->flag_active == 0) checked @endif><label for="f2">Non Active</label>
                                        </td>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Start Date') }}<span style="color:red">*</span></label>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input type="date" max="9999-12-31" id="sd1" onchange="setEndDate()" name="start_date" class="form-control"
                                            value="{{ $level->start_date }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input type="date" max="9999-12-31" id="ed1" disabled name="end_date" class="form-control"
                                            value="{{ $level->end_date }}" required>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection