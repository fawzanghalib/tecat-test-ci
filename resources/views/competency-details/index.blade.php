@extends('layouts.home')
@section('head')
    <style>
        .multiple-select-filter>span{
            width: 100% !important;
        }
    </style>
@endsection
@section('title-tab')
    Matrix Competency Dashboard 
@endsection
@section('title-content')
    Matrix Competency Dashboard
@endsection
@section('breadcrumb')
@endsection
@section('button')
<button type="button" class="btn btn-info btn-md pull-right" data-toggle="modal" data-target=".filter-modal">
    <i class="fas fa-filter"></i>
    filter
</button>
@endsection
@section('main-content')
    {{-- fillter modal --}}
    <div class="modal fade filter-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('competency-detail')}}" method="GET" style="margin-top:-30px">
                        <label class="mt-4">Job</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter" >
                                <select name="job_id[]" class="custom-select select-job-filter" multiple="multiple">                                
                                    @foreach ($jobs as $job)
                                        <option value="{{$job->job_id}}"
                                        @if ($request->get('job_id') !== null)    
                                            @if (in_array($job->job_id,$request->get('job_id'), FALSE))
                                                selected
                                            @endif
                                        @endif
                                        > {{$job->job_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Competency</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="competency_id[]" class="form-control custom-select select-competency-filter" multiple="multiple">                                
                                    @foreach ($competencies as $competency)
                                        <option value="{{$competency->competency_id}}"
                                            @if ($request->get('competency_id') !== null)
                                                @if (in_array($competency->competency_id,$request->get('competency_id'), FALSE))
                                                    selected
                                                @endif  
                                            @endif  
                                        > {{$competency->competency_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Level</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="level_id[]" class="custom-select select-level-filter" multiple="multiple">                                
                                    @foreach ($levels as $level)
                                        <option value="{{$level->level_id}}"
                                            @if ($request->get('level_id') !== null)
                                                @if (in_array($level->level_id, $request->get('level_id'), FALSE))
                                                    selected
                                                @endif
                                            @endif
                                        > level {{$level->level_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Date</label>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                            </div>
                            <div class="col-md-1 pl-0 pr-0">
                                <center>~</center>
                            </div>
                            <div class="col-md-3 pl-0">
                                <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}" class="form-control">
                            </div>
                        </div>

                        <label class="mt-4">Active</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="flag_active" class="custom-select">
                                    <option value="">-</option>
                                    <option value="1" @if ($request->get('flag_active') == "1") selected @endif>Active</option>
                                    <option value="0" @if ($request->get('flag_active') == "0") selected @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('competency-detail')}}">
                                    <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

        <div class="card">
            <div class="card-block">
                <div class="table-responsive" style="overflow-x:auto;">
                    <table id="" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm text-center" width="15%">Job Name</th>
                                <th class="th-sm text-center">Function</th>
                                <th class="th-sm text-center">Competency</th>               
                                <th class="th-sm text-center" width="1%">Level</th>
                                <th class="th-sm text-center">Start Date</th>
                                <th class="th-sm text-center">End Date</th>
                                <th class="th-sm text-center">Status</th>
                                <th class="th-sm text-center" width="13%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($competencyDetails as $competencyDetail)
                                <tr>
                                    <td>{{$competencyDetail->job_name}}</td>
                                    <td>{{$competencyDetail->function_name}}</td>
                                    <td>{{$competencyDetail->competency_name}}</td>
                                    <td class="text-center">{{$competencyDetail->level_name}}</td>
                                    <td class="text-center">{{$competencyDetail->start_date}}</td>
                                    <td class="text-center">{{$competencyDetail->end_date}}</td>
                                    <td class="text-center">
                                        @if ($competencyDetail->flag_active == 1)
                                            Active
                                        @else
                                            No
                                        @endif
                                    </td>
                                    <td>
                                        <a  class="float-left ml-2" href="{{route('competency-detail.edit',['job_id' => $competencyDetail->job_id, 'competency_id' => $competencyDetail->competency_id,'level_id' => $competencyDetail->level_id])}}">
                                            <button class="btn btn-sm btn-success">
                                                <i>edit</i>
                                            </button>
                                        </a>
                                        {{-- <form class="float-left ml-2 delete" action="{{route('competency-detail.delete',['job_id' => $competencyDetail->job_id, 'competency_id' => $competencyDetail->competency_id,'level_id' => $competencyDetail->level_id])}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i>disable</i>
                                            </button>
                                        </form> --}}
                                        @if ($competencyDetail->flag_active )
                                            <form class="float-left ml-2 delete" action="{{route('competency-detail.unactivated',['job_id' => $competencyDetail->job_id, 'competency_id' => $competencyDetail->competency_id,'level_id' => $competencyDetail->level_id])}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('put') }}
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i>disable</i>        
                                                </button>
                                            </form>
                                        @else
                                            <form class="float-left ml-2 delete" action="{{route('competency-detail.activate',['job_id' => $competencyDetail->job_id, 'competency_id' => $competencyDetail->competency_id,'level_id' => $competencyDetail->level_id])}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('put') }}
                                                <button type="submit" class="btn btn-sm btn-primary">
                                                    <i>enable</i>        
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            {{-- @foreach ($jobs as $job)
                                @if (count($job['Competency_detail'])!=0)
                                    <tr>
                                        <td rowspan="{{count($job['Competency_detail'])}}">{{$job['Name']}}</td>
                                        @for ($i = 0; $i < count($job['Competency_detail']); $i++)
                                            @if ($i!=0)
                                                <tr>
                                            @endif
                                                <td>{{$job['Competency_detail'][$i]->competency_name}}</td>
                                                <td>{{$job['Competency_detail'][$i]->level_name}}</td>
                                                <td>
                                                    @if ($job['Competency_detail'][$i]->flag_active == 1)
                                                        Active
                                                    @else
                                                        No
                                                    @endif
                                                </td>
                                                <td>
                                                    <a  class="float-left ml-2" href="{{route('competency-detail.edit',['job_id' => $job['JobId'], 'competency_id' => $job['Competency_detail'][$i]->competency_id,'level_id' => $job['Competency_detail'][$i]->level_id])}}">
                                                        <button class="btn btn-sm btn-success">
                                                            edit
                                                        </button>
                                                    </a>
                                                    <form class="float-left ml-2 delete" action="{{route('competency-detail.delete',['job_id' => $job['JobId'], 'competency_id' => $job['Competency_detail'][$i]->competency_id,'level_id' => $job['Competency_detail'][$i]->level_id])}}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button type="submit" class="btn btn-sm btn-danger">
                                                            Delete
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endfor
                                    @endif
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
        $(document).ready(function(){
            $(".select-competency-filter, .select-level-filter , .select-job-filter").select2();
        })
    </script>
@endsection
