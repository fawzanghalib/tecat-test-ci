@extends('layouts.home')
@section('title-tab')
    Edit Competency Required | {{$job->job_name}}
@endsection
@section('title-content')
    Edit Competency Required
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('competency-detail')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Edit Competency Required</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
                <form class="needs-validation" action="{{ route('competency-detail.update') }}" method="POST">
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="mb-3 col-md-6">
                            <label>Job<span style="color:red">*</span></label>
                            <select name="job_id" class="form-control custom-select" disabled >
                                <option  value="{{ $job->job_id }}" selected>{{$job->job_name}}</option>
                            </select>
                            <input type="hidden"  name="job_id" value="{{ $job->job_id }}" >
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Competency<span style="color:red">*</span></label>
                            <select name="competency_id" class="form-control custom-select" disabled >
                                <option  value="{{ $competency->competency_id }}" selected>{{$competency->competency_name}}</option>
                            </select>
                            <input type="hidden"  name="competency_id" value="{{ $competency->competency_id }}" >
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Level<span style="color:red">*</span></label>
                            <select name="level_id" class="form-control custom-select" disabled >
                                <option  value="{{ $level->level_id }}" selected>{{$level->level_name}}</option>
                            </select>
                            <input type="hidden"  name="level_id" value="{{ $level->level_id }}" >
                        </div>
                        <div class="mb-3 col-md-12">
                            <label>Description<span style="color:red">*</span></label>
                            <textarea name="description" class="form-control" id="" rows="3" >{{  old('description') ?? $competency_detail->description}}</textarea>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Flag Active :<span style="color:red">*</span></label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" type="radio" required id="f1" value="1" @if($competency_detail->flag_active
                            == 1) checked @endif><label for="f1">&nbsp;Active</label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" type="radio" id="f2" value="0" @if($competency_detail->flag_active
                            == 0) checked @endif><label for="f2">&nbsp;non Active</label>
                        </div>
                        <div class="row ml-1 mb-3">
                            <div class="col-md-3 mb-3">
                                <label>Start Date<span style="color:red">*</span></label>
                                <input name="start_date" max="9999-12-31" type="date" class="form-control"  required value="{{ old('start_date') ?? $competency_detail->start_date }}" min="1">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>End Date</label>
                                <input name="end_date" max="9999-12-31" type="date" class="form-control" value="{{ old('end_date') ?? $competency_detail->end_date }}">
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
                </form>
        </div>
    </div>
@endsection
