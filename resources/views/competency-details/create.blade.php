@extends('layouts.home')
@section('title-tab')
    Competency Required
@endsection
@section('title-content')
    Competency Required
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('competency-detail')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Competency Required</li>
</ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="card card-shadow col-md-12">
                <form class="needs-validation" action="{{ route('competency-detail.store') }}" method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="mb-3 col-md-6">
                            <label>Job Name<span style="color:red">*</span></label>
                            <select name="job_id" required class="form-control custom-select job-select2" id="">
                                <option value="">-</option>
                                @foreach ($functions as $function)
                                <optgroup label="{{$function->function_name}}">                                    
                                    @foreach($function->mstJobs as $job)
                                        <option value="{{ $job->job_id }}"
                                            @if (old('job_id') == $job->job_id)
                                            selected="selected"
                                            @endif
                                        >{{$job->job_name}}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Competency<span style="color:red;">*</span></label>
                            <select name="competency_id" required class="form-control custom-select ">
                                <option> - </option>
                                @foreach($competencies as $competency)
                                    <option value="{{ $competency->competency_id }}"
                                    @if ($competency->competency_id == old('competency_id'))
                                        selected="selected"
                                    @endif
                                        >{{$competency->competency_name}} ({{$competency->description}})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Level<span style="color:red;">*</span></label>
                            <select name="level_id" required class="form-control custom-select ">
                                <option> - </option>
                                @foreach($levels as $level)
                                    <option value="{{ $level->level_id }}"
                                    @if ($level->level_id == old('level_id'))
                                        selected="selected"
                                    @endif
                                        >{{$level->level_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-12">
                            <label>Description<span style="color:red;">*</span></label>
                            <textarea name="description" class="form-control" required id="" rows="3" maxlength="250" minlength="6" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"> {{  old('description') ?? ''}}</textarea>
                        </div>
                        <div class="row ml-1">
                            <div class="col-md-3 mb-3">
                                <label>Start Date<span style="color:red;">*</span></label>
                                <input name="start_date" max="9999-12-31" type="date" class="form-control"  required value="{{ old('start_date') ?? null }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>End Date</label>
                                <input name="end_date" max="9999-12-31" type="date" class="form-control" value="{{ old('end_date') ?? null }}">
                            </div>
                        </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Add Competency Required</button>
                </form>
            
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.job-select2').select2();
        });
    </script>
@endsection