@extends('layouts.home')
@section('title-tab')
    Master Job 
@endsection
@section('title-content')
    Master Job
@endsection
@section('button')
    <a href="{{route('job.create')}}">
        <button class="btn btn-md btn-warning pull-right">
            <i class="fas fa-plus-circle"></i>
            Add Job
        </button>
    </a>
    <a href="{{route('job.refresh')}}">
        <button class="button-refresh-job btn btn-md btn-info pull-right mr-2">
            <i class="fas fa-sync"></i>
            Refresh   
        </button>
    </a>
@endsection
@section('main-content')
    {{-- fillter modal --}}
    <div class="modal fade filter-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('job')}}" method="GET" autocomplete="off">
                        <label>Search</label>
                        <div class="row">
                            <div class="col-md-9 pr-0">
                                <input name="search" type="text" class="form-control" placeholder="Search by Job Id / Job Name / Description.." value="{{$request->get('search')}}">
                            </div>
                        </div>
                        <br/>
                        <label>Date</label>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                            </div>
                            <div class="col-md-1 pl-0 pr-0">
                                <center>~</center>
                            </div>
                            <div class="col-md-3 pl-0">
                                <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}" class="form-control">
                            </div>
                        </div>

                        <label class="mt-4">Active</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="flag_active" class="custom-select">
                                    <option value="">-</option>
                                    <option value="1" @if ($request->get('flag_active') == "1") selected @endif>Active</option>
                                    <option value="0" @if ($request->get('flag_active') == "0") selected @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('job')}}">
                                    <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-info btn-md float-right mb-3" data-toggle="modal" data-target=".filter-modal">
                    <i class="fas fa-filter"></i>
                    filter
                </button>
            </div>
        </div>
        
        <div class="card">
            <div class="card-block">
                <div class="table-responsive" style="overflow-x:auto;">
                    <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm text-center" width="1%">No.</th>
                                <th class="th-sm text-center" width="10%">Job Id</th>
                                <th class="th-sm text-center" width="10%">@sortablelink('job_name','Job Name')</th>
                                <th class="th-sm text-center" width="10%">Function</th>
                                <th class="th-sm text-center" width="10%">Description</th>
                                <th class="th-sm text-center">@sortablelink('start_date','Start Date')</th>
                                <th class="th-sm text-center">@sortablelink('end_date','End Date')</th>
                                <th class="th-sm text-center">Status</th>
                                <th class="th-sm text-center" width="12%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($jobs->count())
                            @php $i = 0; @endphp
                                @foreach($jobs as $job)
                                    <tr>
                                        <td class="text-center">{{ ++$i }}</td>
                                        <td>{{ $job->job_id }}</td>
                                        <td>{{ $job->job_name }}</td>
                                        <td>@php echo ucfirst($job->function_name) @endphp</td>
                                        <td>{{ $job->description }}</td>
                                        <td class="text-center">{{ $job->start_date }}</td>
                                        <td class="text-center">{{ $job->end_date }}</td>
                                        <td class="text-center">
                                            @if ($job->flag_active == 1)
                                                Active
                                            @else
                                                No
                                            @endif
                                        </td>
                                        <td>
                                            <a class="float-left ml-2" href="{{route('job.edit',['id' => $job->job_id])}}">
                                                <button class="btn btn-sm btn-success">
                                                    <i>edit</i>
                                                </button>
                                            </a>
                                            {{-- <form class="float-left ml-2 delete" action="{{route('job.delete',['id' => $job->id])}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i>disable</i>     
                                                </button>
                                            </form> --}}
                                            @if ($job->flag_active )
                                                <form class="float-left ml-2 delete" action="{{route('job.unactivated',['id' => $job->job_id])}}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('put') }}
                                                    <button type="submit" class="btn btn-sm btn-danger">
                                                        <i>disable</i>      
                                                    </button>
                                                </form>
                                            @else
                                                <form class="float-left ml-2 delete" action="{{route('job.activate',['id' => $job->job_id])}}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('put') }}
                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i>enable</i>   
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>      
        </div>
        {{-- {{$jobs->links("pagination::bootstrap-4")}} --}}

@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
        $(".button-refresh-job").on("click", function(){
            $('.button-refresh-job').html("<i class='fa fa-spinner fa-spin'></i>Process");
            $('.button-refresh-job').attr("disabled", true);
        });
    </script>
@endsection