@extends('layouts.home')
@section('title-tab')
    Edit Job | {{$job->job_name}}
@endsection
@section('title-content')
    Edit Job
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('job')}}">Master Job</a></li>
    <li class="breadcrumb-item active">Edit Job</li>
</ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <form class="needs-validation" action="{{ route('job.update', ['id' => $job->id]) }}" method="POST">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            <div class="row">
                <div class="card card-shadow col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-block">
                        <div class="mb-3 col-md-6">
                            <label>Job Name<span style="color:red">*</span> </label>
                            <input name="job_name" maxlength="50" minlength="4" type="text" class="form-control" value="{{  old('job_name') ?? $job->job_name}}" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Description<span style="color:red">*</span></label>
                            <textarea name="description" class="form-control" id="" rows="3" maxlength="250" minlength="6" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>{{  old('description') ?? $job->description}}</textarea>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Function<span style="color:red;">*</span></label>
                            <select name="function_id" class="form-control custom-select" required>
                                <option value> - </option>
                                @foreach($functions as $function)
                                    <option value="{{ $function->function_id }}"
                                    @if ($function->function_id == (old('function_id') ?? $job->function_id))
                                        selected="selected"
                                    @endif
                                        >{{$function->function_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Flag Active :<span style="color:red">*</span></label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f1" type="radio" value="1" @if($job->flag_active == 1) checked @endif><label for="f1">&nbsp;Active</label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f2" type="radio" value="0" @if($job->flag_active == 0) checked @endif><label for="f2">&nbsp;non Active </label>
                        </div>
                        <div class="row ml-1">
                            <div class="col-md-3 mb-3">
                                <label>Start Date<span style="color:red">*</span></label>
                                <input name="start_date" type="date" id="sd1" onchange="setEndDate()" max="9999-12-31" class="form-control"  required value="{{ old('start_date') ?? $job->start_date }}" min="1">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>End Date</label>
                                <input name="end_date" type="date" id="ed1" max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $job->end_date }}" min="1">
                                {{-- <input name="end_date" type="date" id="ed1" disabled max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $job->end_date }}" min="1"> --}}
                            </div>
                        </div>
                    </div>
                </div> 
                <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
            </div>
            
        </form>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
        $(document).ready(function(){
            $(".select-competency-filter, .select-level-filter , .select-job-filter").select2();
        })
    </script>
@endsection