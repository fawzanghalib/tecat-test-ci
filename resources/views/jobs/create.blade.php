@extends('layouts.home')
@section('title-tab')
    Create Job 
@endsection
@section('title-content')
    Create Job
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('job')}}">Master Job</a></li>
    <li class="breadcrumb-item active">Create Job</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <form class="needs-validation" action="{{ route('job.store') }}" method="POST" autocomplete="off">
                {{ csrf_field() }}
                <div class="row mb-3">
                    <div class="card card-shadow col-md-12 mb-5">
                        @if ($errors->any())
                            <div class="alert alert-danger mt-3">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label>Function<span style="color:red;">*</span></label>
                                    <select name="function_id" class="form-control custom-select" id="" required>
                                        <option value="" selected="selected"> - </option>
                                        @foreach ($functions  as $function)    
                                            <option value="{{ $function->function_id }}">
                                                {{ $function->function_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-4 col-md-6">
                                    <label>Job Name<span style="color:red;">*</span></label>
                                    <input name="job_name" type="text" class="form-control" maxlength="50" minlength="4" required value="{{ old('job_name') ?? "" }}" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label>Job ID<span style="color:red;">*</span></label>
                                    <input name="job_id" type="text" class="form-control" maxlength="15" minlength="15" required value="{{ old('job_id') ?? "" }}" onKeyPress="return angkadanhuruf(event,'1234567890',this)"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-3 col-md-12">
                                    <label>Description<span style="color:red;">*</span></label>
                                    <textarea name="description" class="form-control" rows="3" maxlength="250" minlength="6" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>{{ old('description') ?? "" }}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label>Start Date<span style="color:red;">*</span></label>
                                    <input name="start_date" type="date" max="9999-12-31" id="sd1" onchange="setEndDate()" class="form-control"  required value="{{ old('start_date') ?? "" }}" min="1">
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label>End Date</label>
                                    <input name="end_date" type="date" id="ed1" max="9999-12-31" class="form-control" value="{{ old('end_date') ?? "" }}" min="1">
                                    {{-- <input name="end_date" type="date" id="ed1" disabled max="9999-12-31" class="form-control" value="{{ old('end_date') ?? "" }}" min="1"> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Add Job</button>
                </div>
            </form>
        </div>
    </div>
@endsection