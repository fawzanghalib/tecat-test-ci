@extends('layouts.home')
@section('title-tab')
    Edit Akun 
@endsection
@section('title-content')
    Akun
@endsection
@section('main-content')
    <div class="container">
        <form class="needs-validation" action="{{ route('user.update') }}" method="POST">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            <div class="row mb-5">
                <div class="card card-shadow col-md-12 mb-5">
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="mb-3 col-md-6">
                            <label>Nama</label>
                            <input name="name" type="text" class="form-control" id="" placeholder="" value="{{ old('name') ?? $user['name'] }}" required="">
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Phone number</label>
                            <input name="phone_number" type="text" class="form-control" id="" placeholder="" value="{{ old('phone_number') ?? $user['phone_number'] }}" required="">
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Update</button>
        </form>
@endsection