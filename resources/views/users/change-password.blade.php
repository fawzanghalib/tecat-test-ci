@extends('layouts.home')
@section('title-tab')
    Change Password
@endsection
@section('title-content')
    Change Password
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <form class="needs-validation" action="{{ route('user.update.password') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="card card-shadow col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body"><label>Current Password<span style="color:red"> *</span></label>
                        <div class="mb-3 col-md-6 input-group">
                            <input name="current_password" type="password" minlength="8" maxlength="14" class="form-control" id="input-current-password"  placeholder="" required="">
                            <div class="input-group-append">
                              <a href="#" onclick="intipOldPassword()"><button class="btn btn-md" type="button"><i class="fas fa-eye"></i></button></a>
                            </div>
                        </div>
                        <label>New Password<span style="color:red"> *</span></label>
                        <label class="label label-primary">Password harus mengandung huruf, angka, spesial karakter, dan capital.</label>
                        <div class="mb-3 col-md-6 input-group">
                            <input name="new_password" type="password" minlength="8" maxlength="14" class="form-control" id="input-password" placeholder="" required="">
                            <div class="input-group-append">
                              <a href="#" onclick="intipNewPassword()"><button class="btn btn-md" type="button"><i class="fas fa-eye"></i></button></a>
                            </div>
                        </div>
                        <label>Re-type New Password<span style="color:red"> *</span></label>
                        <div class="mb-3 col-md-6 input-group">  
                            <input name="new_confirm_password" type="password" minlength="8" maxlength="14" class="form-control" id="repw" placeholder="" required="">
                            <div class="input-group-append">
                              <a href="#" onclick="intipRePassword()"><button class="btn btn-md" type="button"><i class="fas fa-eye"></i></button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-warning btn-lg btn-block" type="submit">Save Changes</button>
            </div>   
        </form>
  </div>

  
@endsection

<script>
  function intipOldPassword() {
    var x = document.getElementById("input-current-password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  function intipNewPassword() {
    var x = document.getElementById("input-password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  function intipRePassword() {
    var x = document.getElementById("repw");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>