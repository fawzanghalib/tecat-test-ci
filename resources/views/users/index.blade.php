@extends('layouts.home')
@section('title-tab')
    Master User 
@endsection
@section('title-content')
    Master User
@endsection
@section('button')
    <a href="{{route("user.create")}}">
        <button class="btn btn-md btn-warning pull-right">
            <i class="fas fa-plus-circle"></i>
            Add User
        </button>
    </a>
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <div class="table-responsive" style="overflow-x:auto;">
            <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm text-center" width="5%">NPK</th>
                        <th class="th-sm text-center" width="15%">Nama Lengkap</th>
                        <th class="th-sm text-center" width="15%">Email</th>
                        <th class="th-sm text-center" width="5%">Status</th>
                        <th class="th-sm text-center" width="3%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($users->count())
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center">{{ $user->npk }}</td>   
                                <td class="text-center">@php echo ucfirst($user->name) @endphp</td>
                                <td class="text-center">{{ $user->email }}</td>
                                <td class="text-center">
                                    @if ($user->flag_active )
                                        <label class="label label-success">Active</label>
                                    @else
                                    <label class="label label-danger">No</label>
                                    @endif
                                </td>
                                <td class="text-center">
                                @if ($user->id == auth()->user()->id || $user->id == 1)
                                -
                                @else
                                    @if ($user->flag_active )
                                        <form class="text-center ml-2 delete" action="{{route('user.unactivated',['id' => $user->id])}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('put') }}
                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i>unactivated</i>
                                            </button>
                                        </form>
                                    @else
                                        <form class="text-center ml-2 delete" action="{{route('user.activate',['id' => $user->id])}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('put') }}
                                            <button type="submit" class="btn btn-sm btn-primary">
                                                <i>activate</i>        
                                            </button>
                                        </form>
                                    @endif

                                @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
    </script>
@endsection