@extends('layouts.home')
@section('title-tab')
    Reports
@endsection
@section('title-content')
    Reports
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <form action="{{ route('reports')}}" method="GET" style="margin-top:-50px">
            <div class="row">
                <div class="col-md-6">
                    <label class="mt-5">Function</label>
                    <div class="row">
                        <div class="col-md-9 pr-0 multiple-select-filter"  >
                            <select name="function_id[]" class="form-control custom-select select-function-filter" multiple="multiple">                                
                                @foreach ($functions as $function)
                                    <option value="{{$function->function_id}}"
                                    @if ($request->get('function_id') !== null)    
                                        @if (in_array($function->function_id,$request->get('function_id'), FALSE))
                                            selected
                                        @endif
                                    @endif
                                    > {{$function->function_name}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="mt-5">Job</label>
                    <div class="row">
                        <div class="col-md-9 pr-0 multiple-select-filter"  >
                            <select name="job_id[]" class="form-control custom-select select-job-filter" multiple="multiple">                                
                                @foreach ($jobs as $job)
                                    <option value="{{$job->job_id}}"
                                    @if ($request->get('job_id') !== null)    
                                        @if (in_array($job->job_id,$request->get('job_id'), FALSE))
                                            selected
                                        @endif
                                    @endif
                                    > {{$job->job_name}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="mt-4">Competency</label>
                    <div class="row">
                        <div class="col-md-9 pr-0 multiple-select-filter"  >
                            <select name="competency_id[]" class="form-control custom-select select-competency-filter" multiple="multiple">                                
                                @foreach ($competencies as $competency)
                                    <option value="{{$competency->competency_id}}"
                                        @if ($request->get('competency_id') !== null)
                                            @if (in_array($competency->competency_id,$request->get('competency_id'), FALSE))
                                                selected
                                            @endif  
                                        @endif  
                                    > {{$competency->competency_name}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="mt-4">Level</label>
                    <div class="row">
                        <div class="col-md-9 pr-0 multiple-select-filter"  >
                            <select name="level_id[]" class="form-control custom-select select-level-filter" multiple="multiple">                                
                                @foreach ($levels as $level)
                                    <option value="{{$level->level_id}}"
                                        @if ($request->get('level_id') !== null)
                                            @if (in_array($level->level_id, $request->get('level_id'), FALSE))
                                                selected
                                            @endif
                                        @endif
                                    > level {{$level->level_name}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="mt-4">Date</label>
                    <div class="row">
                        <div class="col-md-5 pr-0">
                            <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                        </div>
                        <div class="col-md-1 pl-0 pr-0">
                            <center>~</center>
                        </div>
                        <div class="col-md-5 pl-0">
                            <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row mt-5 col-md-6">
                    <div class="col-md-4">
                        <button class="btn btn-warning btn-lg btn-block" name="action" type="submit" value="filter">Filter</button>
                    </div>
                    <div class="col-md-4">
                        <a href="{{route('reports')}}">
                            <button class="btn btn-outline-warning btn-lg btn-block" type="button">Reset</button>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-warning btn-lg btn-block" name="action" type="submit" value="download">Download</button>
                    </div>
                </div>
            </div>
        </form>

        <table class="table table-striped table-bordered table-sm mt-5 table-striped" width="100%" ellspacing="0" width="100%" >
            <thead>
                <tr>
                    <th class="th-sm text-center">NPK</th>
                    <th class="th-sm text-center">Function Name</th>
                    <th class="th-sm text-center">Job Name</th>
                    <th class="th-sm text-center" width="8%">Competency</th>
                    <th class="th-sm text-center">Level</th>
                    <th class="th-sm text-center" width="5%">Question code</th>
                    <th class="th-sm text-center">Question</th>
                    <th class="th-sm text-center">Answer</th>
                    <th class="th-sm text-center">Score</th>
                    <th class="th-sm text-center" width="13%">Question Date</th>
                    <th class="th-sm text-center" width="13%">Answer Date</th>
                    {{-- <th class="th-sm">Timer Answer</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach ($reports as $report)
                    <tr>
                        <td>{{$report->user_id}}</td>
                        <td>{{$report->function_name}}</td>
                        <td>{{$report->job_name}}</td>
                        <td>{{$report->competency_name}}</td>
                        <td class="th-sm text-center">{{$report->level_name}}</td>
                        <td class="th-sm text-center">{{$report->question_id}}</td>
                        <td>@php echo ucfirst($report->question) @endphp</td>
                        <td>@php echo ucfirst($report->answer) @endphp</td>
                        <td>{{$report->sum_score}}</td>
                        <td>{{$report->time_start}}</td>
                        <td>{{$report->time_finish}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $(".select-function-filter, .select-competency-filter, .select-level-filter , .select-job-filter").select2();
        })
    </script>
@endsection

