@extends('layouts.home')
@section('title-tab')
    Create Competency 
@endsection
@section('title-content')
    Create Competency
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('competency')}}">Master Competency</a></li>
    <li class="breadcrumb-item active">Create Competency</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <div class="card card-shadow col-md-12 mb-5">
                <form class="needs-validation" action="{{ route('competency.store') }}" method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="mb-3 col-md-6">
                            <label>Competency Name<span style="color:red">*</span></label>
                            <input name="competency_name" type="text" class="form-control" maxlength="50" minlength="2" required value="{{ old('competency_name') ?? "" }}" autocomplete="off" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                        </div>
                        <div class="mb-3 col-md-12">
                            <label>Description<span style="color:red">*</span></label>
                            <textarea name="description" class="form-control" rows="3" maxlength="250" minlength="6" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>{{ old('description') ?? "" }}</textarea>
                        </div>
                        <div class="row ml-1">
                            <div class="col-md-3 mb-3">
                                <label>Start Date<span style="color:red">*</span></label>
                                <input name="start_date" id="sd1" onchange="setEndDate()" max="9999-12-31" type="date" class="form-control" required value="{{ old('start_date') ?? "" }}" min="1">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>End Date</label>
                                <input name="end_date" id="ed1" max="9999-12-31" type="date" class="form-control"  value="{{ old('end_date') ?? "" }}">
                                {{-- <input name="end_date" id="ed1" disabled max="9999-12-31" type="date" class="form-control"  value="{{ old('end_date') ?? "" }}"> --}}
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <button class="btn btn-warning btn-block btn-lg" type="submit">Add Competency</button>
                </form>
            
        </div>
    </div>
@endsection