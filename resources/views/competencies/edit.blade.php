@extends('layouts.home')
@section('title-tab')
    Edit Competency | {{$competency->competency_name}}
@endsection
@section('title-content')
    Edit Competency
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('competency')}}">Master Competency</a></li>
    <li class="breadcrumb-item active">Edit Competency</li>
</ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <form class="needs-validation" action="{{ route('competency.update', ['id' => $competency->competency_id]) }}"
            method="POST" autocomplete="off">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            @if ($errors->any())
            <div class="alert alert-danger mt-3">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                <div class="card card-shadow col-md-12">
                    <div class="card-body">
                        <div class="mb-3 col-md-6">
                            <label>Competency Name<span style="color:red">*</span> </label>
                            <input name="competency_name" type="text" class="form-control" id="" placeholder=""
                                value="{{  old('competency_name') ?? $competency->competency_name}}" maxlength="50" minlength="2" required="" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                        </div>
                        <div class="mb-3 col-md-6">
                            <label>Description<span style="color:red">*</span></label>
                            <textarea name="description" class="form-control" id=""
                                rows="3" maxlength="250" minlength="6" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)">{{  old('description') ?? $competency->description}}</textarea>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label>Flag Active :<span style="color:red">*</span></label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f1" type="radio" value="1" @if($competency->flag_active
                            == 1) checked @endif><label for="f1">&nbsp;Active</label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f2" type="radio" value="0" @if($competency->flag_active
                            == 0) checked @endif><label for="f2">&nbsp;non Active</label>
                        </div>
                        <div class="row ml-1">
                            <div class="col-md-3 mb-3">
                                <label>Start Date<span style="color:red">*</span></label>
                                <input name="start_date" max="9999-12-31" id="sd1" onchange="setEndDate()" type="date" class="form-control" required value="{{ old('start_date') ?? $competency->start_date }}" min="1">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>End Date</label>
                                <input name="end_date" max="9999-12-31" id="ed1" type="date" class="form-control" value="{{ old('end_date') ?? $competency->end_date }}">
                                {{-- <input name="end_date" max="9999-12-31" id="ed1" disabled type="date" class="form-control" value="{{ old('end_date') ?? $competency->end_date }}"> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Update</button>
            </div>
            
        </form>
    </div>
</div>
@endsection