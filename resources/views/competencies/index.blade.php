@extends('layouts.home')
@section('title-tab')
Competency
@endsection
@section('title-content')
Master Competency
@endsection
@section('button')
    {{-- <a class="ml-2" href="{{route('competency.download')}}" onclick="window.open( this.href ,'_blank');return false;">
        <button class="btn btn-sm btn-outline-success pull-right">
            <i class="fas fa-file-download"></i>
            Download   
        </button>
    </a> --}}
    <a class="ml-2" href="{{route("competency.create")}}">
        <button class="btn btn-md btn-warning pull-right">
            <i class="fas fa-plus-circle"></i>
            Add Competency
        </button>
    </a>
@endsection

@section('main-content')
{{-- filter modal --}}
<div class="modal fade filter-competency" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('competency')}}" method="GET" autocomplete="off">
                    <label>Search</label>
                    <div class="row">
                        <div class="col-md-9 pr-0">
                            <input name="search" type="text" class="form-control" placeholder="Search by Competency Name / Description.." value="{{$request->get('search')}}">
                        </div>
                    </div>

                    <label class="mt-4">Date</label>
                    <div class="row">
                        <div class="col-md-3 pr-0">
                            <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                        </div>
                        <div class="col-md-1 pl-0 pr-0">
                            <center>~</center>
                        </div>
                        <div class="col-md-3 pl-0">
                            <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}"
                                class="form-control">
                        </div>
                    </div>

                    <label class="mt-4">Active</label>
                    <div class="row">
                        <div class="col-md-4">
                            <select name="flag_active" class="custom-select">
                                <option value="">-</option>
                                <option value="1" @if ($request->get('flag_active') == "1") selected @endif>Active
                                </option>
                                <option value="0" @if ($request->get('flag_active') == "0") selected @endif>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                        </div>
                        <div class="col-md-6">
                            <a href="{{route('competency')}}">
                                <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-info btn-md float-right mb-3" data-toggle="modal"
                data-target=".filter-competency">
                <i class="fas fa-filter"></i>
                filter
            </button>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="table-responsive" style="overflow-x:auto;">
                <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm text-center">@sortablelink('competency_name','Competency Name')</th>
                            <th class="th-sm text-center">Description</th>
                            <th class="th-sm text-center">@sortablelink('start_date','Start Date')</th>
                            <th class="th-sm text-center">@sortablelink('end_date','End Date')</th>
                            <th class="th-sm text-center">Status</th>
                            <th class="th-sm text-center" width="13%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($competencies->count())
                        @foreach($competencies as $competency)
                        <tr>
                            <td>{{ $competency->competency_name }}</td>
                            <td>{{ $competency->description }}</td>
                            <td class="text-center">{{ $competency->start_date }}</td>
                            <td class="text-center">{{ $competency->end_date }}</td>
                            <td class="text-center">
                                @if ($competency->flag_active == 1)
                                Active
                                @else
                                No
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="float-left ml-2" href="{{route('competency.edit',['id' => $competency->competency_id])}}">
                                    <button class="btn btn-sm btn-success">
                                        <i>edit</i>
                                    </button>
                                </a>
                                {{-- <form class="float-left ml-2 delete"
                                    action="{{route('competency.delete',['id' => $competency->competency_id])}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-sm btn-danger">
                                        <i>disable</i>
                                    </button>
                                </form> --}}
                                @if ($competency->flag_active )
                                    <form class="float-left ml-2 delete" action="{{route('competency.unactivated',['id' => $competency->competency_id])}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('put') }}
                                        <button type="submit" class="btn btn-sm btn-danger">
                                            <i>disable</i>        
                                        </button>
                                    </form>
                                @else
                                    <form class="float-left ml-2 delete" action="{{route('competency.activate',['id' => $competency->competency_id])}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('put') }}
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            <i>enable</i>        
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(".delete").on("submit", function () {
        return confirm("Are you sure?");
    });
</script>
@endsection