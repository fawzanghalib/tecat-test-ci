@extends('layouts.home')
@section('title-tab')
    Edit Notification | {{$data->notification_name}}
@endsection
@section('title-content')
    Edit Notification
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('customize-notification.index')}}">Master Notification</a></li>
    <li class="breadcrumb-item active">Edit Notification</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    <form action="{{ route('updateNotification.update',  ['notification_id' => $data->notification_id]) }}" autocomplete="off" method="POST">
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Type') }}</label>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input readonly class="form-control" value="{{ $data->type }}"> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <input type="text" readonly value="{{ $data->notification_name }}" class="form-control" maxlength="150" minlength="6" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Template') }}</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <textarea required name="template" class="form-control textarea" id="" rows="3" maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data->template }}</textarea>
                            </div>
                        </div>
                    </div>
                    @if($data->default == 0)
                    {{-- 
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Flag') }}</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                            <td>
                                <input id="act" type="radio" name="flag_active" value="1" @if((old('flag_active') ?? $data->flag_active) == 1) checked @endif /> 
                                <label for="act">Active</label>
                                <a>&nbsp;&nbsp;</a>
                                <input id="non" type="radio" name="flag_active" value="0" @if((old('flag_active') ?? $data->flag_active) == 0) checked @endif /> 
                                <label for="non">Non Active</label>
                            </td> 
                            </div>
                        </div>
                    </div>
                    --}}
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Start Date') }}</label>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <input type="date" max="9999-12-31" name="start_date" class="form-control" value="{{ $data->start_date }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <input type="date" max="9999-12-31" name="end_date" class="form-control" value="{{ $data->end_date }}">
                            </div>
                        </div>
                    </div>
                    @endif
                        <br/>
                        <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
    .textarea{
        max-height: 100px;
        min-height: 50px;
    }
    </style>
@endsection
