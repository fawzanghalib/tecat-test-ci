@extends('layouts.home')
@section('title-tab')
    Master Notification
@endsection
@section('title-content')
<a href="{{ route('customize-notification.index') }}" style="color:#212529">
    Master Notification
</a>
@endsection
@section('button')
    <a href="{{ route('customize-notification.create') }}">
        <button class="btn btn-md btn-warning pull-right">
            <i class="fas fa-plus-circle"></i>
            Add Notification
        </button>
    </a>
    <a href="{{ route('notification-log.index') }}">
        <button class="btn btn-smd btn-success pull-right mr-2">  
            Log Notification
        </button>
    </a>
@endsection
@section('main-content')
{{-- filter modal --}}
<div class="modal fade filter-notification" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width:650px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('customize-notification.index')}}" method="GET" autocomplete="off">
                    @foreach ($request->query as $key => $value)
                    @if ($key != 'end_date' && $key != 'start_date' && $key != 'flag_active')
                    <input type="hidden" name="{{$key}}" value="{{$value}}">
                    @endif
                    @endforeach
                    <label>Date</label>
                    <div class="row">
                        <div class="col-md-4 pr-0">
                            <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}"
                                class="form-control">
                        </div>
                        <div class="col-md-1 pl-0 pr-0">
                            <center>~</center>
                        </div>
                        <div class="col-md-4 pl-0">
                            <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}"
                                class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-0">
                            <label class="mt-4">Active</label>
                            <select name="flag_active" class="form-control custom-select">
                                <option value="">-</option>
                                <option value="1" @if ($request->get('flag_active') == "1") selected @endif>Active
                                </option>
                                <option value="0" @if ($request->get('flag_active') == "0") selected @endif>No</option>
                            </select>
                        </div>
                        <div class="col-md-4 pl-4">
                            <label class="mt-4">Type</label>
                            <select name="type" class="form-control custom-select">
                                <option value="">-</option>
                                <option value="Daily" @if ($request->get('type') == "Daily") selected @endif>
                                    Daily
                                </option>
                                <option value="Feedback" @if ($request->get('type') == "Feedback") selected @endif>
                                    Feedback
                                </option>
                                <option value="Loyalty" @if ($request->get('type') == "Loyalty") selected @endif>
                                    Loyalty
                                </option>
                                <option value="Broadcast" @if ($request->get('type') == "Broadcast") selected @endif>
                                    Broadcast
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                        </div>
                        <div class="col-md-6">
                            @php
                            $tamp ="";
                            if($request->get('search')){
                            $tamp = "?search=".$request->get('search');
                            }
                            @endphp
                            <a href="{{route('customize-notification.index').$tamp}}">
                                <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <div class="card">
        <div class="card-block">
            <div class="row" >
                <div class="col-md-11">
                    {{-- search form --}}
                    <form action="{{route('customize-notification.index')}}" method="GET" autocomplete="off">
                        <div class="input-group mb-3 ">
                            @foreach ($request->query as $key => $value)
                            @if ($key != 'search')
                            <input type="hidden" name="{{$key}}" value="{{$value}}">
                            @endif
                            @endforeach
                            <input name="search" type="text" class="form-control" placeholder="Search by notification name.."
                                value="{{$request->get('search')}}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i
                                        class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-info btn-md float-right mb-5" data-toggle="modal"
                        data-target=".filter-notification">
                        <i class="fas fa-filter"></i>
                        filter
                    </button>
                </div>

            </div>
            @if (session('status'))
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-success" style="margin-bottom:40px; margin-top:-20px">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-times"></i>
                        </button>
                        <span>{{ session('status') }}</span>
                        </div>
                    </div>
                </div>
            @endif

            <div class="table-responsive" style="overflow-x:auto;">
                <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm text-center" width="20%">@sortablelink('notification_name','Notification Name')</th>
                            <th class="th-sm text-center">Type</th>
                            <th class="th-sm text-center">Template</th>
                            <th class="th-sm text-center" width="12%">@sortablelink('start_date','Start Date')</th>
                            <th class="th-sm text-center" width="12%">@sortablelink('end_date','End Date')</th>
                            <th class="th-sm text-center" width="8%"> @sortablelink('flag_active','Status')</th>
                            <th class="th-sm text-center" width="12%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($data->count())
                        @foreach($data as $datas)
                        <tr>
                            <td>{{ $datas->notification_name }}</td>
                            <td class="text-center">{{ $datas->type }}</td>
                            <td>{{ $datas->template }}</td>
                            <td class="text-center">{{ $datas->start_date }}</td>
                            <td class="text-center">{{ $datas->end_date }}</td>
                            <td class="text-center">
                            @if ($datas->flag_active == 1)
                            Active
                            @else
                            No
                            @endif
                            </td>
                            <td>
                                <a class="float-left ml-2" href="{{route('editNotification.edit',['id' => $datas->notification_id])}}">
                                    <button class="btn btn-sm btn-success">
                                        <i>edit</i>
                                    </button>
                                </a>
                                @if ($datas->flag_active == 1 && $datas->default == 0)
                                <form class="float-left ml-2 delete"
                                    action="{{route('destroyNotification.delete',['id' => $datas->notification_id])}}" method="POST" autocomplete="off">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-sm btn-danger" >
                                        <i>disable</i>
                                    </button>
                                </form>
                                @endif
                                @if ($datas->flag_active == 0 && $datas->default == 0)
                                <form class="float-left ml-2 delete"
                                    action="{{route('restoreNotification.restore',['id' => $datas->notification_id])}}" method="POST" autocomplete="off">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-sm btn-primary" >
                                        <i>enable</i>
                                    </button>
                                </form>
                                @endif
                                
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
                {!! $data->render() !!}
        </div>
    </div>
@section('script')
<script>
    $(".delete").on("submit", function () {
        return confirm("Are you sure?");
    });
</script>
@endsection

@endsection