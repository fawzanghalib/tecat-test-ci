@extends('layouts.home')
@section('title-tab')
    Notifications Log
@endsection
@section('title-content')
    Notification Log
@endsection
@section('button')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('customize-notification.index')}}">Master Notification</a></li>
    <li class="breadcrumb-item active">Notification Log</li>
</ol>
@endsection
@section('main-content')

<div class="card">
    <div class="card-block">
        <div class="row" >
            <div class="col-md-12">
                {{-- search form --}}
                <form action="{{route('notification-log.index')}}" method="GET" autocomplete="off">
                    <div class="input-group mb-3 ">
                        @foreach ($request->query as $key => $value)
                        @if ($key != 'search')
                        <input type="hidden" name="{{$key}}" value="{{$value}}">
                        @endif
                        @endforeach
                        <input name="search" type="text" class="form-control" placeholder="Search by NPK.."
                            value="{{$request->get('search')}}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i
                                    class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <span>{{ session('status') }}</span>
                    </div>
                </div>
            </div>
        @endif
        <div class="table-responsive" style="overflow-x:auto;">
            <table id="dtBasicExample" class="table table-bordered table-md" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm text-center" width="8%">NPK</th>
                        <th class="th-sm text-center">Notification Name</th></th>
                        <th class="th-sm text-center">Template</th>
                        <th class="th-sm text-center" width="5%">Keterangan</th>
                        <th class="th-sm text-center">Delivered</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data->count())
                    @foreach($data as $datas)
                    <tr>
                        <td class="text-center">{{ $datas->npk }}</td>
                        <td>{{ $datas->notification_name }}</td>
                        <td>{{ $datas->template }}</td>
                        <td class="text-center">{{ $datas->keterangan }}</td>
                        <td class="text-center">{{ $datas->created_at }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
            {!! $data->render() !!}
    </div>
</div>
@endsection