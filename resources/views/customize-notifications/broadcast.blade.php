@extends('layouts.home')
@section('title-tab')
    Customize Notification - Broadcast 
@endsection
@section('title-content')
    Customize Notification - Broadcast
@endsection
@section('main-content')
    <div class="container">
        <div class="row ">
            <div class="card card-shadow col-md-12 ">
                <div class="card-body">
                    <div class="mb-3 col-md-12 mb-5">
                        <label>Format:</label>
                        <textarea name="" class="form-control" id="" rows="3" >Kok cuma diliatin  aja notifikasi Daily Test – nya? Kamu ga pengen menumpuk pertanyaan-pertanyaannya kan? Bahaya nih kalo keterusan! Yuk jawab pertanyaannya!</textarea>
                    </div>
                    <div class="card-body row">
                        
                        <div class="mb-3 col-md-3">
                            <label>Function</label>
                            <select name="job_id" class="custom-select" id="">
                                <option value="">-</option>
                                @foreach($functions as $function)
                                    <option value="{{$function->function_id}}">{{$function->function_name}}</option>                                    
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-3">
                            <label>Job Name</label>
                            <select name="job_id" class="custom-select" id="">
                                <option value="">-</option>
                                @foreach($functions as $function)
                                    <optgroup label="{{$function->function_name}}">
                                    @foreach ($function->mstJobs as $job)
                                        <option value="{{ $job->job_id }}">{{$job->job_name}}</option>
                                    @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-3">
                            <label>Competency</label>
                            <select name="competency_id" class="custom-select" id="">
                                <option value="">-</option>
                                @foreach($competencies as $competency)
                                    <option value="">{{$competency->competency_name}}</option>                                    
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-3">
                            <label>Modul</label>
                            <select name="modul_id" class="custom-select" id="">
                                <option value="">-</option>
                                @foreach($moduls as $modul)
                                    <option value="">{{$modul->modul_name}}</option>                                    
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-3">
                            <label>Level</label>
                            <select name="level_id" class="custom-select" id="">
                                <option value="">-</option>
                                @foreach($levels as $level)
                                    <option value="">{{$level->level_name}}</option>                                    
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 col-md-3">
                            <label>Area</label>
                            <select name="area_id" class="custom-select" id="">
                                <option value="">-</option>
                            </select>
                        </div>
                        <div class="mb-3 col-md-3">
                            <label>Branch</label>
                            <select name="branch_id" class="custom-select" id="">
                                <option value="">-</option>
                            </select>
                        </div>                        
                        <div class="mb-3 col-md-3">
                            <button class="btn btn-danger btn-block float-md-right" type="reset" style="margin-top:30px">Reset Filters</button>
                        </div>                        
                    </div>
                    <div class="card-body row">
                        <div class="mb-3 col-md-6">
                        <label>Sort</label>
                        <select name="" class="custom-select" id="">
                            <option value="">-</option>
                            <option value="">All</option>
                            <option value="">Bottom 100 Participant</option>
                            <option value="">Bottom 3 Area</option>
                            <option value="">Bottom 3 Branch</option>
                            <option value="">Top 100 Participant</option>
                            <option value="">Top 3 Area</option>
                            <option value="">Top 3 Branch</option>
                            <option value="">10 Best Point</option>
                            <option value="">Input Manual</option>
                        </select>
                        </div>                        
                    </div>
                    <div class="mb-3 col-md-3">
                        <button class="btn btn-primary btn-block" type="submit" style="pull-right">Send Broadcast</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('script')
<script>
        $(document).ready(function() {
            
            $("select[name='job_id']").change(function(){
                var job_id = $(this).val();
                var token = $("input[name='_token']").val();
                $("select[name='competency_id']").html("loading...");
                $.ajax({
                    url: "{{route('question.get.ajax')}}",
                    method: 'POST',
                    data: {job_id:job_id, _token:token,type:'competency'},
                    success: function(data){
                        $("select[name='competency_id']").html(data);
                        $("select[name='modul_id'] , select[name='level_id']").html("<option>-</option>");
                    }
                });
            });

            $("select[name='competency_id']").change(function(){
                var competency_id = $(this).val();
                var job_id = $("select[name='job_id']").val();
                var token = $("input[name='_token']").val();
                $("select[name='modul_id']").html("loading...");
                $.ajax({
                    url: "{{route('question.get.ajax')}}",
                    method: 'POST',
                    data: {job_id:job_id,competency_id:competency_id, _token:token,type:'modul'},
                    success: function(data){
                        $("select[name='modul_id']").html(data);
                        $("select[name='level_id']").html("<option>-</option>");
                    }
                });
            });
            $("select[name='modul_id']").change(function(){
                var modul_id = $(this).val();
                var competency_id = $("select[name='competency_id']").val();
                var job_id = $("select[name='job_id']").val();
                var token = $("input[name='_token']").val();
                $("select[name='level_id']").html("loading...");
                $.ajax({
                    url: "{{route('question.get.ajax')}}",
                    method: 'POST',
                    data: {job_id:job_id,competency_id:competency_id,modul_id:modul_id, _token:token,type:'level'},
                    success: function(data){
                        $("select[name='level_id']").html(data);
                    }
                });
            });

        })
    </script>
@endsection