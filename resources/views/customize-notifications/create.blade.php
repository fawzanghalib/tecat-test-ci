@extends('layouts.home')
@section('title-tab')
    Create Notification 
@endsection
@section('title-content')
    Create Notification
@endsection
@section('button')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('customize-notification.index')}}">Master Notification</a></li>
    <li class="breadcrumb-item active">Create Notification</li>
</ol>
@endsection
@section('main-content')
    <div class="container">
        
            {{-- tab view --}}
            <button class="tablink" onclick="openTab('Daily', this, '#757474')" id="defaultTab">Daily</button>
            <button class="tablink" onclick="openTab('Feedback', this, '#757474')">Feedback</button>
            <button class="tablink" onclick="openTab('Loyalty', this, '#757474')">Loyalty</button>
            <button class="tablink" onclick="openTab('Broadcast', this, '#757474')">Broadcast</button>

            {{-- tab content --}}
            <div id="Daily" class="row tabcontent">
                <div class="card card-shadow col-md-12 ">
                    
                    {{-- daily --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Edit - Master Daily Notification Format
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                    <form action="{{ route('updateDefaultNotification.update') }}" autocomplete="off" method="POST">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="hidden" name="notification_id" value="{{ $data[3]->notification_id }}" class="form-control">
                                                    <input type="hidden" name="type" value="{{ $data[3]->type }}" class="form-control" required="required">   
                                                    <input type="hidden" name="notification_name" value="{{ $data[3]->notification_name }}" class="form-control" required="required">
                                                    <textarea required name="template" class="form-control textarea" maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data[3]->template }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-success btn-block col-md-2 float-md-right" type="submit">Update</button>        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    {{-- custom seasonal --}}
                    <div class="card-block">
                        <div class="col-md-12">
                        <div class="row">
                            <h6 style="margin-left:20px;">
                                <input type="radio" name="cars" id="cs1" value="1" checked ><label for="cs1">&nbsp; Custom Reminder Seasonal Format</label>
                            </h6>
                            <h6 style="margin-left:20px;">
                                <input type="radio" name="cars" id="cs2" value="2"><label for="cs2">&nbsp; Custom Reminder - Feedback - Scoring Seasonal Format</label>
                            </h6>
                        </div>
                        
                            <div class="card desc" id="Cars1">
                                <div class="card-block">
                                <form action="{{ route('storeNotification.store') }}" autocomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="type" value="Daily" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="Custom Seasonal Format" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> Spesial ulang tahun ACC! Dapatkan point +20 jika kamu menjawab benar Daily Test, hanya berlaku hari ini. Ayo buruan jawab!</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Start Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" id="sd1" name="start_date" onchange="setEndDate()" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" disabled id="ed1" name="end_date" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-warning btn-block  col-md-2 float-md-right" type="submit">Save</button>        
                                </form>
                                </div>
                            </div>

                            <div class="card desc" id="Cars2"  style="display: none;">
                                <div class="card-block">
                                <form action="{{ route('storeNotificationCustom.store') }}" autocomplete="off" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Template Reminder') }}</label>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <input type="hidden" name="custom_format" value="1" class="form-control" required="required">  
                                                <input type="hidden" name="type_reminder" value="Daily" class="form-control" required="required">   
                                                <input type="hidden" name="notification_name_reminder" value="Custom Seasonal Format" class="form-control" required="required">
                                                <textarea required name="template_reminder" class="form-control textarea">Spesial ulang tahun ACC! Dapatkan point +20 jika kamu menjawab benar Daily Test, hanya berlaku hari ini. Ayo buruan jawab!</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Template Feedback') }}</label>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <input type="hidden" name="type_feedback" value="Feedback" class="form-control" required="required">   
                                                <input type="hidden" name="notification_name_feedback" value="Custom Seasonal Format Correct" class="form-control" required="required">
                                                <textarea required name="template_feedback" class="form-control textarea">Bahagianya ACC, di hari ulang tahunnya kamu berhasil menjawab benar daily test mu. Kamu mendapat extra point +20. Selamat!</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Scoring Type') }}</label>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                            <input type="hidden" name="type" value="2"/>
                                            <input type="text" value="Jawaban Benar & Tepat waktu" class="form-control" readonly>   
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Scoring Name') }}</label>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <input name="scoring_type" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                        </div>
                                        <label class="col-sm-2 col-form-label">{{ __('Score') }}</label>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <input name="score" type="number" class="form-control" required >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Start Date') }}</label>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                            <input type="date" max="9999-12-31" id="sd2" name="start_date" onchange="setEndDate2()" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                        </div>
                                        <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                            <input type="date" max="9999-12-31" id="ed2" disabled name="end_date" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                        <button class="btn btn-warning btn-block  col-md-2 float-md-right" type="submit">Save</button>        
                                </form>
                                </div>
                            </div>
                                
                        </div>
                    </div>

                    {{-- on time answered --}}
                    <div class="card-body">
                        <div class="col-md-12">
                            <h6>
                                Edit - Master On Time Answered Question Notification Format
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('updateDefaultNotification.update') }}" autocomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="notification_id" value="{{ $data[2]->notification_id }}" class="form-control">
                                            <input type="hidden" name="type" value="{{ $data[2]->type }}" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="{{ $data[2]->notification_name }}" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data[2]->template }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-success btn-block  col-md-2 float-md-right" type="submit">Update</button>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div id="Feedback" class="tabcontent">
                <div class="card card-shadow col-md-12 ">
                    {{-- correct answer --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Edit - Master Correct Answer Notification Format
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('updateDefaultNotification.update') }}" autocomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="notification_id" value="{{ $data[0]->notification_id }}" class="form-control">
                                            <input type="hidden" name="type" value="{{ $data[0]->type }}" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="{{ $data[0]->notification_name }}" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data[0]->template }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-success btn-block  col-md-2 float-md-right" type="submit">Update</button>        
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- custom seasonal --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Add - Custom Seasonal Format Correct Answer
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('storeNotification.store') }}" autcomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="type" value="Feedback" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="Custom Seasonal Format Correct" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> Bahagianya ACC, di hari ulang tahunnya kamu berhasil menjawab benar daily test mu. Kamu mendapat extra point +20. Selamat!</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Start Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" id="sd3" name="start_date" onchange="setEndDate3()" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" id="ed3" disabled name="end_date" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-warning btn-block  col-md-2 float-md-right" type="submit">Save</button>        
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- uncorrect answer --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Edit - Master Uncorrect Answer Notification Format
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('updateDefaultNotification.update') }}" autcomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="notification_id" value="{{ $data[1]->notification_id }}" class="form-control">
                                            <input type="hidden" name="type" value="{{ $data[1]->type }}" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="{{ $data[1]->notification_name }}" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data[1]->template }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-success btn-block  col-md-2 float-md-right" type="submit">Update</button>       
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- custom seasonal --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Add - Custom Seasonal Format
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('storeNotification.store') }}" autcomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="type" value="Feedback" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="Custom Seasonal Format Uncorrect" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" readonly maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> Oh tidak, penalty point -5 karena kamu sudah 3 kali menjawab salah pertanyaan yang sama.</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Start Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" name="start_date" class="form-control" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" name="end_date" class="form-control" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Receiver') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <select class="form-control" disabled>
                                            <option> - </option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-warning btn-block  col-md-2 float-md-right" type="submit" disabled >Save</button>        
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="Loyalty" class="tabcontent">
                <div class="card card-shadow col-md-12 ">
                    {{-- after contributor submit --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Edit - Master After Contributor Submit the question
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('updateDefaultNotification.update') }}" autcomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="notification_id" value="{{ $data[4]->notification_id }}" class="form-control">
                                            <input type="hidden" name="type" value="{{ $data[4]->type }}" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="{{ $data[4]->notification_name }}" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" readonly maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data[4]->template }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-success btn-block  col-md-2 float-md-right" type="submit" disabled>Update</button>        
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- question submit declined --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Edit - Master Question Submitted Declined
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('updateDefaultNotification.update') }}" autcomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="notification_id" value="{{ $data[5]->notification_id }}" class="form-control">
                                            <input type="hidden" name="type" value="{{ $data[5]->type }}" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="{{ $data[5]->notification_name }}" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" readonly maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data[5]->template }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-success btn-block  col-md-2 float-md-right" type="submit" disabled>Update</button>        
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- question submit approved --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Edit - Master Question Submitted Approved
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('updateDefaultNotification.update') }}" autcomplete="off" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="notification_id" value="{{ $data[6]->notification_id }}" class="form-control">
                                            <input type="hidden" name="type" value="{{ $data[6]->type }}" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="{{ $data[6]->notification_name }}" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" readonly maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> {{ $data[6]->template }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-success btn-block  col-md-2 float-md-right" type="submit" disabled>Update</button>        
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- custom seasonal --}}
                    <div class="card-block">
                        <div class="col-md-12">
                            <h6>
                                Add - Custom Seasonal Format
                            </h6>
                            <div class="card">
                                <div class="card-block">
                                <form action="{{ route('storeNotification.store') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="type" value="Loyalty" class="form-control" required="required">   
                                            <input type="hidden" name="notification_name" value="Custom Seasonal Format Loyalty" class="form-control" required="required">
                                            <textarea required name="template" class="form-control textarea" readonly maxlength="350" minlength="6" onKeyPress="return angkadanhuruf(event,' !?+><%$.,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/> Bahagianya ACC, di hari ulang tahun ini soal yang kamu upload disetujui oleh komite bank soal. Kamu mendapat extra point +20. Selamat!</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Start Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" name="start_date" class="form-control" readonly required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('End Date') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        <input type="date" max="9999-12-31" name="end_date" class="form-control" readonly required>
                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-warning btn-block  col-md-2 float-md-right" type="submit" disabled>Save</button>        
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>

            <div id="Broadcast" class="row tabcontent">
                <div class="card card-shadow col-md-12 ">
                    
                </div>
            </div>

    </div>
    <script>
    $(document).ready(function() {
        $("input[name$='cars']").click(function() {
            var test = $(this).val();

            $("div.desc").hide();
            $("#Cars" + test).show();
        });
    });
    </script>

<style>
    .textarea{
        max-height: 100px;
        min-height: 50px;
    }

    .tablink {
        background-color: #555;
        color: white;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        font-size: 17px;
        width: 25%;
        margin-bottom:30px;
    }

    .tabcontent {
        display: none;
        padding: 10px;
    }
</style>

<script>
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    today = yyyy+'-'+mm+'-'+dd;

    function setEndDate()
    {
        var StartDate= document.getElementById('sd1').value;
        if(StartDate!='')
        {
            document.getElementById('ed1').disabled = false;
            $('#ed1').val("");
            $('#ed1').attr('min' , StartDate);
            return false;
        }
        
    }

    function setEndDate2()
    {
        var StartDate= document.getElementById('sd2').value;
        if(StartDate!='')
        {
            document.getElementById('ed2').disabled = false;
            $('#ed2').val("");
            $('#ed2').attr('min' , StartDate);
            return false;
        }
        
    }

    function setEndDate3()
    {
        var StartDate= document.getElementById('sd3').value;
        if(StartDate!='')
        {
            document.getElementById('ed3').disabled = false;
            $('#ed3').val("");
            $('#ed3').attr('min' , StartDate);
            return false;
        }
        
    }

    function openTab(cityName,elmnt,color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(cityName).style.display = "block";
        elmnt.style.backgroundColor = color;
    }

    document.getElementById("defaultTab").click();

</script>
@endsection
