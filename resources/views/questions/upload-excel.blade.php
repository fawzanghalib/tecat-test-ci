@extends('layouts.home')
@section('title-tab')
    Input Bank Soal
@endsection
@section('title-content')
    Input Bank Soal (excel)
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('bank-soal.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('question.create')}}" >Input Bank Soal</a></li>
    <li class="breadcrumb-item active">Input Bank Soal (excel)</li>
</ol>
@endsection
@section('button')

@endsection
@section('main-content')
<!--ExampleExcelUploadModal-->
<div class="modal fade" id="ExampleExcelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width:1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Contoh File Excel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md 6">
                        <a href="{{URL::asset('img/excelExampleFormatCompetencies.png')}}" target="_blank">
                            <img src="{{URL::asset('img/excelExampleFormatCompetencies.png')}}" style="width:100%; ">
                        </a>
                        <br/><br/>
                        <center><h4>Pada Questions Tipe : <b>Competencies</b></h4></center>
                        <ol>
                            <li><u>Jangan membuka file</u> yang akan di upload</li>
                            <li><b>Maksimal 1000 soal dalam 1 package</b></li>
                            <li>Berada di <u>sheet pertama</u></li>
                            <li>Memiliki urutan sebagai berikut
                                <ul>
                                    <li>Column 1 untuk Nama Fungsi</li>
                                    <li>Column 2 untuk Topik / Learning Point</li>
                                    <li>Column 3 untuk Nama Kompetensi</li>
                                    <li>Column 4 untuk Level</li>
                                    <li>Column 5 untuk Nama Job</li>
                                    <li>Column 6 untuk Soal</li>
                                    <li>Column 7,8,9,10 untuk Pilihan Jawaban</li>
                                    <li>Column 11 untuk Kunci Jawaban
                                </ul>
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-6">
                        <a href="{{URL::asset('img/excelExampleFormatGeneral.png')}}" target="_blank">
                            <img src="{{URL::asset('img/excelExampleFormatGeneral.png')}}" style="width:100%; ">
                        </a>
                        <br/><br/>
                        <center><h4>Pada Questions Tipe : <b>General</b></h4></center>
                        <ol>
                            <li><u>Jangan membuka file</u> yang akan di upload</li>
                            <li><b>Maksimal 1000 soal dalam 1 package</b></li>
                            <li>Berada di <u>sheet pertama</u></li>
                            <li>Memiliki urutan sebagai berikut
                                <ul>
                                    <li>Column 1 untuk Topik / Learning Point</li>
                                    <li>Column 2 untuk Soal</li>
                                    <li>Column 3,4,5,6 untuk Pilihan Jawaban</li>
                                    <li>Column 7 untuk Kunci Jawaban</li>
                                </ul>
                            </li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card card-block">
        <form id="uploadForm" class="needs-validation" action="{{route('question.storeWithExcel')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if ($errors->any())
                <div class="alert alert-danger mt-3">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <div class="mb-3 col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Questions Bank Package<span style="color:red"> *</span></label>
                            <div class="input-group">
                                <select name="package_id" required class="form-control custom-select" id=" ">                                
                                    <option value=""> - </option>
                                    @foreach ($packages as $package)
                                        <option value="{{$package->package_id}}"> {{$package->package_name}} </option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <a href="{{route('question-excel.create-package')}}"><button class="btn btn-outline-secondary" type="button">+</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Questions Type<span style="color:red"> *</span></label>
                            <select name="question_type_id" required class="form-control custom-select" id=" ">
                                <option value="">-</option>
                                @foreach ($questionTypes as $questionType)
                                    <option value="{{$questionType->question_type_id}}">{{$questionType->question_type_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mb-5 col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <label>File excel<span style="color:red"> *</span></label>
                            <button type="button" style="background-color:transparent;border:none" data-toggle="modal" data-target="#ExampleExcelModal">
                                <i class="fas fa-question-circle"></i>
                            </button>
                            <input type="file" class="form-control" id="customFile" name="file">
                        </div>

                        <div class="mt-4 col-md-6">
                            <label class="alert alert-danger">
                                <i class="fas fa-info-circle"></i>&nbsp;Maksimal 1000 soal dalam 1 package
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-warning btn-lg btn-block submit-button" type="submit">Add Questions</button>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#uploadForm').submit(function(e) {
            $('.submit-button').html("<i class='fa fa-spinner fa-spin'></i> Process");
            $('.submit-button').attr("disabled", true);
        });
        $("#customFile").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    })
</script>
@endsection