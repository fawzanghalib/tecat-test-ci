@extends('layouts.home')
@section('title-tab')
    Edit Question | {{$question->question}}
@endsection
@section('title-content')
    <a href="{{route('package.show',['id' => $question->package_id])}}" >
        <i style="color:black; font-size:1.5rem" class="fas fa-arrow-left"></i>
    </a>
    Question Edit
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <form class="needs-validation" action="{{route('question.update')}}" method="POST" autocomplete="off">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <input type="hidden" name="question_id" value="{{$question->question_id}}">
                <div class="row">
                    <div class="card card-body col-md-12 mb-2">
                        @if ($errors->any())
                            <div class="alert alert-danger mt-3">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="row ml-1">
                                <div class="mb-3 col-md-6">
                                    <label>Questions Bank Package<span style="color:red"> *</span></label>
                                    <div class="input-group">
                                        <select name="package_id" class="form-control custom-select" id=" ">                                
                                            @foreach ($packages as $package)
                                                <option value="{{$package->package_id}}"
                                                @if ($package->package_id == $question->package_id)
                                                    selected
                                                @endif> 
                                                {{$package->package_name}} </option>
                                            @endforeach
                                            <option value=""></option>
                                        </select>
                                        <div class="input-group-append">
                                            <a href="{{route('package.create')}}"><button class="btn btn-outline-secondary" type="button">+</button></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label>Questions Type<span style="color:red"> *</span></label>
                                    <select name="question_type_id" class="form-control custom-select" id=" ">
                                        @foreach ($questionTypes as $questionType)
                                            <option value="{{$questionType->question_type_id}}"
                                            @if ($questionType->question_type_id == $question->question_type_id)
                                                    selected
                                            @endif>    
                                            {{$questionType->question_type_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row ml-1">
                                <div class="mb-3 col-md-6">
                                    <label>Job Name
                                        @if($question->question_type_id==1)
                                            <span class="mandatory" style="color:red"> *</span>
                                        @else
                                            <span class="mandatory" style="color:transparent"> *</span>
                                        @endif
                                    </label>
                                    <select name="job_id" class="form-control custom-select" id="">
                                        <option value="">-</option>
                                        @foreach($jobs as $job)
                                            <option value="{{ $job->job_id }}"
                                            @if ($job->job_id  == $question->job_id)
                                                    selected
                                            @endif>
                                            {{$job->job_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label>Competency Name
                                        @if($question->question_type_id==1)
                                            <span class="mandatory" style="color:red"> *</span>
                                        @else
                                            <span class="mandatory" style="color:transparent"> *</span>
                                        @endif
                                    </label>
                                    <select name="competency_id" class="form-control custom-select" id="">
                                        <option value="">-</option>
                                        @foreach ($competencies as $competency)
                                            <option value="{{$competency->competency_id}}"
                                            @if ($competency->competency_id == $question->competency_id)
                                                    selected
                                            @endif>    
                                            {{$competency->competency_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row ml-1">
                                <div class="mb-3 col-md-6">
                                    <label>Level
                                        @if($question->question_type_id==1)
                                            <span class="mandatory" style="color:red"> *</span>
                                        @else
                                            <span class="mandatory" style="color:transparent"> *</span>
                                        @endif
                                    </label>
                                    <select name="level_id" class="form-control custom-select" id="">
                                        <option value="">-</option>
                                        @foreach ($levels as $level)
                                            <option value="{{$level->level_id}}"
                                            @if ($level->level_id == $question->level_id)
                                                    selected
                                            @endif>    
                                            {{$level->level_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label>Modul</label>
                                    <select name="modul_id" class="form-control custom-select" id="">
                                        <option value="">-</option>
                                        @foreach ($moduls as $modul)
                                            <option value="{{$modul->modul_id}}"
                                            @if ($modul->modul_id == $question->modul_id)
                                                    selected
                                            @endif>    
                                            {{$modul->modul_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body col-md-12">
                        <div class="mb-3 col-md-12">
                            <label>Questions<span style="color:red"> *</span></label>
                            <textarea name="question" class="form-control" id="exampleFormControlTextarea1" required rows="3" maxlength="550" minlength="6">{{old('question') ?? $question->question}}</textarea>
                        </div>
                        <div class="mb-3 col-md-12">
                            <label>Answer<span style="color:red"> *</span></label>
                            <div class="row">
                                @php
                                    $i = 2;
                                @endphp
                                @foreach ($question->mstQuestionAnswers as $answer)
                                <div class="col-md-6 mb-3">
                                            @if ($answer->score == 1)
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label class="label label-success" style="margin-left:-10px">True</label>
                                                </div>
                                                <div class="col-md-11">
                                                    <input name="answer_1_id" type="hidden" value="{{$answer->question_answer_id }}">
                                                    <input name="answer_1" type="text" maxlength="550" minlength="6" class="form-control" value="{{ old('answer_1') ?? $answer->answer }}" required="" autocomplete="off">
                                                </div>
                                            </div>
                                            @else
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label class="label label-danger" style="margin-left:-10px">False</label>
                                                </div>
                                                <div class="col-md-11">
                                                    <input name="answer_{{$i}}_id" type="hidden" value="{{$answer->question_answer_id }}">
                                                    <input name="answer_{{$i}}" type="text" maxlength="550" minlength="6" class="form-control" value="{{ old('answer_'.$i) ?? $answer->answer }}" required="" autocomplete="off">
                                                
                                                @php
                                                    $i++;
                                                @endphp
                                                </div>
                                            </div>
                                            @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row ml-1">
                            <div class="mb-3 col-md-3">
                                <label>Start Date<span style="color:red"> *</span></label>
                                <input name="start_date" type="date" max="9999-12-31" class="form-control" required value="{{ old('start_date') ?? $question->start_date}}">
                            </div>
                            <div class="mb-3 col-md-3">
                                <label>End Date</label>
                                <input name="end_date" type="date" max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $question->end_date}}">
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-warning btn-lg btn-block" type="submit">Update Question</button>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            if ($("select[name='question_type_id']").val() != 1) {
                $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").attr('disabled',true);
            }
            $("select[name='question_type_id']").change(function(){
                if ($(this).val() == 1) {
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").attr('disabled',false);
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").val(0);
                    $("select[name='level_id'] , select[name='competency_id']").html("<option>-</option>");
                    $(".mandatory").css("color","red");
                }
                else if($(this).val() != 1){
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").attr('disabled',true);
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").val(0);
                    $("select[name='level_id'] , select[name='competency_id']").html("<option>-</option>");
                    $(".mandatory").css("color","transparent");
                }
            })

            $("select[name='job_id']").change(function(){
                var job_id = $(this).val();
                var token = $("input[name='_token']").val();
                $("select[name='competency_id']").html("loading...");
                console.log(job_id);
                $.ajax({
                    url: "{{route('question.get.ajax')}}",
                    method: 'POST',
                    data: {job_id:job_id, _token:token,type:'competency'},
                    success: function(data){
                        $("select[name='competency_id']").html(data);
                        $("select[name='level_id']").html("<option>-</option>");
                    }
                });
            });

            $("select[name='competency_id']").change(function(){
                var competency_id = $(this).val();
                var job_id = $("select[name='job_id']").val();
                var token = $("input[name='_token']").val();
                $("select[name='level_id']").html("loading...");
                $.ajax({
                    url: "{{route('question.get.ajax')}}",
                    method: 'POST',
                    data: {job_id:job_id,competency_id:competency_id, _token:token,type:'level'},
                    success: function(data){
                        $("select[name='level_id']").html(data);
                    }
                });
            });

            $('.job-select2').select2();

        })
    </script>
@endsection