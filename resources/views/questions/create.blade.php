@extends('layouts.home')
@section('title-tab')
    Input Bank Soal
@endsection
@section('title-content')
    Input Bank Soal
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('bank-soal.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Input Bank Soal</li>
</ol>
@endsection
@section('button')
    <a href="{{route('question.createWithExcel')}}" class="btn pull-right hidden-sm-down btn-success">
        <i class="far fa-file-excel"></i> 
        Submit Using Excel  
    </a>
@endsection
@section('main-content')
<div class="card">
    <div class="card card-block">
        <form class="needs-validation" action="{{route('question.store')}}" method="POST" autocomplete="off">
            {{ csrf_field() }}
            <div class="row">  
                <div class="card card-body col-md-12 mb-2">
                    @if ($errors->any())
                        <div class="alert alert-danger mt-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="row ml-1">
                            <div class="mb-3 col-md-6">
                                <label>Questions Bank Package<span style="color:red"> *</span></label>
                                <div class="input-group">
                                    <select name="package_id" class="form-control custom-select" id=" ">   
                                        <option value=""> - </option>
                                        @foreach ($packages as $package)
                                            <option value="{{$package->package_id}}"
                                                @if (old('package_id') ==  $package->package_id)
                                                    selected
                                                @endif
                                            > {{$package->package_name}} </option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <a href="{{route('question.create-package')}}"><button class="btn btn-outline-secondary" type="button">+</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label>Questions Type<span style="color:red"> *</span></label>
                                <select name="question_type_id" class="form-control custom-select" id=" ">
                                    <option value="">-</option>
                                    @foreach ($questionTypes as $questionType)
                                        <option value="{{$questionType->question_type_id}}"
                                            @if (old('question_type_id') ==  $questionType->question_type_id)
                                                selected
                                            @endif
                                        >{{$questionType->question_type_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row ml-1">
                            <div class="mb-3 col-md-6">
                                <label>Job Name
                                    @if(old('question_type_id')==1)
                                        <span class="mandatory" style="color:red"> *</span>
                                    @else
                                        <span class="mandatory" style="color:transparent"> *</span>
                                    @endif
                                </label>
                                
                                <select name="job_id" class="form-control custom-select">
                                    <option value="">-</option>
                                    @foreach ($functions as $function)
                                    <optgroup label="{{$function->function_name}}">                                    
                                        @foreach($function->mstJobs as $job)
                                            <option value="{{ $job->job_id }}"
                                            @if (old('job_id') == $job->job_id)
                                            selected="selected"
                                            @endif
                                            >{{$job->job_name}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label>Competency Name
                                        @if(old('question_type_id')==1)
                                            <span class="mandatory" style="color:red">*</span>
                                        @else
                                            <span class="mandatory" style="color:transparent">*</span>
                                        @endif
                                </label>
                                <select name="competency_id" class="form-control custom-select" id="">
                                    <option value="">-</option>
                                    @foreach($competencies as $competency)
                                        <option value="{{ $competency->competency_id }}"
                                        @if (old('competency_id') == $competency->competency_id)
                                        selected="selected"
                                        @endif
                                        >{{$competency->competency_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row ml-1">
                            <div class="mb-3 col-md-6">
                                <label>Level Job Name
                                        @if(old('question_type_id')==1)
                                            <span class="mandatory" style="color:red"> *</span>
                                        @else
                                            <span class="mandatory" style="color:transparent"> *</span>
                                        @endif
                                </label>
                                <select name="level_id" class="form-control custom-select" id="">
                                    <option value="">-</option>
                                    @foreach($levels as $level)
                                        <option value="{{ $level->level_id }}"
                                        @if (old('competency_id') == $level->level_id)
                                        selected="selected"
                                        @endif
                                        >{{$level->level_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label>Modul</label>
                                <select name="modul_id" class="form-control custom-select" id="">
                                    <option value=""> - </option>
                                    @foreach($moduls as $modul)
                                        <option value="{{ $modul->modul_id }}">{{$modul->modul_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body col-md-12">
                    <div class="mb-3 col-md-12">
                        <label>Questions<span style="color:red"> *</span></label>
                        <textarea name="question" class="form-control" id="exampleFormControlTextarea1" rows="3" required maxlength="550" minlength="6"> {{ old('question') ?? "" }}</textarea>
                    </div>
                    <div class="mb-3 col-md-12">
                        <label>Answer<span style="color:red"> *</span></label>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label class="label label-success" style="margin-left:-10px">True</label>
                                    </div>
                                    <div class="col-md-11">
                                        <input name="answer_1" type="text" class="form-control" value="{{ old('answer_1') ?? "" }}" required="" maxlength="550" minlength="6">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label class="label label-danger" style="margin-left:-10px">False</label>
                                    </div>
                                    <div class="col-md-11">
                                        <input name="answer_2" type="text" class="form-control" value="{{ old('answer_2') ?? "" }}" required="" maxlength="550" minlength="6">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label class="label label-danger" style="margin-left:-10px">False</span>
                                    </div>
                                    <div class="col-md-11">
                                        <input name="answer_3" type="text" class="form-control" value="{{ old('answer_3') ?? "" }}" required="" maxlength="550" minlength="6">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label class="label label-danger" style="margin-left:-10px">False</label>
                                    </div>
                                    <div class="col-md-11">
                                        <input name="answer_4" type="text" class="form-control" value="{{ old('answer_3') ?? "" }}" required="" maxlength="550" minlength="6">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ml-1">
                        <div class="col-md-2 mb-3">
                            <label>Start Date<span style="color:red"> *</span></label>
                            <input name="start_date" type="date" max="9999-12-31" class="form-control"  required value="{{ old('start_date') ?? "" }}" min="1">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label>End Date</label>
                            <input name="end_date" type="date" max="9999-12-31" class="form-control" value="{{ old('end_date') ?? "" }}" min="1">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <button class="btn btn-warning btn-lg btn-block" type="submit">Add Question</button>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            if ($("select[name='question_type_id']").val() != 1) {
                $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").attr('disabled',true);
            }
            $("select[name='question_type_id']").change(function(){
                if ($(this).val() == 1) {
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").attr('disabled',false);
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").val(0);
                    $("select[name='level_id'] , select[name='competency_id']").html("<option>-</option>");
                    $(".mandatory").css("color","red");
                }
                else if($(this).val() != 1){
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").attr('disabled',true);
                    $("select[name='job_id'] , select[name='level_id'] , select[name='competency_id']").val(0);
                    $("select[name='level_id'] , select[name='competency_id']").html("<option>-</option>");
                    $(".mandatory").css("color","transparent");
                }
            })

            $("select[name='job_id']").change(function(){
                var job_id = $(this).val();
                var token = $("input[name='_token']").val();
                $("select[name='competency_id']").html("loading...");
                $.ajax({
                    url: "{{route('question.get.ajax')}}",
                    method: 'POST',
                    data: {job_id:job_id, _token:token,type:'competency'},
                    success: function(data){
                        $("select[name='competency_id']").html(data);
                        $("select[name='level_id']").html("<option>-</option>");
                    }
                });
            });

            $("select[name='competency_id']").change(function(){
                var competency_id = $(this).val();
                var job_id = $("select[name='job_id']").val();
                var token = $("input[name='_token']").val();
                $("select[name='level_id']").html("loading...");
                $.ajax({
                    url: "{{route('question.get.ajax')}}",
                    method: 'POST',
                    data: {job_id:job_id,competency_id:competency_id, _token:token,type:'level'},
                    success: function(data){
                        $("select[name='level_id']").html(data);
                    }
                });
            });

            $('.job-select2').select2();

        })
    </script>
@endsection