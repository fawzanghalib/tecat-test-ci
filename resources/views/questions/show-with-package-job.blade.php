@extends('layouts.home')
@section('title-tab')
    Question - {{$package->package_name}} - {{$job->job_name}}
@endsection
@section('title-content')
    <a href="{{route('package.show',['id' => $package->package_id])}}" >
        <i style="color:black; font-size:1.5rem" class="fas fa-arrow-left"></i>
    </a>
    {{$package->package_name}} - {{$job->job_name}} <span style="font-size:1rem;"><i>Question</i></span>
@endsection
@section('main-content')
    <div class="modal fade filter-question" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <button type="button" class="btn btn-info btn-md float-right mb-3" data-toggle="modal" data-target=".filter-question">
                <i class="fas fa-filter"></i>
                filter
            </button>
            <div style="clear:right;"></div>
            <div class="row">
                <div class="card card-shadow col-md-12">
                    @foreach ($questions as $question)
                        <div class="row mb-5 mt-2">
                            <div class="col-md-9">
                                <h5 class="mt-3"> {{$question->question}} </h5>
                                <ul>
                                    @foreach ($question->mstQuestionAnswers as $answer)
                                        <li @if ($answer->score == true)
                                                style="color:green; font-weight:bold;"
                                            @endif>
                                            {{$answer->answer}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-3" style="border-left: 1px solid lightgray;">
                                <span class="mt-1" style="display:inline-block"> {{$question->competency_name}} </span><br>
                                <span class="mt-1" style="display:inline-block"> {{$question->modul_name}} </span><br>
                                <span class="mt-1" style="display:inline-block"> {{$question->level_name}} </span><br>
                                <span class="mt-1" style="display:inline-block;
                                    @if (strtotime($question->start_date) > strtotime($today))
                                        color:blue;
                                    @elseif(strtotime($question->end_date) < strtotime($today))
                                        color:red;
                                    @endif">
                                    {{$question->start_date}} - {{$question->end_date}} 
                                </span><br>
                                <div class="mt-3">
                                    <a href="{{route('question.edit',['question_id' => $question->question_id])}}">
                                        <button class="btn btn-sm btn-success">
                                            <i>edit</i>
                                        </button>
                                    </a>
                                    <form style="display: inline;" action="{{route('question.delete',['id' => $question->question_id])}}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="hidden" name="job_id" value="{{$question->job_id}}">
                                        <input type="hidden" name="package_id" value="{{$question->package_id}}">
                                        <button class="btn btn-sm btn-warning" type="submit"><i>disable</i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection