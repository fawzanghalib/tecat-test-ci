@extends('layouts.template')
@section('title-tab')
    Progress Uploading
@endsection
@section('head')
    <style>
        #myProgress {
            width: 100%;
            background-color: #ddd;
        }
        
        #myBar {
            width: 0%;
            height: 30px;
            background-color: #4CAF50;
            text-align: center;
            line-height: 30px;
            color: white;
        }
    </style>
@endsection
<body style="background-color:#f2f7fa">
    <div id="divSaveData" style="display:none">
        <button id="saveData">Save Question</button>
    </div>
    <div class="row">
        <div id="back-button" class="col-md-2">
        </div>
        <div class="col-md-8">
            <br/><h2 id="uploadProcessText">upload process...</h2><br>
            <div id="myProgress">
                <div id="myBar">0%</div>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                    Soal Di Terima
                    <span class="badge badge-pill badge-danger number-approved-question">0</span>
                </a>
            </li>
            <li class="nav-item" >
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                    Soal Di Tolak
                    <span class="badge badge-pill badge-danger number-not-approved-question">0</span>
                </a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active approved-question" id="home" role="tabpanel" aria-labelledby="home-tab">

            </div>
            <div class="tab-pane fade not-approved-question" id="profile" role="tabpanel" aria-labelledby="profile-tab">

            <form class="needs-validation" action="{{route('download-failed-question-upload')}}" method="POST" autocomplete="off" style="margin-top:15px;">
            {{ csrf_field() }}
                <input type="hidden" value="{{ $question_type_id }}" name="question_type_id"/>
                <input type="hidden" id="arrayIndexFailedUpload" value="" name="arrayIndexFailedUpload"/>
                <button class="btn btn-md btn-danger" type="submit">
                    <i class="far fa-file-excel"></i> 
                    Download  
                </button>
            </form>
            
            </div>
        </div>
    </div>
</body>
@section('script')
    <script type="text/javascript">
        var dataQuestion = '<?php echo $data; ?>'
        var myBar = $("#myBar");
        var QuestionLength = '<?php echo $questionsLength; ?>';
        var QuestionUploaded = 0;
        var notApprovedQuestion = 0;
        var ApprovedQuestion = 0;
        var arrayIndexSuccessUpload = [];
        var arrayIndexFailedUpload = [];

        myBar.innerHTML = QuestionUploaded + "/" + (QuestionLength-1);
        var progressUpload = function(){
            for(j=1; j < QuestionLength; j++){
                $.ajax({
                    url: "{{route('question.progressStoreWithExcel')}}",
                    method: 'POST',
                    // async: false,
                    data: {i:j,data:dataQuestion,_token:"{{ csrf_token() }}"},
                    error: function(){
                        console.log("error "+ j);
                        $("#myBar").css('background-color','red');
                        $("#uploadProcessText").html("<a href='{{route('bank-soal.dashboard')}}' style='color:black' ><i style='color:black; font-size:1.5rem' class='fas fa-arrow-left'></i>&nbsp;Back</a><center><a style='color:#dc3545'>Connection Lost</a></center>");
                        window.stop();
                    },
                    success: function(data){
                        console.log(j);
                        data = JSON.parse(data);
                        if(data['result'] == true){
                            $(".approved-question").append('<input type="hidden" id="checkSudahMasuk'+j+'">'+data['question'] +'<br>');
                            arrayIndexSuccessUpload.push(data['index']);
                            ApprovedQuestion++;
                        }
                        else{
                            $(".not-approved-question").append('<input type="hidden" id="checkSudahMasuk'+j+'">'+data['question'] +'<br>');
                            arrayIndexFailedUpload.push(data['index']);
                            notApprovedQuestion++;
                        }
                        QuestionUploaded++;
                        let width = Math.floor(QuestionUploaded*100/(QuestionLength-1));
                        myBar.html(QuestionUploaded + "/" + (QuestionLength-1));
                        myBar.css( "width" ,(width + "%" ));
                        $("#myBar").css('background-color','#4CAF50');
                        $('.number-not-approved-question').html(notApprovedQuestion);
                        $('.number-approved-question').html(ApprovedQuestion);
                        if(QuestionUploaded == QuestionLength-1){
                            saveData();
                            $( "#arrayIndexFailedUpload" ).val( arrayIndexFailedUpload );
                        }
                    }
                })
            }
        }

        function saveData(){
            $("#divSaveData").remove();
            $.ajax({
                url: "{{route('question.commiteStoreWithExcel')}}",
                method: 'POST',
                data: {data:dataQuestion,arrayIndexSuccessUpload:arrayIndexSuccessUpload,_token:"{{ csrf_token() }}"},
                success: function(data2){
                    console.log(data2);
                    if(data2){
                        $("#uploadProcessText").html("<a href='{{route('bank-soal.dashboard')}}' style='color:black' ><i style='color:black; font-size:1.5rem' class='fas fa-arrow-left'></i>&nbsp;Back</a><center><a style='color:#4CAF50'>DONE</a></center>");
                    }
                    else{
                        $("#uploadProcessText").html("<a href='{{route('bank-soal.dashboard')}}' style='color:black' ><i style='color:black; font-size:1.5rem' class='fas fa-arrow-left'></i>&nbsp;Back</a><center><a style='color:red'>FAILED</a></center>");
                    }
                    $('#back-button').html("")
                }
            })
        }

        progressUpload();
    </script>
@endsection