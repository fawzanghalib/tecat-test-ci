@extends('layouts.home')
@section('head')
    <style>
        .multiple-select-filter>span{
            width: 100% !important;
        }
    </style>
@endsection
@section('title-tab')
    Quiz - Add Question
@endsection
@section('title-content')
    <a href="{{route('quiz-que-grade.create',['quiz_id' => $quiz->quiz_id])}}" >
        <i style="color:black; font-size:1.5rem" class="fas fa-arrow-left"></i>
    </a>
    {{$quiz->quiz_name}} <span style="font-size:1rem;"><i>(Add Question)</i></span>
@endsection
@section('main-content')
    {{-- filter modal --}}
    <div class="modal fade filter-question" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('quiz-que-grade.create-step-2',['quiz_id' => $quiz->quiz_id,'package_id' => $package_id])}}" method="GET">
                        <label>Search</label>
                        <div class="row">
                            <div class="col-md-9 pr-0">
                                <input type="text" name="search" placeholder="Search By Question..." class="form-control" value="{{$request->get('search')}}">
                            </div>
                        </div>

                        <label class="mt-4">Job</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="job_id[]" class="custom-select select-job-filter" multiple="multiple">                                
                                    @foreach ($jobs as $job)
                                        <option value="{{$job->job_id}}"
                                        @if ($request->get('job_id') !== null)    
                                            @if (in_array($job->job_id,$request->get('job_id'), FALSE))
                                                selected
                                            @endif
                                        @endif
                                        > {{$job->job_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Competency</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="competency_id[]" class="custom-select select-competency-filter" multiple="multiple">                                
                                    @foreach ($competencies as $competency)
                                        <option value="{{$competency->competency_id}}"
                                            @if ($request->get('competency_id') !== null)
                                                @if (in_array($competency->competency_id,$request->get('competency_id'), FALSE))
                                                    selected
                                                @endif  
                                            @endif  
                                        > {{$competency->competency_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Level</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="level_id[]" class="custom-select select-level-filter" multiple="multiple">                                
                                    @foreach ($levels as $level)
                                        <option value="{{$level->level_id}}"
                                            @if ($request->get('level_id') !== null)
                                                @if (in_array($level->level_id, $request->get('level_id'), FALSE))
                                                    selected
                                                @endif
                                            @endif
                                        > level {{$level->level_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('quiz-que-grade.create-step-2',['quiz_id' => $quiz->quiz_id,'package_id' => $package_id])}}">
                                    <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label class="alert alert-danger">
                <i class="fas fa-info-circle"></i>&nbsp;Maksimal 1000 soal dalam 1 package
            </label>
        </div>
        <div class="col-md-8">
            <button type="button" class="btn btn-info btn-md float-right mb-5" data-toggle="modal" data-target=".filter-question">
                <i class="fas fa-filter"></i>
                filter
            </button>
        </div>
    </div>
    <div class="card ">
        <div class="card-block">
            <input id="checkAll" type="checkbox" onclick="checkFunction()"/> <label for="checkAll" style="font-weight:bold; font-size:18px">
                <p id="text" style="display:block">Check All</p>
                <p id="text2" style="display:none">Uncheck All</p> </label>
            <br/>
            <form action="{{ route('quiz-que-grade.store') }}" method="post">
                <div class="row">
                    <div class="card card-shadow col-md-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="quiz_id" value="{{ $quiz->quiz_id }}">
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($questions as $question)
                            <div class="row mb-5 mt-2">
                                <div class="col-md-9">
                                    <h5 class="">
                                        <label for="question[{{$i}}]">
                                        <input name="question[{{$i}}]" id="question[{{$i}}]" type="checkbox" value="{{ $question->question_id }}" />
                                            {{$question->question}}
                                        </label>
                                    </h5>
                                    <ul>
                                        @foreach ($question->mstQuestionAnswers as $answer)
                                            <li @if ($answer->score == true)
                                                    style="color:green; font-weight:bold;"
                                                @endif>
                                                {{$answer->answer}}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-md-3" style="border-left: 1px solid lightgray;">
                                    @if ($question->competency_name)    
                                        <span class="mt-1" style="display:inline-block"> No. {{$i+1}} </span><br>
                                        <span class="mt-1" style="display:inline-block"> {{$question->job_name}} </span><br>
                                        <span class="mt-1" style="display:inline-block"> {{$question->competency_name}} </span><br>
                                        <span class="mt-1" style="display:inline-block"> {{$question->level_name}} </span><br>
                                        <span class="mt-1" style="display:inline-block"> {{$question->modul_name}} </span><br>
                                    @else
                                        <span class="mt-1" style="display:inline-block"> {{$i+1}} </span><br>
                                        <span class="mt-1" style="display:inline-block">General </span><br>
                                    @endif
                                </div>
                            </div>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Add Question</button>
                </div>

            </form>
        </div>
    </div>
@endsection
    @section('script')
    <script>
        $("#checkAll").change(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);       
        });
        $(document).ready(function(){
            $(".select-competency-filter, .select-level-filter , .select-job-filter").select2();
        })
    </script>

    <script>
    function topFunction() {
        window.scrollTo(0,document.body.scrollHeight);
    }
    function checkFunction(){
        var checkBox = document.getElementById("checkAll");
        var text = document.getElementById("text");
        var text2 = document.getElementById("text2");

        if (checkBox.checked == true){
            text.style.display = "none";
            text2.style.display = "block";
        } else {
            text.style.display = "block";
            text2.style.display = "none";
        }
    }
    </script>
    
@endsection