@extends('layouts.home')
@section('title-tab')
Quiz - Add Question
@endsection
@section('title-content')
    Add Question
@endsection
@section('main-content')
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('quiz')}}">Master Quiz</a></li>
    <li class="breadcrumb-item"><a href="{{route('quiz.show',['id' => $quiz->quiz_id])}}">Detail Quiz</a></li>
    <li class="breadcrumb-item active">Add Question</li>
</ol>
@endsection
    <div class="container">
        <br/>
        <div class="row">
            @foreach ($packages as $package)
                <div class="col-md-4 mb-4">
                    <a style="color:transparent" href="{{route('quiz-que-grade.create-step-2',['quiz_id' => $quiz->quiz_id, 'package_id' => $package->package_id])}}">
                        <button type="submit" class="btn btn-warning btn-lg btn-block text-left">
                            <i class="fa fa-folder fa-lg"></i>
                            {{$package['package_name']}}
                        </button>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection

