@extends('layouts.home')
@section('title-tab')
    Edit Modul | {{$modul->modul_name}}
@endsection
@section('title-content')
    Edit Modul
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('modul')}}">Master Modul</a></li>
    <li class="breadcrumb-item active">Edit Modul </li>
</ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <form class="needs-validation" action="{{ route('modul.update', ['id' => $modul->modul_id]) }}" method="POST">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            <input name="modul_id" type="hidden"  required value="{{ $modul->modul_id }}">
            <div class="row">
                <div class="card card-shadow col-md-12">
                    <div class="card-body">
                        <div class="mb-3 col-md-6">
                            <label>Modul Name<span style="color:red">*</span> </label>
                            <input name="modul_name" type="text" class="form-control" maxlength="50" minlength="5" value="{{  old('modul_name') ?? $modul->modul_name}}" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                        </div>
                        <div class="mb-3 col-md-12">
                            <label>Description<span style="color:red">*</span></label>
                            <textarea name="description" class="form-control" id="" rows="3" maxlength="250" minlength="6" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>{{  old('description') ?? $modul->description}}</textarea>
                        </div>
                        <div class="mb-4 col-md-6">
                            <label>Flag Active :<span style="color:red">*</span></label>
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" type="radio" value="1" @if($modul->flag_active == 1) checked @endif>Active 
                            &nbsp;&nbsp;&nbsp;<input name="flag_active" type="radio" value="0" @if($modul->flag_active == 0) checked @endif>non Active  
                        </div>
                        <div class="row ml-1">
                            <div class="col-md-3 mb-3">
                                <label>Start Date<span style="color:red">*</span></label>
                                <input name="start_date" type="date" id="sd1" onchange="setEndDate()" max="9999-12-31" class="form-control"  required value="{{ old('start_date') ?? $modul->start_date }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>End Date</label>
                                <input name="end_date" type="date" id="ed1"  max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $modul->end_date }}" >
                                {{-- <input name="end_date" type="date" id="ed1" disabled max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $modul->end_date }}" > --}}
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
            </div>
            
        </form>
    </div>
</div>
@endsection