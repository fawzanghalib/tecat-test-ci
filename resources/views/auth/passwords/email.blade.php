@extends('layouts.app')

@section('content')
<center>
<div class="card mt-4">
    <div class="card-block">
        <div class="col-md-8 col-md-offset-2">
            <div class="card-block">
                <div class="panel-heading">
                    <h2>Reset Password<h2>
                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="panel-body">

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}" autocomplete="off">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6  mt-2">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required onKeyPress="return angkadanhuruf(event,'.@1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <label class="label label-danger">{{ $errors->first('email') }}</label>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1 text-left">
                                <a href="{{route('login')}}"><u>Login<u></a>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! NoCaptcha::renderJs() !!}
                            {!! NoCaptcha::display() !!}
                            <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-md btn-warning">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
@endsection
