@extends('layouts.app')

@section('content')
<center>
<div class="card mt-4">
    <div class="card-block">
        <div class="col-md-8 col-md-offset-2">
            <div class="card-block">
                <div class="panel-heading">
                    <h2>Reset Password</h2>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}" autocomplete="off">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6 mt-2">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus onKeyPress="return angkadanhuruf(event,'.@1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <label class="label label-danger">{{ $errors->first('email') }}</label>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6 mt-2 input-group">
                                <input id="password" minlength="8" type="password" class="form-control" name="password" required>
                                <div class="input-group-append" style="background-color:transparent">
                                    <a href="#" class="btn" style="color:black; background-color:transparent" onclick="intipNewPassword()"><i class="fas fa-eye"></i></a>
                                </div>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <label class="label label-danger">{{ $errors->first('password') }}</label>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6 mt-2 input-group">
                                <input id="password-confirm" minlength="8" type="password" class="form-control" name="password_confirmation" required>
                                <div class="input-group-append" style="background-color:transparent">
                                    <a href="#" class="btn" style="color:black; background-color:transparent" onclick="intipRePassword()"><i class="fas fa-eye"></i></a>
                                </div>
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <label class="label label-danger">{{ $errors->first('password_confirmation') }}</label>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-warning btn-md">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
<script>
  function intipNewPassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  function intipRePassword() {
    var x = document.getElementById("password-confirm");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
@endsection
