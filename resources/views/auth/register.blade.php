@extends('layouts.home')
@section('title-tab')
    Add User 
@endsection
@section('title-content')
    Add User
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('user')}}">Master User</a></li>
    <li class="breadcrumb-item active">Add User</li>
</ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card-block">
        <div class="col-md-9 col-md-offset-2">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}" autocomplete="off">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('npk') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">NPK<span style="color:red"> *</span></label>
                    <div class="col-md-6">
                        <input id="npk" type="text" maxlength="5" minlength="5" class="form-control" name="npk" value="{{ old('npk') }}" required autofocus onKeyPress="return angkadanhuruf(event,'1234567890',this)"/>

                        @if ($errors->has('npk'))
                            <span class="help-block">
                                <label class="label label-danger">{{ $errors->first('npk') }}</label>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name<span style="color:red"> *</span></label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" maxlength="30" minlength="3" name="name" value="{{ old('name') }}" required autofocus onKeyPress="return angkadanhuruf(event,' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <label class="label label-danger">{{ $errors->first('name') }}</label>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address<span style="color:red"> *</span></label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required onKeyPress="return angkadanhuruf(event,'_.@1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <label class="label label-danger">{{ $errors->first('email') }}</label>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-6 control-label">Password<span style="color:red"> *</span>
                    <label class="label control-label label-primary mt-1 mb-2">Harus mengandung huruf, angka, spesial karakter, dan capital.</label>
                    </label>
                    <div class="mb-3 col-md-6 input-group">
                        <input id="input-password" type="password" minlength="8" maxlength="14" class="form-control" name="password" required>
                        <div class="input-group-append">
                            <a href="#" onclick="intipNewPassword()"><button class="btn btn-md" type="button"><i class="fas fa-eye"></i></button></a>
                        </div>
                    </div>
                    @if ($errors->has('password'))
                            <span class="help-block">
                                <label class="label label-danger">{{ $errors->first('password') }}</label>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password<span style="color:red"> *</span></label>

                    <div class="mb-3 col-md-6 input-group">
                        <input id="repw" type="password" minlength="8" maxlength="14" class="form-control" name="password_confirmation" required>
                        <div class="input-group-append">
                            <a href="#" onclick="intipRePassword()"><button class="btn btn-md" type="button"><i class="fas fa-eye"></i></button></a>
                        </div>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <label class="label label-danger">{{ $errors->first('password') }}</label>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-warning btn-md">
                            Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

<script>
  function intipNewPassword() {
    var x = document.getElementById("input-password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  function intipRePassword() {
    var x = document.getElementById("repw");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
