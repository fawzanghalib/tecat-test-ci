@extends('layouts.app')

@section('content')
<center>
<div class="card mt-4">
    <div class="card-block">
        <div class="col-md-8 col-md-offset-5" >
            <div class="card-block">
                <div class="panel-heading">
                    <h2>Login Page</h2>
                </div>
                @if ($errors->has('email'))
                    <br/>
                    <span class="help-block alert alert-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    <br/><br/>
                @endif
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}" autocomplete="off">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-6 control-label text-left">E-Mail Address</label>
                            <div class="col-md-6 mt-2">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus onKeyPress="return angkadanhuruf(event,'_.@1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-6 control-label text-left">Password</label>

                            <div class="col-md-6 mt-2 input-group">
                                <input id="password" type="password" class="form-control" minlength="8" name="password" required>
                                <div class="input-group-append" style="background-color:transparent">
                                    <a href="#" class="btn" style="color:black; background-color:transparent" onclick="intipPassword()"><i class="fas fa-eye"></i></a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1 text-left">
                                <a href="{{route('password.reset')}}"><u>Reset Password<u></a>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! NoCaptcha::renderJs() !!}
                            {!! NoCaptcha::display() !!}
                            <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4">
                                <button type="submit" class="btn btn-lg btn-warning" style="padding: 8px 18px;">
                                    Login
                                </button>

                                {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
<style>
.card {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
}
</style>
<script>

  function intipPassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  </script>
@endsection

