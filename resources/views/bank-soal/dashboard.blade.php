@php
    $colorListDashboard = ['royalblue','orangered','orange','forestgreen','darkmagenta','lightseagreen','indianred','olivedrab','darkorchid','yellow','darkslateblue','firebrick','darkred','steelblue','chocolate','limegreen','mediumvioletred','deeppink','royalblue','orangered','orange','forestgreen','darkmagenta','lightseagreen','indianred','olivedrab','darkorchid','yellow','darkslateblue','firebrick','darkred','steelblue','chocolate','limegreen','mediumvioletred','deeppink','royalblue','orangered','orange','forestgreen','darkmagenta','lightseagreen','indianred','olivedrab','darkorchid','yellow','darkslateblue','firebrick','darkred','steelblue','chocolate','limegreen','mediumvioletred','deeppink',];
@endphp
@extends('layouts.home')
@section('title-tab')
    Dashboard Bank Soal 
@endsection
@section('title-content')
    Dashboard
@endsection
@if (isset($functionQuestionTotal))
    @section('main-content')
    <div style="overflow-x:auto; width:120%;">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            @foreach ($functionQuestionTotal as $item)    
                <li class="nav-item ml-3 mr-3" style="background-color:transparent">
                    <a class="nav-link row"  id="{{str_replace(" ","_",$item->function_name)}}-tab" data-toggle="tab" href="#{{str_replace(" ","_",$item->function_name)}}" role="tab" aria-controls="{{str_replace(" ","_",$item->function_name)}}" aria-selected="true">
                        <div id="{{str_replace(" ","_",$item->function_name)}}_piechart" style="width: 240px; height: 240px; background-color:transparent"></div>
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content " id="myTabContent">
            @foreach ($functionQuestionTotal as $item)
                <div class="tab-pane fade" id="{{str_replace(" ","_",$item->function_name)}}" role="tabpanel" aria-labelledby="profile-tab">
                    <ul class="nav nav-tabs mt-3" id="myTab" role="tablist" >
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($item->jobQuestionTotal as $item2)
                            <li class="nav-item">
                                <a class="nav-link" style="color:black" id="{{str_replace(" ","_",(str_replace("&","",($item2->job_name))))}}-tab" data-toggle="tab" href="#{{str_replace(" ","_",(str_replace("&","",($item2->job_name))))}}" role="tab" aria-controls="{{str_replace(" ","_",(str_replace("&","",($item2->job_name))))}}" aria-selected="true">
                                    <i class="fas fa-circle" style="color: {{$colorListDashboard[$i]}}"></i>
                                    {{$item2->job_name}}
                                </a>
                            </li>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </ul>
                        <div class="tab-content" id="myTabContent">
                            @foreach ($item->jobQuestionTotal as $item2)
                                <div class="tab-pane fade" id="{{str_replace(" ","_",(str_replace("&","",($item2->job_name))))}}" role="tabpanel" aria-labelledby="profile-tab">
                                    <div id="{{str_replace(" ","_",(str_replace("&","",($item2->job_name))))}}_chart"></div>
                                </div>
                            @endforeach
                        </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $('#description').removeClass('mb-5');
        })
        {
            google.charts.load('current', {'packages':['corechart']});
            google.charts.load('current', {'packages': ['corechart', 'bar']});
            
            @foreach ($functionQuestionTotal as $item)
                google.charts.setOnLoadCallback(drawChart{{str_replace(" ","_",$item->function_name)}});
                function drawChart{{str_replace(" ","_",$item->function_name)}}() {

                    var data = google.visualization.arrayToDataTable([
                        ['Job', 'Jumlah Soal'],
                        @foreach ($item->jobQuestionTotal as $item2)
                            ['{{$item2->job_name}}',  {{$item2->total_soal}}],
                        @endforeach
                    ]);

                    var options = {
                        title: '{{$item->function_name}} - ({{$item->total_soal}} soal)',
                        titleTextStyle : {fontSize: 14,bold: true},
                        legend : {position :'none'},
                        backgroundColor: { fill:'transparent' },
                        slices: {
                            0: { color: 'royalblue' },
                            1: { color: 'orangered' },
                            2: { color: 'orange' },
                            3: { color: 'forestgreen' },
                            4: { color: 'darkmagenta' },
                            5: { color: 'lightseagreen' },
                            6: { color: 'indianred' },
                            7: { color: 'olivedrab' },
                            8: { color: 'darkorchid' },
                            9: { color: 'yellow' },
                            10: { color: 'darkslateblue' },
                            11: { color: 'firebrick' },
                            12: { color: 'darkred' },
                            13: { color: 'steelblue' },
                            14: { color: 'chocolate' },
                            15: { color: 'limegreen' },
                            16: { color: 'mediumvioletred' },
                            17: { color: 'deeppink' },
                            
                        }
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('{{str_replace(" ","_",$item->function_name)}}_piechart'));

                    chart.draw(data, options);
                }
                @foreach ($item->jobQuestionTotal as $item2)
                    google.charts.setOnLoadCallback(drawStacked{{str_replace(" ","_",(str_replace("&","",($item2->job_id))))}});

                    function drawStacked{{str_replace(" ","_",(str_replace("&","",($item2->job_id))))}}() {
                        var data = google.visualization.arrayToDataTable([
                            ['Competency', 'Soal Terjawab', 'Sisa Soal' , { role: 'annotation' }],
                            @foreach ($item2->competencyQuizTotal as $item3)
                                ['{{$item3->competency_name}}', {{$item3->total_soal_dikerjakan}}, {{$item3->total_soal - $item3->total_soal_dikerjakan}} , '{{$item3->total_soal_dikerjakan}}/{{$item3->total_soal}}'],
                            @endforeach
                        ]);

                        var options = {
                            width: 1000,
                            height: 300,
                            title: '{{str_replace("&","AND",($item2->job_name))}}',
                            legend: { position: 'bottom'},
                            bar: { groupWidth: '40%' },
                            isStacked: true,
                        };
                        
                        var chart = new google.visualization.ColumnChart(document.getElementById('{{str_replace(" ","_",(str_replace("&","",($item2->job_name))))}}_chart'));
                        chart.draw(data, options);
                        }
                @endforeach
            @endforeach
        }
    </script>
@endsection
@endif
