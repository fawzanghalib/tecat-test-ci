@extends('layouts.home')
@section('head')
    <style>
        .multiple-select-filter>span{
            width: 100% !important;
        }
    </style>
@endsection
@section('title-tab')
    Master Scoring
@endsection
@section('title-content')
    Master Scoring
@endsection
@section('button')
    {{-- <a class="ml-2" href="{{route('scoring.download')}}" onclick="window.open( this.href ,'_blank');return false;">
        <button class="btn btn-md btn-info pull-right">
            <i class="fas fa-file-download"></i>
            Download   
        </button>
    </a> --}}
    <a class="ml-2" href="{{route("scoring.create")}}">
        <button class="btn btn-md btn-warning pull-right">
            <i class="fas fa-plus-circle"></i>
            Add Scoring
        </button>
    </a>
@endsection
@section('main-content')
    {{-- fillter modal --}}
    <div class="modal fade filter-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('scoring')}}" method="GET" autocomplete="off" style="margin-top:-20px">
                        <label class="mt-4">Search</label>
                        <div class="row">
                            <div class="col-md-9 pr-0">
                                <input name="search" type="text" class="form-control" placeholder="Search by Score Name.." value="{{$request->get('search')}}">
                            </div>
                        </div>

                        <label class="mt-4">Date</label>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                            </div>
                            <div class="col-md-1 pl-0 pr-0">
                                <center>~</center>
                            </div>
                            <div class="col-md-3 pl-0">
                                <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}" class="form-control">
                            </div>
                        </div>
                        <label class="mt-4">Type</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="type[]" class="custom-select select-scoring-type-filter" multiple="multiple">                                
                                        <option value='1'> jawab benar </option>
                                        <option value='2'> jawab benar & tepat waktu </option>
                                        <option value='3'> jawab salah </option>
                                        <option value='4'> jawab salah & tepat waktu </option>
                                        <option value='5'> loyalti </option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('scoring')}}">
                                    <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-info btn-md float-right mb-3" data-toggle="modal" data-target=".filter-modal">
                <i class="fas fa-filter"></i>
                filter
            </button>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="table-responsive" style="overflow-x:auto;">
                <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%"> 
                    <thead>
                        <tr>
                            <th class="th-sm text-center">@sortablelink('scoring_type','Score Name')</th>
                            <th class="th-sm text-center">@sortablelink('type', 'Type')</th>
                            <th class="th-sm text-center">@sortablelink('score', 'Score')</th>
                            <th class="th-sm text-center">@sortablelink('start_date', 'Start Date')</th>
                            <th class="th-sm text-center">@sortablelink('end_date', 'End Date')</th>
                            <th class="th-sm text-center">Status</th>
                            <th class="th-sm text-center" width="6%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($scorings->count())
                            @foreach($scorings  as $scoring)
                                <tr>
                                    <td>@php echo ucfirst($scoring->scoring_type) @endphp</td>
                                    <td>
                                        @if ($scoring->type == 1)
                                            Jawab benar
                                        @elseif($scoring->type == 2)
                                            Jawab benar & Tepat waktu
                                        @elseif($scoring->type == 3)
                                            Jawab salah
                                        @elseif($scoring->type == 4)
                                            Jawab salah & Tepat waktu
                                        @elseif($scoring->type == 5)
                                            Loyalti
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $scoring->score }}</td>
                                    <td class="text-center">{{ $scoring->start_date }}</td>
                                    <td class="text-center">{{ $scoring->end_date }}</td>
                                    <td class="text-center">
                                        @if ($scoring->flag_active == 1)
                                            Active
                                        @else
                                            No
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a class="float-left ml-2" href="{{route('scoring.edit',['id' => $scoring->scoring_id])}}">
                                            <button class="btn btn-sm btn-success">
                                                <i>edit</i>
                                            </button>
                                        </a>

                                        {{-- @if ($scoring->scoring_id != 1 && $scoring->scoring_id != 2 && $scoring->scoring_id != 3 $scoring->scoring_id != 4)
                                            @if ($scoring->flag_active )
                                                <form class="float-left ml-2 delete" action="{{route('scoring.unactivated',['id' => $scoring->scoring_id])}}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('put') }}
                                                    <button type="submit" class="btn btn-sm btn-danger">
                                                        <i>disable</i>        
                                                    </button>
                                                </form>
                                            @else
                                                <form class="float-left ml-2 delete" action="{{route('scoring.activate',['id' => $scoring->scoring_id])}}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('put') }}
                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i>enable</i>        
                                                    </button>
                                                </form>
                                            @endif
                                        @endif --}}

                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
        $(document).ready(function(){
            $(".select-scoring-type-filter").select2();
        })
    </script>
@endsection