@extends('layouts.home')
@section('title-tab')
    Create Scoring
@endsection
@section('title-content')
    Create Scoring
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('scoring')}}">Master Scoring</a></li>
    <li class="breadcrumb-item active">Create Scoring</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <form class="needs-validation" action="{{ route('scoring.store') }}" method="POST" autocomplete="off">
                {{ csrf_field() }}
                <div class="row">
                    <div class="card card-shadow col-md-12">
                        @if ($errors->any())
                            <div class="alert alert-danger mt-3">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="mb-3 col-md-6">
                                <label>Scoring Name<span style="color:red">*</span></label>
                                <input name="scoring_type" type="text" class="form-control" id="" placeholder="" maxlength="50" minlength="5" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label>Type<span style="color:red">*</span></label>
                                <select name="type" class="form-control custom-select" required>
                                    <option value="1">Jawab benar</option>
                                    <option value="2">Jawab benar & Tepat waktu</option>
                                    <option value="3">Jawab salah</option>
                                    <option value="4">Jawab salah & Tepat waktu</option>
                                    <option value="5">Loyalti</option>
                                </select>
                            </div>
                            <div class="mb-3 col-md-3">
                                <label>Score<span style="color:red">*</span></label>
                                <input name="score" type="number" class="form-control" id="" placeholder=""  maxlength="3" min="0" minlength="1" required onKeyPress="return angkadanhuruf(event,'1234567890',this)"/>
                            </div>
                            <div class="row pl-3">
                                <div class="mb-3 col-md-3">
                                    <label>Start Date<span style="color:red">*</span></label>
                                    <input name="start_date" type="date" id="sd1" onchange="setEndDate()" max="9999-12-31" class="form-control" required onKeyPress="return angkadanhuruf(event,'1234567890',this)"/>
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label>End Date<span style="color:red">*</span></label>
                                    <input name="end_date" type="date" id="ed1" max="9999-12-31" class="form-control" required >
                                    {{-- <input name="end_date" type="date" id="ed1" disabled max="9999-12-31" class="form-control" required > --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Add Score</button>
                </div>
            
            </form>
        </div>
    </div>
@endsection

