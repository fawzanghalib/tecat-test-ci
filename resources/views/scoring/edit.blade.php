@extends('layouts.home')
@section('title-tab')
    Edit Scoring | {{$scoring->scoring_type}}
@endsection
@section('title-content')
    Edit Scoring
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('scoring')}}">Master Scoring</a></li>
    <li class="breadcrumb-item active">Edit Scoring</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <form class="needs-validation" action="{{ route('scoring.update') }}" method="POST" autocomplete="off">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <input name="scoring_id" type="hidden" value="{{ $scoring->scoring_id}}" required>
                <div class="row">
                    <div class="card card-shadow col-md-12">
                        @if ($errors->any())
                            <div class="alert alert-danger mt-3">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="mb-3 col-md-6">
                                <div class="row ml-1">
                                    <label>Scoring Name</label>
                                    <input name="scoring_type" type="text" maxlength="50" minlength="5"  class="form-control" value="{{old('scoring_type') ?? $scoring->scoring_type}}" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"
                                        @if ($scoring->scoring_id == 1 || $scoring->scoring_id == 2 || $scoring->scoring_id ==3 || $scoring->scoring_id ==4)
                                            readonly
                                        @endif
                                    required>
                                </div>
                            </div>
                            <div class="mb-3 col-md-6">
                                <div class="row ml-1">
                                    <label>Type</label>
                                    <select name="type" class="form-control custom-select"
                                    @if ($scoring->scoring_id == 1 || $scoring->scoring_id == 2 || $scoring->scoring_id ==3 || $scoring->scoring_id ==4)
                                        disabled 
                                    @endif
                                    >
                                        <option value="1" @if((old('type') ?? $scoring->type) == 1) selected @endif>jawab benar</option>
                                        <option value="2" @if((old('type') ?? $scoring->type) == 2) selected @endif>jawab benar & tepat waktu</option>
                                        <option value="3" @if((old('type') ?? $scoring->type) == 3) selected @endif>jawab salah</option>
                                        <option value="4" @if((old('type') ?? $scoring->type) == 4) selected @endif>jawab salah & tepat waktu</option>
                                        <option value="5" @if((old('type') ?? $scoring->type) == 5) selected @endif>loyalti</option>
                                    </select>
                                    @if ($scoring->scoring_id == 1 || $scoring->scoring_id == 2 || $scoring->scoring_id ==3 || $scoring->scoring_id ==4)
                                        <input type="hidden" name="type" value="{{$scoring->type}}">
                                    @endif
                                </div>
                            </div>
                            <div class="mb-3 col-md-3">
                                <div class="row ml-1">
                                    <label>Score</label>
                                    <input name="score" type="number" maxlength="3" min="0" minlength="1" class="form-control" value="{{old('score') ?? $scoring->score}}" required onKeyPress="return angkadanhuruf(event,'1234567890',this)"/>
                                </div>
                            </div>
                            <div class="mb-4 col-md-6">
                                <div class="row ml-1">
                                    <label>Flag Active :</label>
                                    &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f1" type="radio" value="1" @if((old('flag_active') ?? $scoring->flag_active) == 1) checked @endif><label for="f1">&nbsp;Active</label> 
                                    @if ($scoring->scoring_id != 1 && $scoring->scoring_id != 2 && $scoring->scoring_id !=3)    
                                        &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f2" type="radio" value="0" @if((old('flag_active') ?? $scoring->flag_active) == 0) checked @endif><label for="f2">&nbsp;non Active</label>
                                    @endif
                                </div>
                            </div>
                            <div class="row pl-3">
                                <div class="mb-3 col-md-3">
                                    <div class="row ml-1">
                                        <label>Start Data</label>
                                        <input name="start_date" type="date" max="9999-12-31" id="sd1" onchange="setEndDate()" class="form-control" required value="{{old('start_date') ?? $scoring->start_date}}"
                                        @if ($scoring->scoring_id == 1 || $scoring->scoring_id == 2 || $scoring->scoring_id ==3 || $scoring->scoring_id ==4)
                                            readonly
                                        @endif
                                        >
                                    </div>
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label>End Data</label>
                                    <input name="end_date" type="date" max="9999-12-31" id="ed1" disabled class="form-control" required value="{{old('end_date') ?? $scoring->end_date}}"
                                    @if ($scoring->scoring_id == 1 || $scoring->scoring_id == 2 || $scoring->scoring_id ==3 || $scoring->scoring_id ==4)
                                        readonly
                                    @endif
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
                </div>
                
            </form>
        </div>
    </div>
@endsection

