@extends('layouts.home')
@section('title-tab')
    Create Quiz
@endsection
@section('title-content')
    Create Quiz
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('quiz')}}">Master Quiz</a></li>
    <li class="breadcrumb-item active">Create Quiz</li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <form class="needs-validation" action="{{ route('quiz.store') }}" method="POST" autocomplete="off">
                {{ csrf_field() }}
                <div class="row">
                    <div class="card card-shadow col-md-12 mb-5">
                        @if ($errors->any())
                            <div class="alert alert-danger mt-3">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="mb-3 col-md-6">
                                <label>Quiz Name<span style="color:red">*</span></label>
                                <input name="quiz_name" type="text" class="form-control"  maxlength="50" minlength="5" value="{{old('quiz_name')}}" autocomplete="off" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                            </div>
                            <div class="row pl-3">
                                <div class="mb-3 col-md-3">
                                    <label>Start Data<span style="color:red">*</span></label>
                                    <input max="9999-12-31" name="start_date" id="sd1" onchange="setEndDate()" type="date" class="form-control" required value="{{old('start_date')}}">
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label>End Data</label>
                                    <input max="9999-12-31" name="end_date" id="ed1" disabled type="date" class="form-control" required value="{{old('end_date')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Add Quiz</button>
                </div>
                
            </form>
        </div>
    </div>
@endsection

