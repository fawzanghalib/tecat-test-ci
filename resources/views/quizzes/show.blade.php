@extends('layouts.home')
@section('head')
    <style>
        .multiple-select-filter>span{
            width: 100% !important;
        }
    </style>
@endsection
@section('title-tab')
    Quiz - {{$quiz->quiz_name}}
@endsection
@section('title-content')
    {{$quiz->quiz_name}}
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('quiz')}}">Master Quiz</a></li>
    <li class="breadcrumb-item active">Detail Quiz</li>
</ol>
@endsection
@section('button')
    <a href="{{route("quiz-que-grade.create", ['quiz_id' => $quiz->quiz_id])}}">
        <button class="mr-2 btn btn-md btn-warning pull-right mr-2">
            <i class="fas fa-plus-circle"></i>
            Add Question
        </button>
    </a>
    <a href="{{route("quiz.edit", ['id' => $quiz->quiz_id, 'from' => 'quiz.show'])}}">
        <button class="btn btn-md btn-success pull-right mr-2">
            <i class="fas fa-pen"></i>
            Edit
        </button>
    </a>
    @if ($quiz->flag_active )
        <form class="delete" action="{{route('quiz.unactivated',['id' => $quiz->quiz_id])}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <button type="submit" class="ml-2 btn btn-md btn-danger pull-right mr-2">
                <i class="fas fa-trash-alt"></i>
                Disable        
            </button>
        </form>
    @else
        <form class="delete" action="{{route('quiz.activate',['id' => $quiz->quiz_id])}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <button type="submit" class="ml-2 btn btn-md btn-primary pull-right mr-2">
                <i class="fas fa-check"></i>
                Enable        
            </button>
        </form>
    @endif
@endsection
@section('description')
<div class="row ml-1 mb-4">
    <h4>Quiz Date :&nbsp;</h4>{{date("d-M-Y",strtotime($quiz->start_date))}} s/d {{date("d-M-Y",strtotime($quiz->end_date))}}
</div>
@endsection
@section('main-content')
    {{-- filter modal --}}
    <div class="modal fade filter-question" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('quiz.show',['id' => $quiz->quiz_id])}}" method="GET" autocomplete="off">
                        <label>Search</label>
                        <div class="row">
                            <div class="col-md-9 pr-0">
                                <input placeholder="Search By Question..." type="text" name="search" class="form-control" value="{{$request->get('search')}}">
                            </div>
                        </div>

                        <label class="mt-4">Job</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="job_id[]" class="form-control custom-select select-job-filter" multiple="multiple">                                
                                    @foreach ($jobs as $job)
                                        <option value="{{$job->job_id}}"
                                        @if ($request->get('job_id') !== null)    
                                            @if (in_array($job->job_id,$request->get('job_id'), FALSE))
                                                selected
                                            @endif
                                        @endif
                                        > {{$job->job_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Competency</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="competency_id[]" class="form-control custom-select select-competency-filter" multiple="multiple">                                
                                    @foreach ($competencies as $competency)
                                        <option value="{{$competency->competency_id}}"
                                            @if ($request->get('competency_id') !== null)
                                                @if (in_array($competency->competency_id,$request->get('competency_id'), FALSE))
                                                    selected
                                                @endif  
                                            @endif  
                                        > {{$competency->competency_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Level</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="level_id[]" class="form-control custom-select select-level-filter" multiple="multiple">                                
                                    @foreach ($levels as $level)
                                        <option value="{{$level->level_id}}"
                                            @if ($request->get('level_id') !== null)
                                                @if (in_array($level->level_id, $request->get('level_id'), FALSE))
                                                    selected
                                                @endif
                                            @endif
                                        > level {{$level->level_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Date</label>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                            </div>
                            <div class="col-md-1 pl-0 pr-0">
                                <center>~</center>
                            </div>
                            <div class="col-md-3 pl-0">
                                <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}" class="form-control">
                            </div>
                        </div>

                        <label class="mt-4">Active</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="flag_active" class="form-control custom-select">
                                    <option value="">-</option>
                                    <option value="1" @if ($request->get('flag_active') == "1") selected @endif>Active</option>
                                    <option value="0" @if ($request->get('flag_active') == "0") selected @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('quiz.show',['id' => $quiz->quiz_id])}}">
                                    <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            {{-- <input id="checkAll" type="checkbox" /> Check All
            <form action="{{route('quiz-que-grade.multiDestroy')}}" method="post">
                {{ csrf_field() }}
            </form> --}}
            <div class="row">
                <div class="col-md-2">
                    Total Soal : {{count($questions)}}
                </div>
                <div class="col-md-10">
                    <button type="button" class="btn btn-info btn-md float-right mb-5" data-toggle="modal" data-target=".filter-question">
                        <i class="fas fa-filter"></i>
                        filter
                    </button>
                    <form action="{{route('quiz.show',['id' => $quiz->quiz_id])}}" method="GET" autocomplete="off">
                        <button type="submit" class="btn btn-success btn-md float-right mb-5 mr-2" name="action" value="download">
                        <i class="fas fa-download"></i>
                            download                     
                        </button>
                    </form>
                </div>
            </div>
            <div class="row mb-5">
                <div class="card card-shadow col-md-12 mb-5">
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($questions as $question)
                        <div class="row mb-5 mt-2">
                            <div class="col-md-9">
                                <h5 class="">
                                    {{-- <span>
                                        <input name="question[{{$i}}]" type="checkbox" value="{{ $question->question_id }}" />
                                    </span> --}}
                                    {{$question->question}} 
                                </h5>
                                <ul>
                                    @foreach ($question->mstQuestionAnswers as $answer)
                                        <li @if ($answer->score == true)
                                                style="color:green; font-weight:bold;"
                                            @endif>
                                            {{$answer->answer}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-3" style="border-left: 1px solid lightgray;">
                                    <span class="mt-1" style="display:inline-block"> Code : {{$question->question_id}}</span><br>
                                    <span class="mt-1" style="display:inline-block"> {{$question->package_name}} </span><br>
                                @if ($question->competency_name)
                                    <span class="mt-1" style="display:inline-block"> {{$question->job_name}} </span><br>
                                    <span class="mt-1" style="display:inline-block"> {{$question->competency_name}} </span><br>
                                    <span class="mt-1" style="display:inline-block"> {{$question->level_name}} </span><br>
                                    <span class="mt-1" style="display:inline-block"> {{$question->modul_name}} </span><br>
                                @else
                                    <span class="mt-1" style="display:inline-block"> General </span><br>
                                @endif
                                <div class="mt-1">
                                    <form class="delete" style="display: inline;" action="{{route('quiz-que-grade.delete')}}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="hidden" name="quiz_id" value="{{$quiz->quiz_id}}">
                                        <input type="hidden" name="question_id" value="{{$question->question_id}}">
                                        <button class="btn btn-sm btn-danger" type="submit"><i>delete</i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
        $("#checkAll").change(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);       
        });
        $(document).ready(function(){
            $(".select-competency-filter, .select-level-filter , .select-job-filter").select2();
        })
    </script>
@endsection

