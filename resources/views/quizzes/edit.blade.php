@extends('layouts.home')
@section('title-tab')
    Edit Quiz | {{$quiz->quiz_name}}
@endsection
@section('title-content')
    Edit Quiz
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('quiz')}}" >Master Quiz</a></li>
    @if ($from == 'quiz.show')
        <li class="breadcrumb-item"><a href="{{route('quiz.show' ,['id' => $quiz->quiz_id])}}" >Detail Quiz
    @endif
    </a></li>
    <li class="breadcrumb-item active">Edit Quiz </li>
</ol>
@endsection
@section('main-content')
    <div class="card">
        <div class="card-block">
            <form class="needs-validation" action="{{ route('quiz.update') }}" method="POST">
                <div class="row">
                    <div class="card card-shadow col-md-12">
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger mt-3">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            <input name="quiz_id" type="hidden" value="{{ $quiz->quiz_id}}">
                            <input name="from" type="hidden" value="{{ $from }}">
                            <div class="row ml-1">
                                <div class="mb-3 col-md-6">
                                    <label>Quiz Name<span style="color:red">*</span> </label>
                                    <input name="quiz_name" type="text" class="form-control" maxlength="50" minlength="5" id="" placeholder="" value="{{  old('quiz_name') ?? $quiz->quiz_name}}" required onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                                </div>
                            </div>
                            <div class="row ml-1">
                                <div class="mb-3 col-md-6">
                                    <label>Flag Active :<span style="color:red">*</span></label>
                                    &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f1" type="radio" value="1" @if($quiz->flag_active == 1) checked @endif><label for="f1">Active</label>
                                    &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f2" type="radio" value="0" @if($quiz->flag_active == 0) checked @endif><label for="f2">non Active </label>
                                </div>
                            </div>
                            <div class="row ml-1">
                                <div class="col-md-3 mb-3">
                                    <label>Start Date<span style="color:red">*</span></label>
                                    <input name="start_date" id="sd1" onchange="setEndDate()" type="date" max="9999-12-31" class="form-control"  required value="{{ old('start_date') ?? $quiz->start_date }}" min="1">
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label>End Date</label>
                                    <input name="end_date" id="ed2" type="date" max="9999-12-31" class="form-control" required value="{{ old('end_date') ?? $quiz->end_date }}" min="1">
                                    {{-- <input name="end_date" id="ed2" disabled type="date" max="9999-12-31" class="form-control" required value="{{ old('end_date') ?? $quiz->end_date }}" min="1"> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-lg btn-block" type="submit">Update</button>
                </div>
                
            </form>
        </div>
    </div>
@endsection