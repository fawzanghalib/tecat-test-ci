@extends('layouts.home')
@section('title-tab')
    Master Quiz
@endsection
@section('title-content')
    Master Quiz
@endsection
@section('button')
    <a href="{{route("quiz.create")}}">
        <button class="btn btn-md btn-warning pull-right">
            <i class="fas fa-plus-circle"></i>
            Add Quiz
        </button>
    </a>
@endsection
@section('main-content')
    {{-- fillter modal --}}
    <div class="modal fade filter-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('quiz')}}" method="GET" style="margin-top:-30px">
                        <label class="mt-4">Search</label>
                        <div class="row">
                            <div class="col-md-9 pr-0">
                                <input name="search" type="text" class="form-control" placeholder="Search by Quiz Name.." value="{{$request->get('search')}}">
                            </div>
                        </div>

                        <label class="mt-4">Date</label>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                            </div>
                            <div class="col-md-1 pl-0 pr-0">
                                <center>~</center>
                            </div>
                            <div class="col-md-3 pl-0">
                                <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}" class="form-control">
                            </div>
                        </div>

                        <label class="mt-4">Active</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="flag_active" class="custom-select">
                                    <option value="">-</option>
                                    <option value="1" @if ($request->get('flag_active') == "1") selected @endif>Active</option>
                                    <option value="0" @if ($request->get('flag_active') == "0") selected @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('quiz')}}">
                                    <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-info btn-md float-right mb-3" data-toggle="modal" data-target=".filter-modal">
                <i class="fas fa-filter"></i>
                filter
            </button>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="table-responsive" style="overflow-x:auto;">
                <table id="dtBasicExample" class="table table-bordered table-md table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm text-center">@sortablelink('quiz_name','Quiz Name')</th>
                            <th class="th-sm text-center">@sortablelink('updated_at', 'Last Update')</th>
                            <th class="th-sm text-center">@sortablelink('start_date', 'Start Date')</th>
                            <th class="th-sm text-center">@sortablelink('end_date', 'End Date')</th>
                            <th class="th-sm text-center">Status</th>
                            <th class="th-sm text-center" width="18%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($quizzes->count())
                            @foreach($quizzes  as $key => $quiz)
                                <tr>
                                    <td>{{ $quiz->quiz_name }}</td>
                                    <td class="text-center">{{ $quiz->updated_at }}</td>
                                    <td class="text-center">{{ $quiz->start_date }}</td>
                                    <td class="text-center">{{ $quiz->end_date }}</td>
                                    <td class="text-center">
                                        @if ($quiz->flag_active == 1)
                                            Active
                                        @else
                                            No
                                        @endif
                                    </td>
                                    <td>
                                        <a class="float-left ml-2" href="{{route('quiz.edit',['id' => $quiz->quiz_id,'from' => 'quiz'])}}">
                                            <button class="btn btn-sm btn-success">
                                                <i>edit</i>
                                            </button>
                                        </a>
                                        <a class="float-left ml-2" href="{{route('quiz.show',['id' => $quiz->quiz_id])}}">
                                            <button class="btn btn-sm btn-info">
                                                <i>detail</i>
                                            </button>
                                        </a>
                                        {{-- <form class="float-left ml-2 delete" action="{{route('quiz.delete',['id' => $quiz->quiz_id])}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i>disable</i>        
                                            </button>
                                        </form> --}}
                                        @if ($quiz->flag_active )
                                            <form class="float-left ml-2 delete" action="{{route('quiz.unactivated',['id' => $quiz->quiz_id])}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('put') }}
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i>disable</i>        
                                                </button>
                                            </form>
                                        @else
                                            <form class="float-left ml-2 delete" action="{{route('quiz.activate',['id' => $quiz->quiz_id])}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('put') }}
                                                <input type="hidden" name="quiz_id" value="{{$quiz->quiz_id}}">
                                                <input type="hidden" name="start_date" value="{{$quiz->start_date}}">
                                                <input type="hidden" name="end_date" value="{{$quiz->end_date}}">
                                                <button type="submit" class="btn btn-sm btn-primary">
                                                    <i>enable</i>        
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
    </script>
@endsection