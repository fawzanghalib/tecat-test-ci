@extends('layouts.home')
@section('head')
    <style>
        .multiple-select-filter>span{
            width: 100% !important;
        }
    </style>
@endsection
@section('title-tab')
    Package - {{$package->package_name}} 
@endsection
@section('title-content')
    {{$package->package_name}}
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('bank-soal.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('package')}}" >Package</a></li>
    <li class="breadcrumb-item active">{{$package->package_name}}</li>
</ol>
@endsection
@section('button')
    <a href="{{route('question.create')}}">
        <button class="btn btn-md btn-warning pull-right">
        <i class="fas fa-plus-circle"></i>
            Input Bank Soal        
        </button>
    </a>
    <a href="{{route("package.edit",['id'=>$package->package_id])}}">
        <button class="btn btn-md btn-success mr-2 pull-right">
            <i class="fas fa-pen"></i>
            Edit        
        </button>
    </a>
    @if ($package->flag_active)
        <form class="delete" action="{{route('package.unactivated',['id'=>$package->package_id])}}" method="POST">
            {{ method_field('PUT')}}
            {{ csrf_field() }}
            <button type="submit " class="btn btn-md btn-danger mr-2 pull-right">
            <i class="fas fa-trash-alt"></i>
                Disable
            </button>
        </form>
    @else
    <form class="delete" action="{{route('package.activate',['id'=>$package->package_id])}}" method="POST">
        {{ method_field('PUT')}}
        {{ csrf_field() }}
        <button type="submit" class="btn btn-md btn-primary mr-2 pull-right">
        <i class="fas fa-check"></i>
            Enable        
        </button>
    </form>
    @endif
@endsection
@section('description')
<div class="row ml-1 mb-4">
    {{$package->description}}
</div>
@endsection
@section('main-content')
    {{-- filter modal --}}
    <div class="modal fade filter-question" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('package.show',['id' => $package->package_id])}}" method="GET">
                        <label>Search</label>
                        <div class="row">
                            <div class="col-md-9 pr-0">
                                <input type="text" name="search" placeholder="Search By Question..." class="form-control" value="{{$request->get('search')}}">
                            </div>
                        </div>

                        <label class="mt-4">Job</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="job_id[]" class="custom-select select-job-filter" multiple="multiple">                                
                                    @foreach ($jobs as $job)
                                        <option value="{{$job->job_id}}"
                                        @if ($request->get('job_id') !== null)    
                                            @if (in_array($job->job_id,$request->get('job_id'), FALSE))
                                                selected
                                            @endif
                                        @endif
                                        > {{$job->job_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Competency</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="competency_id[]" class="custom-select select-competency-filter" multiple="multiple">                                
                                    @foreach ($competencies as $competency)
                                        <option value="{{$competency->competency_id}}"
                                            @if ($request->get('competency_id') !== null)
                                                @if (in_array($competency->competency_id,$request->get('competency_id'), FALSE))
                                                    selected
                                                @endif  
                                            @endif  
                                        > {{$competency->competency_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Level</label>
                        <div class="row">
                            <div class="col-md-9 pr-0 multiple-select-filter"  >
                                <select name="level_id[]" class="custom-select select-level-filter" multiple="multiple">                                
                                    @foreach ($levels as $level)
                                        <option value="{{$level->level_id}}"
                                            @if ($request->get('level_id') !== null)
                                                @if (in_array($level->level_id, $request->get('level_id'), FALSE))
                                                    selected
                                                @endif
                                            @endif
                                        > level {{$level->level_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label class="mt-4">Date</label>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <input type="date" max="9999-12-31" name="start_date" value="{{$request->get('start_date')}}" class="form-control">
                            </div>
                            <div class="col-md-1 pl-0 pr-0">
                                <center>~</center>
                            </div>
                            <div class="col-md-3 pl-0">
                                <input type="date" max="9999-12-31" name="end_date" value="{{$request->get('end_date')}}" class="form-control">
                            </div>
                        </div>

                        <label class="mt-4">Active</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="flag_active" class="custom-select">
                                    <option value="">-</option>
                                    <option value="1" @if ($request->get('flag_active') == "1") selected @endif>Active</option>
                                    <option value="0" @if ($request->get('flag_active') == "0") selected @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Filter</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('package.show',['id' => $package->package_id])}}">
                                    <button class="btn btn-outline-primary btn-lg btn-block" type="button">Reset</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-md-2">
                    <h5> Total Soal : {{count($questions)}} </h5>
                </div>
                <div class="col-md-10">
                    <button type="button" class="btn btn-info btn-md float-right mb-5" data-toggle="modal" data-target=".filter-question">
                        <i class="fas fa-filter"></i>
                        filter
                    </button>
                </div>
            </div>
            @php $i=1; @endphp
            @foreach ($questions as $question)
                <div class="row mb-5">
                    <div class="col-md-9">
                        <h5 class="mt-3"> <b> No. <?php echo $i++ ?> : </b>{{$question->question}} </h5>
                        <ul>
                            @foreach ($question->mstQuestionAnswers as $answer)
                                <li @if ($answer->score == true)
                                        style="color:green; font-weight:bold;"
                                    @endif>
                                        {{$answer->answer}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-3" style="border-left: 1px solid lightgray;">
                        <span class="mt-1" style="display:inline-block">{{$question->competency_name}} </span><br>
                        <span class="mt-1" style="display:inline-block">{{$question->modul_name}} </span><br>
                        <span class="mt-1" style="display:inline-block">Level {{$question->level_name}} </span><br>
                        <span class="mt-1" style="display:inline-block">{{$question->job_name}} </span><br>
                        <span class="mt-1" style="display:inline-block;">
                        
                        <?php if($question->end_date == null){ 
                                echo $question->start_date; 
                            }else{
                                echo $question->start_date.' - '.$question->end_date;
                            } 
                        ?>
                        </span><br>
                        <div class="mt-3">
                            <a href="{{route('question.edit',['question_id' => $question->question_id])}}">
                                <button class="btn btn-sm btn-outline-success">
                                    <i>edit</i>
                                </button>
                            </a>
                            @if ($question->flag_active )
                                <form style="display: inline;" action="{{route('question.unactivated',['id' => $question->question_id])}}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    {{-- <input type="hidden" name="job_id" value="{{$question->job_id}}">
                                    <input type="hidden" name="package_id" value="{{$question->package_id}}"> --}}
                                    <button class="btn btn-sm btn-outline-danger" type="submit"><i>disable</i></button>
                                </form>
                            @else
                                <form style="display: inline;" action="{{route('question.activate',['id' => $question->question_id])}}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    {{-- <input type="hidden" name="job_id" value="{{$question->job_id}}">
                                    <input type="hidden" name="package_id" value="{{$question->package_id}}"> --}}
                                    <button class="btn btn-sm btn-outline-primary" type="submit">Activate</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
                
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure?");
        });
        $(document).ready(function(){
            $(".select-competency-filter, .select-level-filter , .select-job-filter").select2();
        })
    </script>

@endsection