@extends('layouts.home')
@section('title-tab')
    Package
@endsection
@section('title-content')
    Package
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('bank-soal.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Package</li>
</ol>
@endsection
@section('button')
    <a href="{{route("package.create")}}" class="btn pull-right hidden-sm-down btn-warning">
        <i class="fas fa-plus-circle"></i>
        Add Package
    </a>
@endsection
@section('main-content')
    <div class="container">
        <br/>
        <div class="row">
            @foreach ($packages as $package)
                <div class="col-md-4 mb-4">
                    <a href="{{route('package.show',['id' => $package['package_id']])}}" style="color:transparent">
                        <button type="submit" class="btn btn-warning btn-lg btn-block text-left" style="height:100%; width:103%">
                            <i class="fa fa-folder fa-lg"></i>
                            {{$package['package_name']}}
                        </button>
                    </a>
                </div>
            @endforeach
            <div class="col-md-3 mb-4">
                    <a style="color:transparent">
                        <button disabled type="submit" class="btn btn-outline-secondary btn-lg btn-block text-left" href="{{route('package.show',['id' => 0])}}">
                            <i class="fa fa-folder fa-lg"></i>
                            Soal Loyalty
                        </button>
                    </a>
                </div>
            </div>
        </div>
@endsection