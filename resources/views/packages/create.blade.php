@extends('layouts.home')
@section('title-tab')
    Create Package 
@endsection
@section('title-content')
    Create Package
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('bank-soal.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item">
    @if ($from == 2)
        <a href="{{route('question.create')}}" >Input Bank Soal
    @elseif($from == 3)
        <a href="{{route('question.createWithExcel')}}" >Input Bank Soal (excel)
    @else
        <a href="{{route('package')}}" >Package
    @endif
        </a>
    </li>
    <li class="breadcrumb-item active">Create Package</li>
</ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card card-block">
        <form class="needs-validation" action="{{ route('package.store') }}" method="POST" autocomplete="off">
            {{ csrf_field() }}
            <input name="from" type="hidden" value="{{$from}}" >
            @if ($errors->any())
                <div class="alert alert-danger mt-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <div class="row ml-1">
                    <div class="mb-3 col-md-6">
                        <label>Package Name<span style="color:red">*</span></label>
                        <input name="package_name" type="text" class="form-control" value="{{ old('package_name') ?? "" }}"  required maxlength="60" minlength="6" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                    </div>
                </div>
                <div class="mb-3 col-md-12">
                    <label>Description<span style="color:red">*</span></label>
                    <textarea name="description" required class="form-control" rows="3" maxlength="250" min="6" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)">{{ old('description') ?? "" }} </textarea>
                </div>
                <div class="row ml-1 mb-3">
                    <div class="col-md-3 mb-3">
                        <label>Start Date<span style="color:red">*</span></label>
                        <input name="start_date" type="date" max="9999-12-31" class="form-control"  required value="{{ old('start_date') ?? "" }}" min="1">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label>End Date</label>
                        <input name="end_date" type="date" max="9999-12-31" class="form-control" value="{{ old('end_date') ?? "" }}" min="1">
                    </div>
                </div>
            </div>  
            <button class="btn btn-warning btn-lg btn-block" type="submit">Add Package</button>
        </form>
    </div>
</div>
@endsection 