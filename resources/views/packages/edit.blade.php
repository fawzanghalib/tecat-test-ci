@extends('layouts.home')
@section('title-tab')
    Edit Package | {{$package->package_name}}
@endsection
@section('title-content')
    Edit Package
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('bank-soal.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('package')}}" >Package</a></li>
    <li class="breadcrumb-item"><a href="{{route('package.show',['id' => $package->package_id])}}" >{{$package->package_name}}</a></li>
    <li class="breadcrumb-item active">Edit Package</li>
    </ol>
@endsection
@section('main-content')
<div class="card">
    <div class="card card-block">
        <form class="needs-validation" action="{{ route('package.update') }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <input type="hidden" name="package_id" value="{{$package->package_id}}">
            <div class="card card-shadow col-md-12 mb-3">
                @if ($errors->any())
                    <div class="alert alert-danger mt-3">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-body">
                    <div class="mb-3 col-md-6">
                        <label>Package Name<span style="color:red">*</span></label>
                        <input name="package_name" value="{{old('package_name') ??$package->package_name}}" type="text" class="form-control" id="" placeholder=""  required="" maxlength="60" minlength="6" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)"/>
                    </div>
                    <div class="mb-3 col-md-12">
                        <label>Description<span style="color:red">*</span></label>
                        <textarea name="description" class="form-control" id="exampleFormControlTextarea1" maxlength="250" minlength="6" onKeyPress="return angkadanhuruf(event,' .,/_-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)" rows="3"> {{old('description') ?? $package->description}}</textarea>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label>Flag Active :<span style="color:red">*</span></label>
                        &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f1" required type="radio" value="1" @if($package->flag_active
                        == 1) checked @endif><label for="f1"> Active</label>
                        &nbsp;&nbsp;&nbsp;<input name="flag_active" id="f2" type="radio" value="0" @if($package->flag_active
                        == 0) checked @endif><label for="f2">non Active</label>
                    </div>
                    <div class="row ml-1">
                        <div class="col-md-3 mb-3">
                            <label>Start Date<span style="color:red">*</span></label>
                            <input name="start_date" type="date" max="9999-12-31" class="form-control"  required value="{{ old('start_date') ?? $package->start_date }}" min="1">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label>End Date</label>
                            <input name="end_date" type="date" max="9999-12-31" class="form-control" value="{{ old('end_date') ?? $package->end_date }}" min="1">
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-warning btn-lg btn-block" type="submit">Update Package</button>
        </form>
    </div>
</div>
@endsection