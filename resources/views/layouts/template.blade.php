<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="charles-hilarius">
    <!-- Favicon icon -->
    <link href="{{URL::asset('img/TECAT_001.png')}}" rel="icon" type="image/x-icon">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/TECAT_001.png')}}">
    <title>TECAT - @yield('title-tab')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
    <!-- Custom CSS -->
    <link href="{{ URL::asset('/css/style.css') }}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ URL::asset('css/colors/blue.css') }}" id="theme" rel="stylesheet">
    <!-- iCon -->
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/TECAT_090.png')}}">
    <!-- Meta Tag -->
    <meta name="description" content="Technical Knowledge Daily Test">
    <!-- jQuery -->
    <script src="{{ URL::asset('/js/jquery.min.js') }}" ></script>
    <script src="{{ URL::asset('/js/bootstrap.min.js') }}"></script>
    
    <!-- Styles -->
    <!--Fontawsome-->
    <script src="{{ URL::asset('/js/fontawesome.js') }}"></script>
    <!-- Chart.js -->
    <script src="{{URL::asset('/js/charts.loader.js')}}"></script>
    <!--select2-->
    <link href="{{ URL::asset('/css/select2.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('/js/select2.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    
    <script src="{{ URL::asset('/js/jquery.min.js') }}" ></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/tether.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ URL::asset('/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ URL::asset('/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ URL::asset('/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ URL::asset('/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ URL::asset('/js/custom.min.js') }}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- Flot Charts JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ URL::asset('/assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ URL::asset('/js/flot-data.js') }}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{ URL::asset('/assets/plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>

</head>
    @yield('head')
</head>
<body class="fix-header fix-sidebar card-no-border">
    @yield('content')
    @yield('script')
</body>

<style>

body {
  padding-right: 0px !important;
}

.modal-open {
  overflow-y: auto;
}

</style>
</html>


