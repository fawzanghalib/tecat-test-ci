@extends('layouts.home')
@section('title-tab')
Error
@endsection
@section('title-content')
Error
@endsection
@section('button')

@endsection
@section('main-content')
    <div class="alert alert-danger" role="alert">
        {{$message}}
    </div>
@endsection