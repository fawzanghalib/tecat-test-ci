<?php 
    use App\Http\Controllers\UserController;
    $userData = UserController::getUserData();
?>
@extends('layouts.template')
@section('content')

    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <div class="navbar-header" style="background-color:white; border-bottom: 5px solid #dcdcdc;">
                    <a class="navbar-brand" href="{{route('bank-soal.dashboard')}}">
                            <img src="{{URL::asset('img/TECAT_TR.png')}}" style="width:60px;" class="dark-logo" alt="homepage"/>
                        <span>
                            <img src="{{URL::asset('img/TECAT_LONG.png')}}" style="width:70px;" class="dark-logo" alt="homepage"/>
                        </span>
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item hidden-sm-down">
                            <!-- <form class="app-search p-l-20">
                                <input type="text" readonly class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a>
                            </form> -->
                            <!-- <a class="nav-link"> 
                            <img src="{{asset('img/logo_acc.png')}}" style="width:20%;" class="dark-logo" alt="homepage"/>
                            </a> -->
                        </li>
                    </ul>
                    <div class="dropdown">
                        <ul class="navbar-nav my-lg-0 m-r-20">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" 
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                                    <img src="{{asset('img/logo_acc.png')}}" alt="user" class="profile-pic m-r-5" />
                                    Hi, {{$userData['name']}}
                                </a>
                                <div class="dropdown-content dropdown-menu-lg-right">
                                <a class="dropdown-item" href="{{route('user.change.password')}}">
                                    Change Password
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Log Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" style="position:fixed;">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-item">
                            <a href="#" class="waves-effect" id="btnBankSoal" data-toggle="collapse" data-target="#collapseBankSoal" aria-expanded="false">
                            <i class="fa fa-bank m-r-10" aria-hidden="true"></i>Bank Soal
                            </a>
                            <div class="collapse" id="collapseBankSoal" aria-expanded="false" style="">
                                <ul style="list-style:none">
                                    <li> <a href="{{route('bank-soal.dashboard')}}"> Dashboard </a></li>
                                    <li> <a href="{{route('question.create')}}"> Input Bank Soal </a></li>
                                    <li> <a href="{{route('package')}}"> Package </a></li>
                                </ul>
                            </div>    
                            <hr>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="waves-effect" id="btnMatrixCompetency" data-toggle="collapse" data-target="#collapseMatrixCompetency" aria-expanded="false">
                            <i class="fa fa-cubes m-r-10" aria-hidden="true"></i>Matrix Competency
                            </a>
                            <div class="collapse" id="collapseMatrixCompetency" aria-expanded="false" style="">
                                <ul style="list-style:none">
                                    <li> <a href="{{route('competency-detail')}}"> Dashboard </a></li>
                                    <li> <a href="{{route('competency-detail.create')}}"> Competency Required </a></li>
                                </ul>
                            </div>    
                            <hr>
                        </li>
                        <li class="nav-item">
                            <a class="waves-effect" href="{{route('customize-notification.index')}}"> 
                            <i class="fa fa-bell m-r-10" aria-hidden="true"></i>Notification 
                            </a>
                            <hr>
                        </li>
                        <li class="nav-item">
                            <a class="waves-effect" href="{{route('reports')}}">
                            <i class="fa fa-book m-r-10" aria-hidden="true"></i>Reports
                            </a>   
                            <hr>
                        </li>
                        <li class="nav-item">
                            <a class="waves-effect" href="{{route('competency-analysis-results.index')}}">
                            <i class="fa fa-life-ring m-r-10" aria-hidden="true"></i>Compt. Analysis Result
                            </a>   
                            <hr>
                        </li>
                        <li class="nav-item">
                        <a class="waves-effect" id="btnCRUD" data-toggle="collapse" data-target="#collapseCRUD" aria-expanded="false">
                        <i class="fa fa-database m-r-10" aria-hidden="true"></i>Setting
                        </a>
                        <div class="collapse" id="collapseCRUD" aria-expanded="false" style="">
                            <ul style="list-style:none">

                            @if($userData['role']=='1')
                                <li> <a href="{{route('user')}}"> Users </a></li>
                            @endif
                                <li> <a href="{{route('function')}}"> Function </a></li>
                                <li> <a href="{{route('job')}}"> Job </a></li>
                                <li> <a href="{{route('competency')}}"> Competency </a></li>
                                <li> <a href="{{route('modul')}}"> Modul </a></li>
                                <li> <a href="{{route('quiz')}}"> Quiz </a></li>
                                <li> <a href="{{route('scoring')}}"> Scoring </a></li>
                                <li> <a href="{{route('level')}}"> Level </a></li>
                            </ul>
                        </div>    
                        <hr>
                        </li>
                    </ul>
                    <!-- 
                        <div class="text-center m-t-30">
                            <a class="btn btn-danger" href="{{ route('logout') }}" style="color:white" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>&nbsp;Log Out
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    -->
                   
                </nav>
            </div>
        </aside>
        <div class="page-wrapper">
            <div id="loader-backgaround">
                <div class="loader"></div>
            </div>
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="m-b-0 m-t-0">@yield('title-content')</h3>
                        @yield('breadcrumb')
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        @yield('button')                
                    </div>
                </div>
                <div class="" id="description">
                    @yield('description')
                </div>
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @yield('main-content')
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#loader-backgaround').remove();
        })
    </script>
    <style>

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #fff;
            min-width: 130px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 14px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .title-breadcrumb{
            color:#e88c1e;
        }
    </style>

    <!-- VALIDATE ONKEYPRESS -->
    <script language="javascript">
        function getkey(e)
        {
        if (window.event)
        return window.event.keyCode;
        else if (e)
        return e.which;
        else
        return null;
        }
        function angkadanhuruf(e, goods, field)
        {
        var angka, karakterangka;
        angka = getkey(e);
        if (angka == null) return true;
        
        karakterangka = String.fromCharCode(angka);
        karakterangka = karakterangka.toLowerCase();
        goods = goods.toLowerCase();
        
        // check goodkeys
        if (goods.indexOf(karakterangka) != -1)
            return true;
        // control angka
        if ( angka==null || angka==0 || angka==8 || angka==9 || angka==27 )
        return true;
            
        if (angka == 13) {
            var i;
            for (i = 0; i < field.form.elements.length; i++)
            if (field == field.form.elements[i])
                break;
            i = (i + 1) % field.form.elements.length;
            field.form.elements[i].focus();
            return false;
            };
        // else return false
        return false;
        }
    </script>

<script>
    function setEndDate()
    {
        var StartDate= document.getElementById('sd1').value;
        if(StartDate!='')
        {
            document.getElementById('ed1').disabled = false;
            $('#ed1').val("");
            $('#ed1').attr('min' , StartDate);
            return false;
        }
        
    }
</script>
@endsection