<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="charles-hilarius">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('/assets/images/9133logo_acc.ico') }}">
    <title>Technical Knowledge Daily Test</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
    <!-- Custom CSS -->
    <link href="{{ URL::asset('/css/style.css') }}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ URL::asset('css/colors/blue.css') }}" id="theme" rel="stylesheet">
    <!-- iCon -->
    <link rel="icon" type="image/png" sizes="192x192" href="{{URL::asset('img/TECAT_090.png')}}">
    <!-- Meta Tag -->
    <meta name="description" content="Technical Knowledge Daily Test">
    <!-- jQuery -->
    <script src="{{ URL::asset('/js/jquery.min.js') }}" ></script>
    <script src="{{ URL::asset('/js/bootstrap.min.js') }}"></script>
    
    <!-- Styles -->
    <!--Fontawsome-->
    <script src="{{ URL::asset('/js/fontawesome.js') }}"></script>
    <!-- Chart.js -->
    <script src="{{URL::asset('/js/charts.loader.js')}}"></script>
    <!--select2-->
    <link href="{{ URL::asset('/css/select2.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('/js/select2.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    
    <script src="{{ URL::asset('/js/jquery.min.js') }}" ></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/tether.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ URL::asset('/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ URL::asset('/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ URL::asset('/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ URL::asset('/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ URL::asset('/js/custom.min.js') }}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- Flot Charts JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ URL::asset('/assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ URL::asset('/js/flot-data.js') }}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{ URL::asset('/assets/plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>

</head>
<div id="main-wrapper">
    <header class="topbar">
        <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
            <div class="navbar-header" style="background-color:white; border-bottom: 5px solid #dcdcdc; border-top: 3px solid #dcdcdc;">
                <a class="navbar-brand" href="{{route('bank-soal.dashboard')}}">
                        <img src="{{URL::asset('img/TECAT_TR.png')}}" style="width:60px;" class="dark-logo" alt="homepage"/>
                    <span>
                        <img src="{{URL::asset('img/TECAT_LONG.png')}}" style="width:70px;" class="dark-logo" alt="homepage"/>
                    </span>
                </a>
            </div>
        </nav>
    </header>
    <body class="fix-header fix-sidebar card-no-border" style="overflow-y:hidden">
        <div id="loader-backgaround">
            <div class="loader"></div>
        </div>
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                </div>
                <div class="col-md-6 col-4 align-self-center">            
                </div>
            </div>
            <div class="" id="description">
            </div>
            @yield('content')
        </div>
    </body>
        
    <!-- Scripts -->
    <script src="{{ URL::asset('js/app.js') }}"></script>
</body>
</div>


    <!-- VALIDATE ONKEYPRESS -->
    <script language="javascript">
        function getkey(e)
        {
        if (window.event)
        return window.event.keyCode;
        else if (e)
        return e.which;
        else
        return null;
        }
        function angkadanhuruf(e, goods, field)
        {
        var angka, karakterangka;
        angka = getkey(e);
        if (angka == null) return true;
        
        karakterangka = String.fromCharCode(angka);
        karakterangka = karakterangka.toLowerCase();
        goods = goods.toLowerCase();
        
        // check goodkeys
        if (goods.indexOf(karakterangka) != -1)
            return true;
        // control angka
        if ( angka==null || angka==0 || angka==8 || angka==9 || angka==27 )
        return true;
            
        if (angka == 13) {
            var i;
            for (i = 0; i < field.form.elements.length; i++)
            if (field == field.form.elements[i])
                break;
            i = (i + 1) % field.form.elements.length;
            field.form.elements[i].focus();
            return false;
            };
        // else return false
        return false;
        }
    </script>

</html>
